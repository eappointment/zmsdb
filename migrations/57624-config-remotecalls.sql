INSERT INTO `config` SET `name` = "remotecall__baseUrl_prod", `value` = "", `description` = "The BaseUrl for the production system RemoteCall request is entered here";
INSERT INTO `config` SET `name` = "remotecall__baseUrl_stg", `value` = "", `description` = "The BaseUrl for the stage system RemoteCall request is entered here";
INSERT INTO `config` SET `name` = "remotecall__baseUrl_dev", `value` = "", `description` = "The BaseUrl for the dev system RemoteCall request is entered here";
INSERT INTO `config` SET `name` = "remotecall__tenantId", `value` = "", `description` = "A Tenant ID is a unique identifier used to identify the RemoteCall tenant";
INSERT INTO `config` SET `name` = "remotecall__positiveList", `value` = "", `description` = "A comma-separated list of domain names which are released for remote calls";
INSERT INTO `config` SET `name` = "cron__sendRemoteCalls", `value` = "dev", `description` = "Sends all remote call requests that are stored in the event log table.";

ALTER TABLE config MODIFY COLUMN value TEXT NOT NULL;