DROP TABLE IF EXISTS `maintenanceschedule`;
CREATE TABLE `maintenanceschedule` (
    `scheduleId` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    `creatorId` INT(10) UNSIGNED NOT NULL ,
    `creationDateTime` DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    `startDateTime` DATETIME DEFAULT NULL,
    `isActive` INT(1) UNSIGNED NOT NULL DEFAULT 0 ,
    `isRepetitive` INT(1) UNSIGNED NOT NULL DEFAULT 0 ,
    `scheduleTime` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
    `duration` INT(5) UNSIGNED NOT NULL DEFAULT 0 ,
    `leadTime` INT(5) UNSIGNED NOT NULL DEFAULT 0 ,
    `affectedArea` VARCHAR(50) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
    `announcement` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
    `documentBody` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL
)
ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_unicode_ci;