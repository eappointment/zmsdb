DROP TABLE IF EXISTS `checkintask`;
CREATE TABLE `checkintask` (
    `id` INT(10) UNSIGNED NOT NULL AUTO_INCREMENT PRIMARY KEY ,
    `status` enum("open", "late", "early", "arrived", "noShow", "N/A") NOT NULL DEFAULT "open",
    `refersTo` CHAR(40) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL ,
    `referenceId` INT UNSIGNED NOT NULL ,
    `scopeId` INT UNSIGNED NOT NULL ,
    `checkInTS` INT NOT NULL DEFAULT 0,
    `scheduledTS` INT NOT NULL ,
    `checkInSource` VARCHAR(30) NOT NULL ,
    `lastChange` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
    INDEX idxReferenceIds (`referenceId`),
    INDEX idxScheduledTS (`scheduledTS`)
)
ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_unicode_ci;

DROP TABLE IF EXISTS `checkinconfig`;
CREATE TABLE `checkinconfig` (
     `id` CHAR(32) NOT NULL PRIMARY KEY ,
     `status` enum("on", "off", "info", "maintenance", "setup") NOT NULL DEFAULT "setup",
     `scopeIds` VARCHAR(250) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
     `lastChange` TIMESTAMP DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP ,
     `startBeforeAvailability` TINYINT UNSIGNED NOT NULL DEFAULT 0,
     `startBeforeMinutes` INT UNSIGNED NOT NULL DEFAULT 0,
     `tooEarlyMinutes` INT UNSIGNED NOT NULL DEFAULT 0,
     `toleranceMinutes` INT UNSIGNED NOT NULL DEFAULT 0,
     `activeAfterClosing` TINYINT UNSIGNED NOT NULL DEFAULT 0,
     `languages` VARCHAR(200) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '',
     `msgAlternative` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '{"de_DE":"","en_GB":""}',
     `msgLate` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '{"de_DE":"","en_GB":""}',
     `msgEarly` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '{"de_DE":"","en_GB":""}',
     `msgWrongDay` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '{"de_DE":"","en_GB":""}',
     `msgWrongCounter` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '{"de_DE":"","en_GB":""}',
     `msgSuccess` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '{"de_DE":"","en_GB":""}',
     `msgMaintenance` TEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL DEFAULT '{"de_DE":"","en_GB":""}',
     INDEX idxScopeIds (`scopeIds`)
)
ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_unicode_ci;

INSERT INTO `config` SET `name` = "checkin__feature", `value` = "stage,dev", `description` = "feature switch for the check-in";
INSERT INTO `config` SET `name` = "checkin__baseUrl", `value` = "/terminvereinbarung/checkin/", `description` = "BaseUrl for the check-in-application";
INSERT INTO `config` SET `name` = "checkin__languages", `value` = "de_DE,en_GB", `description` = "comma separated language codes";