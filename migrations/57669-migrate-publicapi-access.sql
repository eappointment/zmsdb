ALTER TABLE `nutzer`
    ADD COLUMN `apitoken` VARCHAR(255) NOT NULL DEFAULT '';

ALTER TABLE `apikey`
    ADD COLUMN `locked` int(1) NOT NULL DEFAULT '0';

ALTER TABLE `apiclient`
    ADD COLUMN `permission__remotecall` int(1) NOT NULL DEFAULT '0',
    ADD COLUMN `secondsToInvalidation` int(5) NOT NULL;