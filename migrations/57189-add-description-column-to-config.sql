ALTER TABLE `config` ADD COLUMN `description` varchar (250) NOT NULL DEFAULT '';


UPDATE `config` SET `description` = "Sends reminder emails in the specified time period in hours" WHERE `name` = "cron__sendMailReminder";
UPDATE `config` SET `description` = "Releases reserved appointments when the reservation period specified in the scope has expired" WHERE `name` = "cron__deleteReservedData";
UPDATE `config` SET `description` = "Recalculates free and full appointment slots in the slots table if there have been changes to opening hours or relevant location data and cancels past appointment slots" WHERE `name` = "cron__calculateSlots";
UPDATE `config` SET `description` = "API access for an API key is provided with access quotas for a period of time. These are reset here after expiration" WHERE `name` = "cron__resetApiQuota";
UPDATE `config` SET `description` = "Session data is removed based on the session name and the specified time period in minutes" WHERE `name` = "cron__deleteSessionData";
UPDATE `config` SET `description` = "Imports and updates client source data such as services and locations into the database" WHERE `name` = "cron__updateDldbData";
UPDATE `config` SET `description` = "Resets the settings for fictitious clerks in each location back to the default value -1" WHERE `name` = "cron__resetGhostWorkstationCount";
UPDATE `config` SET `description` = "Logs out all agents from the system and resets appointments that are still called up" WHERE `name` = "cron__resetWorkstations";
UPDATE `config` SET `description` = "Removes all holidays from the database after a defined period of time - e.g. 6 months" WHERE `name` = "cron__deleteDayoffData";
UPDATE `config` SET `description` = "Deletes expired and future blocked appointments after the specified period in days" WHERE `name` = "cron__deleteAppointmentData";
UPDATE `config` SET `description` = "Sends the appointment list of the current day to the scope admin email address stored in the scope settings based on the passed scope ids" WHERE `name` = "cron__sendProcessListToScopeAdmin";
UPDATE `config` SET `description` = "Sends reminder SMS based on the reminder settings of the appointment process" WHERE `name` = "cron__sendNotificationReminder";
UPDATE `config` SET `description` = "Checks if there are still open migrations and executes them" WHERE `name` = "cron__migrate";
UPDATE `config` SET `description` = "Calculates and writes all public holidays up to the specified year" WHERE `name` = "cron__calculateDayOffList";
UPDATE `config` SET `description` = "Writes completed and archived appointments to the statistics table" WHERE `name` = "cron__archiveStatisticData";
UPDATE `config` SET `description` = "Canceled appointments released again for booking after the deletion delay specified in the scope" WHERE `name` = "cron__deallocateAppointmentData";
UPDATE `config` SET `description` = "Delete the emails from existing appointments based on the specified blacklistedAddressList config entries " WHERE `name` = "cron__deleteBlacklistedMail";
UPDATE `config` SET `description` = "This cronjob removes pickup entries that have an expiration time that lies twelve months in the past" WHERE `name` = "cron__deleteExpiredPickups";
UPDATE `config` SET `description` = "Removes all opening hours in the past starting from the given number of days (x) or until the given date (yyyy-mm-dd). If no value is specified, 28 days are specified as the default value." WHERE `name` = "cron__deleteOldAvailabilityData";
UPDATE `config` SET `description` = "Removes all entries in the eventlog table that are outdated" WHERE `name` = "cron__deleteOldEventLogEntries";

