ALTER TABLE config
    MODIFY COLUMN value LONGTEXT CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL;

ALTER TABLE `mailpart`
    MODIFY COLUMN mime VARCHAR(100),
    ADD COLUMN `attachmentName` VARCHAR(100) DEFAULT '';


INSERT INTO `config` SET `name` = "mailings__pollingDistanceEmailMessage", `value` = 'Sehr geehrte Damen und Herren,<br /><br />
anbei befindet sich ein automatisch generierter Report zur Auswertung von Buchungs-Zeiträumen im Zeitmanagement-System des Landes Berlin.<br /><br />
Hinweise zur Auswertung:<br />
Die Auswertung ist in mehrere Tabellen-Blätter unterteilt. Dabei werden Namensgleiche Behörden verschiedener Organisationen zusammengefasst, so dass zum Beispiel alle Bürgerämter im Vergleich auf einer Seite angezeigt werden.<br /><br />
Ein Report umfasst die Terminbuchungen eines Tages. Das Datum des ausgewerteten Tages befindet sich im Namen der Tabellenblätter.<br /><br />
Für jede Behörde werden die Buchungstermine des Tages zusammen mit der Anzahl der Buchungen für diesen Termin-Tag angzeigt. Die Spalte "Tage" zeigt die Tage in der Zukunft relativ zum ausgewerteten Tag an.<br /><br />
Termine am selben Tag sowie Spontankunden können aus technischen Gründen nicht ausgewertet werden. Die Tabellen umfassen daher nur Termine, die mindestens einen Tag in der Zukunft liegen.<br /><br />
Bei Problemen oder Fragen wenden Sie sich bitte an die folgende Adresse:<br />
zms-admin@itdz-berlin.de';

INSERT INTO `config` SET `name` = "mailings__pollingDistanceEmailList", `value` = 'torsten.kunst@berlinonline.de,server@service.berlinonline.de,monitoring-buergerdienste@senatskanzlei.berlin.de,ikt-zms@senatskanzlei.berlin.de';

INSERT INTO `config` SET `name` = "mailings__pollingFreeEmailMessage", `value` = 'Sehr geehrte Damen und Herren,<br /><br />
anbei befindet sich ein automatisch generierter Report zur Auswertung verfügbarer Termine im Zeitmanagement-System des Landes Berlin.<br /><br />
Hinweise zur Auswertung:<br />
Der Report prüft eine Reihe von Terminsuchen, welche in der ersten Zeile aufgelistet sind. In der ersten Spalte befindet sich das Datum des Reports und in den folgenden Zeilen jeweils eine Uhrzeit. Zu der jeweils angegebenen Uhrzeit wurde jeweils die für den Bürger ersichtliche Kalender-Ansicht in der Terminvereinbarung abgerufen.<br /><br />
Von den abgerufenen Seiten wird jeweils gezählt, wieviele Tage von dem im Kalender angezeigten heutigen Tag an keine Termine verfügbar waren. Diese Zahl wird in den Zeilen hinter der Uhrzeit angezeigt. Wenn z.B. der heutige Tag der 01.06. wäre und auch an diesem Tag Termine verfügbar wären, würde eine "0" angezeigt werden. Wäre erst am 27.06. ein Termin frei, würde eine "26" angezeigt werden. Das bedeutet, dass erst 26 Tage in der Zukunft wieder Termine verfügbar sind.<br /><br />
Wird ein Termin innerhalb von 14 Tagen angeboten, dann wird die entsprechende Tabellenzelle grün angezeigt, ansonsten rot.<br /><br />
Ist kein Termin mehr als 30 Tage in der Zukunft verfügbar, wird ein "30+" angezeigt. Da nur die erste Kalender-Seite ausgewertet wird, kann technisch hier keine genauere Angabe mehr erfolgen.<br /><br />
Am Ende des Reports befinden sich statistische Kennzahlen. Die errechneten Durchschnittswerte ("ø") sind nicht zu 100% exakt, da für nicht existierende Werte ("30+") ein hypothetischer Wert von 56 verwendet wird. Zur Interpretation von Tendenzen sind die Werte aber geeignet. Der "ø Jahr" liefert die Werte seit dem 1. Januar des Jahres.<br /><br />
Die Prozentwerte ("%") zeigen an, wieviel Prozent der Abfragen an einem Tag innerhalb von 14 Tagen (grün) buchbar waren. Die weiteren Werte stellen jeweils den Durchschnitt der angegebenen Zeitspanne dar.<br /><br />
Bei Problemen oder Fragen wenden Sie sich bitte an die folgende Adresse:<br />
zms-admin@itdz-berlin.de';

INSERT INTO `config` SET `name` = "mailings__pollingFreeBaEmailList", `value` = 'torsten.kunst@berlinonline.de,anke.lausecker@ba-spandau.berlin.de,burkhard.milde@ba-mitte.berlin.de,christine.ruflett@ba-pankow.berlin.de,joachim.stuerzbecher@ba-sz.berlin.de,Torsten.Zickert@ba-ts.berlin.de,cw110000@charlottenburg-wilmersdorf.de,Axel.Hunger@lichtenberg.berlin.de,roman.skwirawski@reinickendorf.berlin.de,monitoring-buergerdienste@seninnds.berlin.de,ikt-zms@senatskanzlei.berlin.de,matthias.hartung@ba-fk.berlin.de,andrea.lange@bezirksamt-neukoelln.de,marion.laemmel@ba-tk.berlin.de,katja.hannebauer@ba-mh.berlin.de';

INSERT INTO `config` SET `name` = "mailings__pollingFreeKfzEmailList", `value` = 'server@service.berlinonline.de,Ulrike.Frey@labo.berlin.de,torsten.kunst@berlinonline.de';

INSERT INTO `config` SET `name` = "mailings__pollingAvailabilityReviewEmailList", `value` = 'server@service.berlinonline.de,ikt-zms@senatskanzlei.berlin.de,torsten.kunst@berlinonline.de';

INSERT INTO `config` SET `name` = "mailings__pollingGroupedEmailList", `value` = 'ikt-zms@senatskanzlei.berlin.de';