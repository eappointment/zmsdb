INSERT INTO `config`
SET `name` = "availability__calculateSlotsIgnoreScopes", `value` = "", `description` = "ScopeIds that are to be excluded from the slot calculation, which takes place every 5 minutes, are entered here.";

INSERT INTO `config`
SET `name` = "availability__calculateSlotsMaxPreBooking", `value` = "365", `description` = "Scopes with a higher pre-booking period than the number of days entered here are excluded from the slot calculation, which takes place every 5 minutes.";

UPDATE `config`
SET `description` = "If this is activated for dev, stage or prod, the slot calculation can be triggered manually via a button after the opening times have been changed." WHERE `name` = "availability__calculateSlotsOnDemand";

UPDATE `config`
SET `description` = "If this is activated for dev, stage or prod, the slots are calculated for an opening time each time they are saved. This can lead to longer waiting times when saving." WHERE `name` = "availability__calculateSlotsOnSave";