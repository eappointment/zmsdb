UPDATE apiclient SET secondsToInvalidation=172800 WHERE secondsToInvalidation < 172800;
INSERT INTO `config` SET `name` = "cron__deleteExpiredApikeyData", `value` = "stage,dev", `description` = "This cronjob allows the deletion of apikeys whose validity period, which is determined by the apiclient, has expired.";
