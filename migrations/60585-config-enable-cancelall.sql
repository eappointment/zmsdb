INSERT INTO `config` SET `name` = "appointments__enableCancelAllFromDay", `value` = "dev", `description` = "If this option is activated, all processes can be canceled at once in the queue view.";
