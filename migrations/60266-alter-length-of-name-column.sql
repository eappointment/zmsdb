ALTER TABLE `nutzer` CHANGE `Name` `Name` VARCHAR(200) NOT NULL DEFAULT '';
ALTER TABLE `kunde` CHANGE `Kundenname` `Kundenname` VARCHAR(200) NOT NULL DEFAULT '';
ALTER TABLE `organisation` CHANGE `Organisationsname` `Organisationsname` VARCHAR(200) NOT NULL DEFAULT '';
ALTER TABLE `behoerde` CHANGE `Ansprechpartner` `Ansprechpartner` VARCHAR(200) NOT NULL DEFAULT '';
ALTER TABLE `standortcluster` CHANGE `name` `name` VARCHAR(200) NOT NULL DEFAULT '';