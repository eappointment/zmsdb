INSERT INTO `config` SET `name` = "mailings__pollingSurveyEmailList", `value` = 'torsten.kunst@berlinonline.de,befragung@statistik-bbb.de,ikt-zms@senatskanzlei.berlin.de,server@service.berlinonline.de';

INSERT INTO `config` SET `name` = "mailings__pollingSurveyEmailMessage", `value` = 'Sehr geehrte Damen und Herren,<br /><br />
anbei befindet sich ein automatisch generierter Report zur Auswertung der Zustimmungen zu Kundenbefragungen der Bürger.<br /><br />
<br /><br />
Bei Problemen oder Fragen wenden Sie sich bitte an die folgende Adresse:<br />
zms-admin@itdz-berlin.de';