DROP TABLE IF EXISTS `remotecall`;
CREATE TABLE `remotecall` (
  `id` BIGINT NOT NULL AUTO_INCREMENT PRIMARY KEY,
  `processArchiveId` VARCHAR(32) CHARACTER SET ascii COLLATE ascii_bin NOT NULL,
  `name` CHAR(80) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `url` CHAR(255) CHARACTER SET utf8 COLLATE utf8_unicode_ci NOT NULL,
  `call__count` INT(5) NOT NULL DEFAULT '0',
  `call__statuscode` INT(3) NOT NULL DEFAULT '0',
  `call__requested` INT(1) NOT NULL DEFAULT '0',
  `call__timestamp` TIMESTAMP NULL,
  INDEX (`name`, `url`)
)
ENGINE = InnoDB
CHARACTER SET utf8
COLLATE utf8_unicode_ci;