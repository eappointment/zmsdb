ALTER TABLE `log` MODIFY COLUMN `expires_on` TIMESTAMP NULL DEFAULT NULL;
CREATE INDEX IF NOT EXISTS type_expires ON `log` (type, expires_on);
CREATE INDEX IF NOT EXISTS type_ts_expires ON `log` (type, ts, expires_on);