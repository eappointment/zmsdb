<?php

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Connection\PdoInterface;
use BO\Zmsdb\Connection\Select as DBConnection;
use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Log as LogRepository;
use DateTimeInterface;
use Exception;

/**
 * @suppressWarnings(complexity)
 * @codeCoverageIgnore
 */
class LogListDeleteByCron
{
    protected const MAX_LOOPS = 5;

    /** @var bool */
    protected bool $verbose = false;

    /** @var int */
    protected int $countLogsByCreateTs = 0;

    /** @var int */
    protected int $countLogsByExpiresOn = 0;

    /** @var DateTimeInterface */
    protected DateTimeInterface $now;

    /**
     * @var int
     */
    protected int $limit;

    private LogRepository $logRepository;

    private PdoInterface $connection;

    public function __construct(DateTimeInterface $now, $verbose = false, $limit = 1000)
    {
        $this->now = $now;
        $this->verbose = $verbose;
        $this->limit = $limit;
        $this->logRepository = new LogRepository();
        DBConnection::setTransaction();
    }

    protected function log($message): void
    {
        if ($this->verbose) {
            error_log($message);
        }
    }

    /**
     * @throws LockTimeout
     * @throws PDOFailed
     * @throws DeadLockFound
     */
    public function startProcessing($commit): void
    {
        $this->connection = DBConnection::getWriteConnection();
        $this->deleteLogsByCreateTimestamp($commit);
        $this->deleteLogsByExpiresOnDate($commit);
        DBConnection::closeWriteConnection();

        $this->log("\nSUMMARY: Deleted expired log entries by create timestamp: " .
            var_export($this->countLogsByCreateTs, true));
        $this->log("SUMMARY: Deleted expired log entries by expires_on date: " .
            var_export($this->countLogsByExpiresOn, true));
    }

    /**
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws Exception
     */
    protected function deleteLogsByCreateTimestamp($commit): void
    {
        $count = 1;
        $retryPdoCount = 0;
        $affectedRows = 0;
        $connectionId = $this->readConnectionId();
        $this->log("Find logs by expired create timestamp (Connection-ID: " . $connectionId . ")");

        do {
            try {
                if ($retryPdoCount > 0) {
                    $this->connection = DBConnection::getWriteConnection();
                    $connectionId = $this->readConnectionId();
                    $this->log("PDO: Neue Connection-ID: " . $connectionId);
                }


                $affectedRows = $this->logRepository->deleteLogListByCreateTimestamp($this->now, $this->limit);
                if ($commit) {
                    DBConnection::writeCommit();
                }
                if ($affectedRows) {
                    $this->countLogsByCreateTs += $affectedRows;
                    $this->log("Loop $count: $affectedRows expired log entries (by create timestamp) deleted: " .
                        var_export($this->countLogsByCreateTs, true));
                    $count++;
                } else {
                    $this->log("Loop $count: 0 expired log entries (by create timestamp) found to delete");
                }
                $retryPdoCount = 0;
                sleep(1);
            } catch (PDOFailed $e) {
                if (str_contains($e->getMessage(), 'server has gone away') || $e->getCode() == 2006) {
                    $retryPdoCount++;
                    if ($retryPdoCount > self::MAX_LOOPS) {
                        throw new Exception("Maximale Anzahl an Wiederholungen erreicht. Verbindung fehlgeschlagen.");
                    }
                    sleep(2);
                    continue;
                } else {
                    throw $e;
                }
            }
        } while ($affectedRows > 0 && $affectedRows >= $this->limit && $count < self::MAX_LOOPS);
    }

    /**
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws Exception
     */
    protected function deleteLogsByExpiresOnDate($commit): void
    {
        $count = 1;
        $retryPdoCount = 0;
        $affectedRows = 0;
        $connectionId = $this->readConnectionId();
        $this->log("Find logs by expires_on date (Connection-ID: " . $connectionId . ")");
        do {
            try {
                if ($retryPdoCount > 0) {
                    $this->connection = DBConnection::getWriteConnection();
                    $connectionId = $this->readConnectionId();
                    $this->log("PDO: Neue Connection-ID: " . $connectionId);
                }

                $affectedRows = $this->logRepository->deleteLogListByExpiresOnDate($this->now, $this->limit);
                if ($commit) {
                    DBConnection::writeCommit();
                }
                if ($affectedRows) {
                    $this->countLogsByExpiresOn += $affectedRows;
                    $this->log("Loop $count: $affectedRows expired log entries (by expires_on date) deleted: " .
                        var_export($this->countLogsByExpiresOn, true));
                    $count++;
                } else {
                    $this->log("Loop $count: 0 expired log entries (by expires_on date) found to delete");
                }
                $retryPdoCount = 0;
                sleep(1);
            } catch (PDOFailed $e) {
                if (str_contains($e->getMessage(), 'server has gone away') || $e->getCode() == 2006) {
                    $retryPdoCount++;
                    if ($retryPdoCount > self::MAX_LOOPS) {
                        throw new Exception("Maximale Anzahl an Wiederholungen erreicht. Verbindung fehlgeschlagen.");
                    }
                    sleep(2);
                    continue;
                } else {
                    throw $e;
                }
            }
        } while ($affectedRows > 0 && $affectedRows >= $this->limit && $count < self::MAX_LOOPS);
    }

    public function getCountByExpireDate(): int
    {
        return $this->countLogsByExpiresOn;
    }

    public function getCountByCreateTs(): int
    {
        return $this->countLogsByCreateTs;
    }

    private function readConnectionId(): int
    {
        $stmt = $this->connection->query("SELECT CONNECTION_ID()");
        return $stmt->fetchColumn();
    }
}
