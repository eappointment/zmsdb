<?php

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Exception\Process\ProcessArchiveUpdateFailed;
use BO\Zmsentities\Process as ProcessEntity;

use BO\Zmsdb\Process as ProcessRepository;

use DateTimeInterface;

/**
 * @codeCoverageIgnore
 */
class AppointmentDeallocateByCron
{
    /** @var bool */
    protected $isDryRun = false;

    /** @var bool */
    protected $verbose = false;

    /** @var int */
    protected $limit = 1000;

    /** @var DateTimeInterface */
    protected $time;
    
    protected $statuslist = [
        "deleted"
    ];

    /** @var int */
    protected $count = 0;

    public function __construct(DateTimeInterface $dateTime, bool $verbose = false, bool $isDryRun = false)
    {
        $this->time = $dateTime;
        $this->verbose = $verbose;
        $this->isDryRun = $isDryRun;
        $this->log("INFO: Release cancelled appointments after the deletion period set in the location");
    }

    protected function log($message): bool
    {
        return $this->verbose && error_log($message);
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setLimit($limit): void
    {
        $this->limit = $limit;
    }

    public function startProcessing(): void
    {
        $this->deallocateProcessList();
        $this->log("\nSUMMARY: Deallocated processes: ". $this->getCount());
    }

    /**
     * Deletion period of the location is written to the IPTimestamp of the appointment
     * in the following query “Query\Process::QUERY_CANCELED“.
     */
    protected function deallocateProcessList()
    {
        $this->log("\nDeallocate cancelled processes");
        $processList = (new ProcessRepository())->readDeallocateProcessList($this->time, $this->limit);
        foreach ($processList as $process) {
            $this->writeDeallocatedProcess($process);
        }
    }

    /**
     * @param ProcessEntity $process
     *
     * @return void
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     * @throws ProcessArchiveUpdateFailed
     */
    protected function writeDeallocatedProcess(ProcessEntity $process): void
    {
        if (in_array($process->status, $this->statuslist)) {
            $this->count++;
            $this->log("INFO: $this->count. deallocate $process");
            if ($this->isDryRun) {
                return;
            }
            $logString = ((new ProcessRepository())->writeBlockedEntity($process)) ?
               "INFO: $this->count. deallocated process $process->id and released slots successfully" :
               "WARN: $this->count. could not deallocate process '$process->id'!";
            $this->log($logString);
        }
    }
}
