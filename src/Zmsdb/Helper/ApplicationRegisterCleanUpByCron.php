<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\ApplicationRegister;
use Psr\Log\LoggerInterface;

class ApplicationRegisterCleanUpByCron
{
    /** @var LoggerInterface */
    private $logger;

    /** @var ApplicationRegister  */
    private $repository;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->repository = new ApplicationRegister();
    }

    public function startProcessing(?\DateTimeInterface $now = null): void
    {
        if (!$now && class_exists('\App') && isset(\App::$now)) {
            $now = \App::$now;
        } elseif (!$now) {
            $now = new \DateTimeImmutable('now');
        }
        $countDeleted = $this->repository->deleteOutdated($now);
        $this->logger->info($countDeleted . ' application register lines have been deleted.');
    }
}
