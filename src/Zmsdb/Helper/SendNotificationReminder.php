<?php

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Exception\Notification\ClientWithoutTelephone;
use BO\Zmsdb\Exception\Process\ProcessUpdateFailed;
use BO\Zmsdb\Log;
use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Notification as NotificationRepository;
use BO\Zmsdb\Department as DepartmentRepository;

use BO\Zmsentities\Notification as NotificationEntity;
use BO\Zmsentities\Process as ProcessEntity;
use DateTimeInterface;

class SendNotificationReminder
{
    /** @var DateTimeInterface */
    protected $dateTime;

    /** @var int */
    protected $reminderInSeconds;

    /** @var bool */
    protected $verbose = false;

    /** @var bool */
    protected $isDryRun = false;

    /** @var int */
    protected $limit = 200;

    /** @var int */
    protected $count = 0;

    public function __construct(DateTimeInterface $now, bool $verbose = false, bool $isDryRun = false)
    {
        $config = (new ConfigRepository())->readEntity();
        $configLimit = $config->getPreference('notifications', 'sqlMaxLimit');
        $this->limit = ($configLimit) ?: $this->limit;
        $this->dateTime = $now;
        $this->isDryRun = $isDryRun;
        $this->verbose = $verbose;
    }

    protected function log($message): bool
    {
        return $this->verbose && error_log($message);
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;
    }

    /**
     * @throws ClientWithoutTelephone
     * @throws ProcessUpdateFailed
     */
    public function startProcessing()
    {
        if (Maintenance::isMaintenanceModeActive()) {
            $this->log("\nINFO: Maintenance mode is active - stop sending reminder");
            return;
        }
        $this->log(
            "\nINFO: Send notification reminder (Limits: ". $this->limit .") dependent on lead time"
        );
        $this->writeNotificationReminderList();
        $this->log("\nINFO: Last run ". $this->dateTime->format('Y-m-d H:i:s'));
        $this->log("SUMMARY: Sent notification reminder: ".$this->count);
    }

    /**
     * @throws ClientWithoutTelephone
     * @throws ProcessUpdateFailed
     */
    protected function writeNotificationReminderList()
    {
        $processList = (new ProcessRepository)->readNotificationReminderProcessList(
            $this->dateTime,
            $this->limit,
            null,
            1
        );
        foreach ($processList as $process) {
            $this->writeReminder($process);
        }
    }

    /**
     * @throws ClientWithoutTelephone
     * @throws ProcessUpdateFailed
     */
    protected function writeReminder(ProcessEntity $process)
    {
        $this->count++;
        $config = (new ConfigRepository)->readEntity();
        $department = (new DepartmentRepository)->readByScopeId($process->getScopeId(), 2);
        $entity = (new NotificationEntity)->toResolvedEntity($process, $config, $department, 'reminder');

        $this->log("INFO: $this->count Create notification: $entity->message");
        if ($this->isDryRun) {
            return;
        }
        if ($notification = (new NotificationRepository)->writeInQueue($entity, $this->dateTime)) {
            Log::writeLogEntry("Write Reminder (Notification::writeInQueue) $entity ", $process->getId());
            $this->log(
                "INFO: $this->count Notification has been written in queue successfully with ID ".
                $notification->getId()
            );
            $process->reminderTimestamp = 0;
            (new ProcessRepository)->updateEntity($process, $this->dateTime);
            $this->log("INFO: $this->count Updated $process->id - reminder timestamp removed");
            return;
        }
        $this->log(
            "WARNING: $this->count Notification for $process->id not possible - no telephone or not enabled"
        );
    }
}
