<?php

namespace BO\Zmsdb\Helper;

use BO\Zmsapi\Exception\Mail\MailSenderFromMissing;
use BO\Zmsdb\Exception\Mail\ClientWithoutEmail;
use BO\Zmsdb\Log as LogRepository;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Mail as MailRepository;
use BO\Zmsdb\Department as DepartmentRepository;
use BO\Zmsdb\Config as ConfigRepository;

use BO\Zmsentities\Collection\ProcessList as Collection;
use BO\Zmsentities\Exception\TemplateNotFound;
use BO\Zmsentities\Config as ConfigEntity;
use BO\Zmsentities\Process as ProcessEntity;

use DateTimeInterface;

/**
 * @SuppressWarnings(CouplingBetweenObjects)
 */
class SendMailReminder
{
    /** @var DateTimeInterface */
    protected $dateTime;

    /** @var DateTimeInterface */
    protected $lastRun;

     /** @var int */
    protected $reminderInSeconds;

    /** @var bool */
    protected $verbose = false;

    /** @var bool */
    protected $isDryRun = false;

    /** @var int */
    protected $limit = 200;

    /** @var int */
    protected $count = 0;

    /** @var ConfigEntity */
    protected $config = null;

    /** @var MailService */
    protected $mailService = null;

    public function __construct(DateTimeInterface $now, $hours = 2, bool $verbose = false, bool $isDryRun = false)
    {
        $this->mailService = new MailService(new MailRepository(), new ConfigRepository());
        $this->config = (new ConfigRepository())->readEntity();
        $configLimit = $this->config->getPreference('mailings', 'sqlMaxLimit');
        $this->limit = ($configLimit) ?: $this->limit;
        $this->dateTime = $now;
        $this->reminderInSeconds = (60 * 60) * $hours;
        $this->lastRun = (new MailRepository)->readReminderLastRun($now);
        $this->isDryRun = $isDryRun;
        $this->verbose = $verbose;
    }

    protected function log($message): bool
    {
        return $this->verbose && error_log($message);
    }

    public function getCount(): int
    {
        return $this->count;
    }

    public function setLimit($limit): void
    {
        $this->limit = $limit;
    }

    /**
     * @throws ClientWithoutEmail|TemplateNotFound
     */
    public function startProcessing(): void
    {
        if (Maintenance::isMaintenanceModeActive()) {
            $this->log("\nINFO: Maintenance mode is active - stop sending reminder");
            return;
        }
        if (! $this->isDryRun) {
            (new MailRepository)->writeReminderLastRun($this->dateTime);
        }
        $this->log(
            "\nINFO: Send email reminder (Limits: ". $this->limit .") dependent on last run: ".
            $this->lastRun->format('Y-m-d H:i:s')
        );

        $this->writeMailReminderList();

        $this->log("\nINFO: Last run ". $this->dateTime->format('Y-m-d H:i:s'));
        $this->log("\nSUMMARY: Sent mail reminder: ".$this->count);
    }

    /**
     * @throws ClientWithoutEmail|TemplateNotFound
     */
    protected function writeMailReminderList(): void
    {
        $processList = (new ProcessRepository)->readEmailReminderProcessListByInterval(
            $this->dateTime,
            $this->lastRun,
            $this->reminderInSeconds,
            $this->limit,
            null,
            2
        );
        foreach ($processList as $process) {
            if ($process->hasId() && $process->getFirstClient()->hasEmail()) {
                $this->writeReminder($process);
            }
        }
    }

    /**
     * @throws ClientWithoutEmail|TemplateNotFound
     * @throws MailSenderFromMissing
     */
    protected function writeReminder(ProcessEntity $process)
    {
        $department = (new DepartmentRepository())->readByScopeId($process->getScopeId());
        if ($department->hasMail()) {
            $this->mailService->setProcessList($this->getProcessListOverview($process));
            $entity = $this->mailService->readResolvedEntity($this->mailService::MAIL_TYPE_REMINDER);
            $this->log(
                "INFO: $this->count. Create mail with process ". $process->getId() .
                " - ". $entity->subject ." for ". $process->getFirstAppointment()
            );
            if ($this->isDryRun || !$entity->hasContent()) {
                return;
            }

            try {
                $entity = $this->mailService->writeMailInQueue($entity, $this->dateTime);
            } catch (\Exception $exception) {
                $email = $process->getFirstClient()->email;
                if ($exception instanceof \BO\Zmsdb\Exception\Mail\RestrictedMail) {
                    $this->log("Warning: $this->count. Failed to write email to ". trim($email) ." - mail restricted");
                    LogRepository::writeLogEntry(
                        "Failed to write email to ". trim($email) .' - mail restricted',
                        $entity->process->getId()
                    );
                }
                $entity = null;
            }

            if ($entity) {
                $this->count++;
                $this->log(
                    "INFO: $this->count. Mail has been written in queue successfully with ID " . $entity->getId()
                );
            }
        }
    }

    protected function getProcessListOverview($process): Collection
    {
        $collection  = (new Collection())->addEntity($process);
        if (in_array(
            getenv('ZMS_ENV'),
            explode(',', $this->config->getPreference('appointments', 'enableSummaryByMail'))
        )) {
            $processList = (new ProcessRepository())->readListByMailAndStatusList(
                $process->getFirstClient()->email,
                [
                    ProcessEntity::STATUS_CONFIRMED,
                    ProcessEntity::STATUS_PICKUP
                ],
                2,
                50
            );
            //add list of found processes without the main process
            $collection->addList($processList->withOutProcessId($process->getId()));
        }
        return $collection->withoutExpiredAppointmentDate($this->dateTime);
    }
}
