<?php

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\EventLog as EventLogRepository;
use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\RemoteCall as RemoteCallRepository;
use BO\Zmsentities\Collection\RemoteCallList;
use BO\Zmsentities\EventLog as EventLogEntity;
use BO\Zmsentities\RemoteCall as RemoteCallEntity;
use DateTimeInterface;
use Psr\Log\LoggerInterface;

/**
 * @codeCoverageIgnore
 * @SuppressWarnings(Coupling)
 */
class SendRemoteCallsByCron
{
    public $configRepository;

    public $eventLogRepository;

    public $remoteCallRepository;

    public $processRepository;

    /** @var bool */
    public $verbose;

    /** @var DateTimeInterface */
    protected $now;

    protected $bdaService;

    /** @var LoggerInterface */
    protected $logger;

    /** @var int */
    protected $countRemoteCallsSent = 0;

    /** @var array */
    protected $requestEvents = [
        EventLogEntity::REMOTECALL_CHANGE_REQUEST,
        EventLogEntity::REMOTECALL_DELETE_REQUEST,
    ];

    /** @var array */
    protected $allowedRemoteCalls = [
        RemoteCallEntity::TYPE_CHANGE,
        RemoteCallEntity::TYPE_DELETE,
    ];

    /**
     * @param                      $bdaService
     * @param bool                 $verbose
     * @param LoggerInterface|null $logger
     */
    public function __construct($bdaService, bool $verbose = false, LoggerInterface $logger = null)
    {
        $this->verbose = $verbose;
        $this->eventLogRepository = new EventLogRepository();
        $this->remoteCallRepository = new RemoteCallRepository();
        $this->processRepository = new ProcessRepository();
        $this->configRepository = new ConfigRepository();
        $this->bdaService = $bdaService;
        $this->logger = $logger ?? new class
        {
            public function info(string $message)
            {
                error_log($message);
            }
        };
    }

    /**
     * @param $message
     *
     * @return void
     */
    protected function log($message)
    {
        if ($this->verbose) {
            $this->logger->info($message);
        }
    }

    /**
     * @param DateTimeInterface $now
     * @param bool              $commit
     *
     * @return void
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function startProcessing(DateTimeInterface $now, bool $commit = false): void
    {
        $this->now = $now;
        $remoteCallList = $this->readRemoteCallList();
        foreach ($remoteCallList as $remoteCall) {
            $this->sendRemoteCall($remoteCall, $commit);
        }
        $this->log("SUMMARY: Handled remotecall entries by event log: " .
            var_export($this->countRemoteCallsSent, true));
    }

    /**
     * @return RemoteCallList
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    protected function readRemoteCallList(): RemoteCallList
    {
        $list = new RemoteCallList();
        foreach ($this->requestEvents as $eventName) {
            $requestEventList = $this->eventLogRepository->readByName($eventName);
            $sentEventList = (array) $this->eventLogRepository->readByName($eventName . '_SENT');
            $sentEventRefs = array_column($sentEventList, 'reference');
            $lastSentArray = [];

            // get last status of repetitive remote call executions
            foreach ($sentEventRefs as $key => $reference) {
                $lastSentArray[$reference] = $sentEventList[$key];
            }

            /** @var EventLogEntity $eventLogEntity */
            foreach ($requestEventList as $eventLogEntity) {
                $reference = $eventLogEntity->reference; // reference is the id of the remote call entity

                if (isset($lastSentArray[$reference])
                    && $lastSentArray[$reference]['creationDateTime'] > $eventLogEntity->creationDateTime
                    && $lastSentArray[$reference]['context']['HTTP_STATUS'] < 400
                ) {
                    continue; // remote call request already sent after requested and it did not fail, so don't list it
                }
                if ($list->getEntity($reference)) {
                    continue; // reduce db load, since the remote call is already in the output list
                }
                $remoteCall = $this->remoteCallRepository->readEntity($eventLogEntity->reference);
                if ($remoteCall->getId()) {
                    $list->addEntity($remoteCall);
                }
            }
        }

        return $list;
    }

    /**
     * @param RemoteCallEntity $remoteCall
     * @param bool             $commit
     *
     * @return void
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    protected function sendRemoteCall(RemoteCallEntity $remoteCall, bool $commit = false): void
    {
        if (! $commit) {
            return;
        }
        $messageName = $this->getMessageNameByType($remoteCall->name);
        try {
            $process = $this->processRepository->readByProcessArchiveId($remoteCall->processArchiveId, 1);
            $content = $this->bdaService
                ->sendRequest($remoteCall->url, $messageName, $process);
            $this->updateRemoteCall($remoteCall, $this->bdaService->getHttpStatusCode());
            $this->logRemoteCallResult($remoteCall, $this->bdaService->getHttpStatusCode());
            $this->countRemoteCallsSent++;
            $this->log("Response: " . $content);
        } catch (\Exception $exception) {
            $this->log('Caught exception: '. $exception->getMessage());
        }
    }

    /**
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     */
    private function updateRemoteCall(RemoteCallEntity $remoteCall, int $statusCode)
    {
        $remoteCall['call']['count']++;
        $remoteCall['call']['timestamp'] == $this->now->getTimestamp();
        $remoteCall['call']['statuscode'] = $statusCode;
        $this->remoteCallRepository->writeUpdatedEntity($remoteCall);
    }

    private function logRemoteCallResult(RemoteCallEntity $remoteCall, int $statusCode)
    {
        $eventLogEntity = new EventLogEntity();
        $eventLogEntity->addData([
            'name' => RemoteCallRepository::getRelatedEventName($remoteCall) . '_SENT',
            'origin' => 'zmsdb',
            'referenceType' => 'remotecall.id',
            'reference' => $remoteCall->id,
            'context' => ['HTTP_STATUS' => $statusCode],
        ])->setSecondsToLive(EventLogEntity::LIVETIME_DAY * 3);

        $this->eventLogRepository->writeEntity($eventLogEntity);
    }

    private function getMessageNameByType(string $type): string
    {
        if ($type == RemoteCallEntity::TYPE_CHANGE) {
            return $this->bdaService::MESSAGE_TYPE_CHANGE;
        }
        if ($type == RemoteCallEntity::TYPE_DELETE) {
            return $this->bdaService::MESSAGE_TYPE_DELETE;
        }
        return '';
    }
}
