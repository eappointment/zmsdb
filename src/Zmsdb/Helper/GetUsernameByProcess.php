<?php

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Log as LogRepository;
use BO\Zmsdb\Useraccount as UseraccountRepository;
use BO\Zmsentities\Collection\UseraccountList;
use DateTime;
use DateTimeZone;

/**
 * @codeCoverageIgnore
 * @SuppressWarnings(Complexity)
 */
class GetUsernameByProcess
{
    private UseraccountRepository $userRepository;

    private LogRepository $logRepository;

    private bool $verbose;

    protected array $processIdList = [];

    protected array $matchedProcessList = [];


    public function __construct(
        string $processId = null,
        bool $verbose = false
    ) {
        $this->verbose = $verbose;
        $this->userRepository = new UseraccountRepository();
        $this->logRepository = new LogRepository();
        $this->processIdList = ($processId) ? explode(',', $processId) : [];
        $this->log("Prüfe die Liste angegebener Vorgänge, von welchem Nutzer diese angelegt/bearbeitet wurden.");
    }

    /**
     * @return void
     */
    public function startProcessing(): void
    {
        $useraccountIds = $this->userRepository->readList();
        if (count($this->processIdList) > 0) {
            $this->testProcessList($useraccountIds);
        }

        if (count($this->matchedProcessList) == 0) {
            $this->log(
                "\033[32mNutzer konnte nicht ermittelt werden\033[0m\n"
            );
        }

        foreach ($this->matchedProcessList as $processId => $result) {
            foreach ($result as $item) {
                $dateTime = (new DateTime("@" . $item['date']))->modify("+ 2hours");
                $dateTime->setTimezone(new DateTimeZone('Europe/Berlin'));
                $dateString = $dateTime->format('Y-m-d H:i:s');
                $this->log(
                    "\033[31mVorgang " . $processId . " wurde vom Nutzer " . $item['user'] . " am " .
                    $dateString . " bearbeitet (" . substr($item['message'], 0, 50) . "...) \033[0m"
                );
            }
        }
    }

    /**
     * @param UseraccountList $useraccountList
     * @return void
     */
    protected function testProcessList(UseraccountList $useraccountList): void
    {
        foreach ($this->processIdList as $processId) {
            foreach ($useraccountList as $useraccount) {
                $hashWithLoginName = sha1($processId . '-' . $useraccount->getId()); //zms2
                $userId = $this->userRepository->readEntityIdByLoginName($useraccount->getId());
                $hashWithUserId = sha1($processId . '-' . $userId); //zms1
                if ($this->verbose) {
                    $this->log(
                        "Prüfe Hash für Vorgang " . $processId . ' und Nutzername ' .
                        $useraccount->getId() . ': ' . $hashWithLoginName
                    );
                    $this->log(
                        "Prüfe Hash für Vorgang " . $processId . ' und Nutzerid ' .
                        $userId . ': ' . $hashWithUserId
                    );
                }

                $logList = $this->logRepository->readByReferenceId($processId);
                foreach ($logList as $log) {
                    if (str_contains($log->message, $hashWithLoginName) ||
                        str_contains($log->message, $hashWithUserId)
                    ) {
                        $this->matchedProcessList[$processId][] = [
                            "user" => $useraccount->getId(),
                            "date" => $log->ts,
                            "message" => $log->message
                        ];
                        break;
                    }
                }
            }
        }
    }

    protected function log($message): void
    {
        error_log($message);
    }
}
