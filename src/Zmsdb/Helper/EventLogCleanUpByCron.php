<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\EventLog as EventLogRepository;
use Psr\Log\LoggerInterface;

class EventLogCleanUpByCron
{
    protected $verbose = false;

    protected $logger;
    
    public function __construct($verbose = false, LoggerInterface $logger = null)
    {
        $this->verbose = $verbose;
        $this->logger = $logger ?? new class
        {
            public function info(string $message)
            {
                error_log($message);
            }
        };
        $this->log("INFO: Delete old eventlog entries");
    }

    protected function log($message)
    {
        if ($this->verbose) {
            $this->logger->info($message);
        }
    }

    public static function startProcessing($commit = false, \DateTimeInterface $now = null)
    {
        $now = $now ?? new \DateTime();
        $eventLogRepo  = new EventLogRepository();
        if ($commit) {
            $eventLogRepo->deleteOutdated($now);
        }
    }
}
