<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\MaintenanceSchedule as ScheduleRepository;
use BO\Zmsentities\Collection\MaintenanceScheduleList as ScheduleList;
use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\Config;
use BO\Zmsentities\MaintenanceSchedule as ScheduleEntity;
use BO\Zmsentities\Helper\MaintenanceSchedule as EntityHelper;
use BO\Zmsentities\Schema\Entity;
use DateTimeInterface;

class MaintenanceSettlement
{
    /** @var ConfigRepository */
    private $configRepo;

    /** @var ScheduleRepository */
    private $scheduleRepo;

    public function __construct(?ConfigRepository $configRepo = null, ?ScheduleRepository $scheduleRepo = null)
    {
        $this->configRepo = $configRepo ?? new ConfigRepository();
        $this->scheduleRepo = $scheduleRepo ?? new ScheduleRepository();
    }

    /**
     * @return ScheduleEntity|null (Entity is not the one from MaintenanceScheduleList but has its ID)
     */
    public function getPlannedMaintenance(): ?ScheduleEntity
    {
        $configEntry = $this->configRepo->readProperty(Config::PROPERTY_MAINTENANCE_NEXT);

        if (!empty($configEntry)) {
            return new ScheduleEntity(json_decode($configEntry, true));
        }

        return null;
    }

    /**
     * @SuppressWarnings(Complexity)
     */
    public function scheduleNextMaintenance(
        ScheduleList $scheduleList,
        ?DateTimeInterface $nowTime = null
    ): void {
        $defaultTime = DateTime::create()->setTimestamp(Entity::getCurrentTimestamp());
        $nowTime = $nowTime ? DateTime::create($nowTime) : $defaultTime;
        $inSchedule   = $this->getPlannedMaintenance();

        // the planned maintenance schedule entry has been deleted, so reset the plan
        if ($inSchedule
            && (!$scheduleList->getEntity($inSchedule->getId())
                || !EntityHelper::getNextRunTime($scheduleList->getEntity($inSchedule->getId()))
                || $inSchedule->getStartDateTime()->getTimestamp()
                    != EntityHelper::getNextRunTime($scheduleList->getEntity($inSchedule->getId()))->getTimestamp()
            )
        ) {
            $this->configRepo->replaceProperty(Config::PROPERTY_MAINTENANCE_NEXT, '');
            $inSchedule = null;
        }

        if ((int) $nowTime->format('s') > 0) {
            $nowTime = $nowTime->modify('-' . $nowTime->format('s') . ' seconds'); // ignore seconds by make it zero
        }
        $filteredList = $this->getSortedSuccessorArray($scheduleList, $nowTime);

        if (count($filteredList) === 0 && !$inSchedule) {
            return;
        } elseif ($inSchedule && count($filteredList) === 0) {
            $this->configRepo->replaceProperty(Config::PROPERTY_MAINTENANCE_NEXT, '');

            return;
        }

        /** @var ScheduleEntity $scheduleNext */
        $scheduleNext  = reset($filteredList);
        if ($inSchedule && $this->entriesEqual($inSchedule, $scheduleNext)) {
            return;
        }

        $scheduleNext['startDateTime'] = EntityHelper::getNextRunTime($scheduleNext, $nowTime);
        $reducedData = EntityHelper::getReducedArray($scheduleNext);

        $this->configRepo->replaceProperty(
            Config::PROPERTY_MAINTENANCE_NEXT,
            json_encode($reducedData),
            "Don't change this! This is the next scheduled maintenance data"
        );
    }

    /**
     * @return bool (true when maintenance was started, else false)
     */
    public function checkActivation(ScheduleList $scheduleList, ?DateTimeInterface $nowTime = null): bool
    {
        $defaultTime  = DateTime::create()->setTimestamp(Entity::getCurrentTimestamp());
        $nowTime      = $nowTime ? DateTime::create($nowTime) : $defaultTime;
        $plannedEntry = $this->getPlannedMaintenance();
        $activeEntry  = $scheduleList->getActiveEntry();

        if ($plannedEntry
            && $plannedEntry->getStartDateTime()->getTimestamp() <= $nowTime->getTimestamp()
            && $scheduleList->getEntity($plannedEntry->getId())
        ) {
            /** @var ScheduleEntity $scheduleEntity */
            $scheduleEntity = $scheduleList->getEntity($plannedEntry->getId());

            if ($activeEntry && $scheduleEntity->getId() !== $activeEntry->getId()) {
                $activeEntryEnd = $activeEntry->getStartDateTime()->getTimestamp();
                $activeEntryEnd += $activeEntry->getDuration() * 60;
                if ($activeEntryEnd >= $nowTime->getTimestamp() + $plannedEntry->getDuration() * 60) {
                    return false; // the active maintenance is overlapping the planned one completely
                }
                $this->deactivateMaintenance($activeEntry);
            }

            $scheduleEntity->setActive(true);
            $scheduleEntity->setStartDateTime($plannedEntry->getStartDateTime());
            $this->scheduleRepo->updateEntity($scheduleEntity);
            $this->configRepo->replaceProperty(Config::PROPERTY_MAINTENANCE_NEXT, '');

            return true;
        }

        return false;
    }

    public function deactivateMaintenance(?ScheduleEntity $scheduleEntry = null): void
    {
        if ($scheduleEntry === null) {
            $scheduleList = $this->scheduleRepo->readList();
            $activeEntry  = $scheduleList->getActiveEntry();

            if ($activeEntry === null) {
                return;
            }
        } else {
            $activeEntry = $scheduleEntry;
        }

        $activeEntry->setActive(false);
        $activeEntry->setStartDateTime(null);
        $this->scheduleRepo->updateEntity($activeEntry);
    }

    public function getMaintenanceEndTime(ScheduleEntity $activeEntry): ?DateTimeInterface
    {
        $startTime = $activeEntry->getStartDateTime()->modify('-1 seconds');
        return  EntityHelper::getNextRunEnd($activeEntry, $startTime);
    }

    private function getSortedSuccessorArray(ScheduleList $list, DateTime $nowTime): array
    {
        $sortedByEnd = (array) ($list->sortByScheduleStartTime($nowTime));
        return array_filter($sortedByEnd, function (ScheduleEntity $entry) use ($nowTime) {
            return EntityHelper::getNextRunTime($entry, $nowTime) !== null;
        });
    }
    
    public function entriesEqual(ScheduleEntity $entity1, ScheduleEntity $entity2): bool
    {
        return $entity1->getId() === $entity2->getId()
            && $entity1->getStartDateTime()
            && $entity2->getStartDateTime()
            && $entity1->getStartDateTime()->getTimestamp() === $entity2->getStartDateTime()->getTimestamp();
    }
}
