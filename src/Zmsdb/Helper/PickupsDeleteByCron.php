<?php

namespace BO\Zmsdb\Helper;

/**
 * @codeCoverageIgnore
 */

use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Scope as ScopeRepository;
use BO\Zmsentities\Collection\ScopeList;
use BO\Zmsentities\Process;
use BO\Zmsentities\Scope;

class PickupsDeleteByCron
{
    /** @var \DateTimeInterface */
    protected $nowTime;

    /** @var bool */
    protected $verbose = false;

    /** @var int */
    protected $limit = 500;

    /** @var bool */
    protected $isDryRun = false;

    /** @var int[] */
    protected $countByScopeId = [];

    /** @var Scope */
    protected $scopeRepository;

    protected $processRepository;

    public function __construct(\DateTimeInterface $now, bool $verbose = false, bool $isDryRun = false)
    {
        $this->nowTime  = $now;
        $this->verbose  = $verbose;
        $this->isDryRun = $isDryRun;

        $this->scopeRepository = new ScopeRepository();
        $this->processRepository = new ProcessRepository();
    }

    /**
     * @return int[]
     */
    public function getCount(): array
    {
        return $this->countByScopeId;
    }

    public function setLimit(int $limit): PickupsDeleteByCron
    {
        $this->limit = $limit;

        return $this;
    }

    public function startProcessing(): void
    {
        $this->log("INFO: Deleting expired pickups older than 1 year");

        $scopeList = $this->scopeRepository->readList();
        $this->countByScopeId = array_fill_keys($scopeList->getIds(), 0);

        $this->deleteExpiredPickups($scopeList);
        $filteredCount = array_filter($this->getCount());

        $this->log(PHP_EOL . "SUMMARY: Processed pickups: " . var_export($filteredCount, true));
    }

    protected function log($message): bool
    {
        return $this->verbose && error_log($message);
    }

    protected function deleteExpiredPickups(ScopeList $scopeList): void
    {
        foreach ($scopeList as $scope) {
            $countedProcesses = $this->deleteProcessesByScope($scope);
            $this->countByScopeId[$scope->id] += $countedProcesses;
        }
    }

    protected function getExpirationTime(): \DateTimeInterface
    {
        $expirationTime = clone $this->nowTime;
        return $expirationTime->modify('- 1year');
    }

    protected function deleteProcessesByScope(Scope $scope): int
    {
        $processCount = 0;
        $processList  = $this->getProcessListByScope($scope);

        foreach ($processList as $process) {
            if ($process->status === Process::STATUS_PENDING) {
                $processTimestamp = $process->getFirstAppointment()->toDateTime()->getTimestamp();
                $age = round(($this->nowTime->getTimestamp() - $processTimestamp) / 60 / 60 / 24);
                $this->log("INFO: found process($process->id) with an age of $age days");
                $this->writeDeleteProcess($process);
            } else {
                $this->log("INFO: Keep process $process->id with status $process->status");
            }

            $processCount++;
        }

        return $processCount;
    }

    protected function getProcessListByScope(Scope $scope): iterable
    {
        $expirationTime = $this->getExpirationTime();
        $processList = $this->processRepository
            ->readExpiredPickupsList($expirationTime, $scope->getId(), $this->limit)
            ->sortByAppointmentDate();

        if ($processList->count() === 0) {
            return [];
        }

        $this->log(
            "\nNow: ". $this->nowTime->format('H:i:s') .
            "\nTime of expiration: ". $expirationTime->format('Y-m-d H:i:s') ." | scope ". $scope->id .
            " | 1 year (". $processList->count() . " pending process(es) found)" .
            "\n-------------------------------------------------------------------"
        );

        return $processList;
    }

    protected function writeDeleteProcess(Process $process)
    {
        if ($this->isDryRun) {
            return;
        }

        if ($this->processRepository->writeDeletedEntity($process->id)) {
            $this->log("INFO: ($process->id) removed successfully\n");
        } else {
            $this->log("WARN: Could not remove process '$process->id'!\n");
        }
    }
}
