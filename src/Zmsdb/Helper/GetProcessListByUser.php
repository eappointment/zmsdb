<?php

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Log as LogRepository;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Useraccount as UseraccountRepository;
use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Collection\ScopeList;

use DateInterval;
use DatePeriod;
use DateTimeZone;
use DateTime;
use DateTimeImmutable;
use DateTimeInterface;

/**
 * @codeCoverageIgnore
 * @SuppressWarnings(Complexity)
 */
class GetProcessListByUser
{
    private ProcessRepository $processRepository;
    private UseraccountRepository $userRepository;

    private LogRepository $logRepository;

    private bool $verbose;

    protected string $username;

    protected ?DateTimeInterface $startDate;

    protected ?DateTimeInterface $endDate;

    protected array $processIdList = [];

    protected ScopeList $scopeList;

    protected ProcessList $processList;

    protected array $matchedProcessList = [];

    public function __construct(
        string $username,
        string $processId = null,
        ?DateTimeInterface $startDate = null,
        ?DateTimeInterface $endDate = null,
        bool $verbose = false
    ) {
        $this->verbose = $verbose;
        $this->userRepository = new UseraccountRepository();
        $this->logRepository = new LogRepository();
        $this->processRepository = new ProcessRepository();
        $this->startDate = $startDate;
        $this->endDate = $endDate;
        $this->username = $username;
        $this->processIdList = ($processId) ? explode(',', $processId) : [];

        if ($startDate && $endDate && !$processId) {
            $this->log("Prüfe alle Vorgänge zwischen ".
                $this->startDate->format('Y-m-d') . " - " .
                $this->endDate->format('Y-m-d') .
                ", ob diese vom Nutzer $this->username angelegt wurden.");
        } elseif ($processId) {
            $this->log("Prüfe die Liste angegebener Vorgänge, ob diese vom Nutzer $this->username angelegt wurden.");
        }
    }

    public function startProcessing(): void
    {
        $useraccount = $this->userRepository->readEntity($this->username);
        if (!$useraccount->hasId()) {
            $this->log("Kein Nutzer zu diesem Nutzernamen ($this->username) gefunden.");
            return;
        }
        $this->scopeList = $useraccount->getDepartmentList()->getUniqueScopeList();
        if ($this->verbose) {
            $this->log("Nutzername: " . $useraccount->getId());
            $this->log("Letzter Login: " . (new DateTimeImmutable())
                    ->setTimestamp($useraccount->lastLogin)
                    ->format('Y-m-d H:i:s'));
            $this->log("Zugewiesene Standorte: " . $this->scopeList->getIdsCsv());
        }

        if (count($this->processIdList) > 0) {
            $this->testProcessInLogList($useraccount->getId());
        } else {
            $interval = DateInterval::createFromDateString('1 day'); // Intervall von einem Tag
            $period = new DatePeriod($this->startDate, $interval, $this->endDate->modify('+1 day'));
            foreach ($period as $date) {
                $this->log('Lese Vorgänge für das Datum '. $date->format('Y-m-d'));
                $processList = $this->readProcessListByDate($date);
                if ($processList->count() > 0) {
                    $this->processIdList = array_merge($this->processIdList, $processList->getIds());
                }
            }
            $this->testProcessInLogList($useraccount->getId());
        }


        if (count($this->matchedProcessList) == 0) {
            $this->log(
                "\033[32mKeine passenden Vorgänge durch Nutzer " .
                $useraccount->getId() . " angelegt/bearbeitet\033[0m\n"
            );
        }

        foreach ($this->matchedProcessList as $processId => $result) {
            foreach ($result as $item) {
                $dateTime = (new DateTime("@" . $item['date']))->modify("+ 2hours");
                $dateTime->setTimezone(new DateTimeZone('Europe/Berlin'));
                $dateString = $dateTime->format('Y-m-d H:i:s');
                $this->log(
                    "\033[31mVorgang " . $processId . " wurde vom Nutzer " . $item['user'] . " am " .
                    $dateString . " bearbeitet (" . substr($item['message'], 0, 50) . "...) \033[0m"
                );
            }
        }
    }

    protected function testProcessInLogList($loginName): void
    {
        foreach ($this->processIdList as $processId) {
            $hashWithLoginName = sha1($processId . '-' . $loginName); //zms2
            $userId = $this->userRepository->readEntityIdByLoginName($loginName);
            $hashWithUserId = sha1($processId . '-' . $userId); //zms1
            if ($this->verbose) {
                $this->log(
                    "Prüfe Hash für Vorgang " . $processId . ' und Nutzername ' .
                    $loginName . ': ' . $hashWithLoginName
                );
                $this->log(
                    "Prüfe Hash für Vorgang " . $processId . ' und Nutzerid ' .
                    $userId . ': ' . $hashWithUserId
                );
            }
            $logList = $this->logRepository->readByReferenceId($processId);
            foreach ($logList as $log) {
                if (str_contains($log->message, $hashWithLoginName) ||
                    str_contains($log->message, $hashWithUserId)
                ) {
                    $this->matchedProcessList[$processId][] = [
                        "user" => $loginName,
                        "date" => $log->ts,
                        "message" => $log->message
                    ];
                    break;
                }
            }
        }
    }

    protected function readProcessListByDate(\DateTimeInterface $date)
    {
        $processList = new ProcessList();
        foreach ($this->scopeList->getIds() as $scopeId) {
            $list = $this->processRepository->readProcessListByScopeAndTime($scopeId, $date);
            $processList->addList($list);
        }
        return $processList;
    }


    protected function log($message): void
    {
        error_log($message);
    }
}
