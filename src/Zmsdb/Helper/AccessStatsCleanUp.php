<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\AccessStats;
use DateTimeInterface;
use Psr\Log\LoggerInterface;

class AccessStatsCleanUp
{
    /** @var LoggerInterface */
    private $logger;

    /** @var AccessStats  */
    private $repository;

    /**
     * @var DateTimeInterface
     */
    private $now;

    public function __construct(LoggerInterface $logger, DateTimeInterface $now = null)
    {
        $this->logger = $logger;
        $this->repository = new AccessStats();
        $this->now = $now ?? new \DateTime();
    }

    public function startProcessing(): void
    {
        $countDeleted = $this->repository->freshenList($this->now);
        $this->logger->info($countDeleted . ' accessstats table lines have been deleted.');
    }
}
