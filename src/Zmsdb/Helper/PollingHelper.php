<?php

declare(strict_types=1);

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Mail as MailRepository;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Availability as AvailabilityRepository;
use BO\Zmsdb\Query\Process as ProcessQueryRepo;
use BO\Zmsdb\Query\Availability as AvailabilityQueryRepo;
use BO\Zmsdb\Query\MailQueue as MailQueryRepo;
use BO\Zmsentities\Helper\Polling\PollingDistance;
use BO\Zmsentities\Helper\Polling\PollingDistanceList;

/**
 * @codeCoverageIgnore
 */
class PollingHelper
{
    /** @var bool */
    protected bool $isDryRun = false;

    /** @var bool */
    protected bool $verbose = false;

    /** @var string */
    protected string $dateString;

    /** @var ConfigRepository */
    private ConfigRepository $configRepository;


    public function __construct(string $dateString)
    {
        $this->dateString = $dateString;
        $this->configRepository = new ConfigRepository();
    }

    public function startProcessingDistanceReport(): PollingDistanceList
    {
        $processRepository = new ProcessRepository();
        $resultList = $processRepository->fetchAll(ProcessQueryRepo::QUERY_POLLING_APPOINTMENT_DISTANCE, [
            'date' => $this->dateString,
        ]);
        $pollingDistanceList = new PollingDistanceList();
        foreach ($resultList as $result) {
            $pollingDistanceList[] = new PollingDistance($result);
        }
        return $pollingDistanceList;
    }

    public function getAvailabilityReviewData(): array
    {
        $availabilityRepo = new AvailabilityRepository();
        return $availabilityRepo->fetchAll(AvailabilityQueryRepo::QUERY_POLLING_AVAILABILITY_REVIEW, [
            'date' => $this->dateString,
        ]);
    }

    public function getGroupedEmailData(): array
    {
        $resultList = [];
        $mailRepo = new MailRepository();
        $resultList['ba'] = $mailRepo->fetchAll(MailQueryRepo::QUERY_POLLING_GROUPED_EMAILS);
        $resultList['kfz'] = $mailRepo->fetchAll(MailQueryRepo::QUERY_POLLING_GROUPED_EMAILS_KFZ);
        return $resultList;
    }

    public function getSurveysData(): array
    {
        $processRepo = new ProcessRepository();
        return $processRepo->fetchAll(ProcessQueryRepo::QUERY_POLLING_SURVEYS);
    }
}
