<?php

namespace BO\Zmsdb\Helper;

use BO\Mellon\Failure\Exception as MellonFailureException;
use BO\Mellon\Validator;

use BO\Zmsapi\Exception\Mail\MailRecipientMissing as MailRecipientMissingException;
use BO\Zmsapi\Exception\Mail\MailSenderFromMissing as MailSenderFromMissingException;
use BO\Zmsapi\Exception\Process\ProcessListSummaryTooOften as ProcessListSummaryTooOftenException;
use BO\Zmsapi\Exception\Mail\MailNotFound as MailNotFoundException;
use BO\Zmsapi\Exception\Mail\MailDeleteFailed as MailDeleteFailedException;

use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Department as DepartmentRepository;
use BO\Zmsdb\EventLog as EventLogRepository;
use BO\Zmsdb\Exception\Mail\RestrictedMail as RestrictedMailException;
use BO\Zmsdb\Log as LogRepository;
use BO\Zmsdb\Mail as MailRepository;
use BO\Zmsdb\Exception\Mail\ClientWithoutEmail as ClientWithoutEmailException;

use BO\Zmsentities\Department as DepartmentEntity;
use BO\Zmsentities\Exception\TemplateNotFound;
use BO\Zmsentities\Helper\DateTime as DateTimeHelper;
use BO\Zmsentities\Mail as MailEntity;
use BO\Zmsentities\Schema\Entity as SchemaEntity;
use BO\Zmsentities\Scope as ScopeEntity;
use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\EventLog as EventLogEntity;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;

/**
 * @SuppressWarnings(coupling)
 */
class MailService
{
    const MAIL_TYPE_REMINDER = 'reminder';
    const MAIL_TYPE_DELETE = 'deleted';
    const MAIL_TYPE_APPOINTMENT = 'appointment';
    const MAIL_TYPE_SPONTANEOUS = 'queued';
    const MAIL_TYPE_OVERVIEW = 'overview';
    const MAIL_TYPE_SURVEY = 'survey';
    const MAIL_TYPE_UPDATE = 'updated';

    /**
     * @var MailRepository
     */
    private $mailRepository;

    /**
     * @var ConfigRepository
     */
    private $configRepository;

    /**
     * @var DepartmentRepositoryRepository
     */
    private $departmentRepository;

    /**
     * @var string
     */
    protected $mailAddress;

    /**
     * @var string
     */
    protected $initiator;

    /**
     * @var ProcessList
     */
    protected $processList;

    /**
     * @param MailRepository            $mailRepository
     * @param ConfigRepository          $configRepository
     * @param DepartmentRepository|null $departmentRepository
     */
    public function __construct(
        MailRepository $mailRepository,
        ConfigRepository $configRepository,
        DepartmentRepository $departmentRepository = null
    ) {
        $this->configRepository = $configRepository;
        $this->mailRepository = $mailRepository;
        $this->departmentRepository = $departmentRepository;
    }

    /**
     * @param string $mailAddress
     *
     * @return void
     */
    public function setMailAddress(string $mailAddress): void
    {
        $this->mailAddress = $mailAddress;
    }

    /**
     *
     * @return string
     */
    public function getMailAddress(): string
    {
        return $this->mailAddress;
    }

    /**
     * @param string $initiator
     *
     * @return void
     */
    public function setInitiator(string $initiator): void
    {
        $this->initiator = $initiator;
    }

    /**
     *
     * @return string
     */
    public function getInitiator(): string
    {
        return $this->initiator;
    }

    public function setProcessList(ProcessList $processList): void
    {
        $this->processList = $processList;
    }

    public function getProcessList(): ProcessList
    {
        return $this->processList;
    }

    /**
     * @param MailEntity                          $entity
     * @param DateTimeInterface|DateTimeImmutable $dateTime
     * @param bool                                $count
     *
     * @return MailEntity|null
     * @throws ClientWithoutEmailException|MailSenderFromMissingException
     */
    public function writeMailInQueue(MailEntity $entity, DateTimeInterface $dateTime, bool $count = true): ?MailEntity
    {
        $entity->testValid();
        $email = $entity->getFirstClient()->toProperty()->email->get();
        if ($this->testValidMail($email)) {
            $entity = (!$this->departmentRepository) ? $entity : $entity
                ->withDepartment($this->readDepartment($entity->process->getScopeId()));
            $mail = $this->mailRepository->writeInQueue($entity, $dateTime, $count);
            if ($mail->hasId()) {
                $logString = "mail#" . (($mail->hasId()) ? $mail->getId() : 0) .
                    " subject: $mail->subject .".
                    " process#" . $mail->getProcessId();
                LogRepository::writeLogEntry(
                    "Write (Mail::writeInQueue) $logString ",
                    $mail->getProcessId() ? $mail->getProcessId() : 0
                );
                return $mail;
            }
        }
        LogRepository::writeLogEntry(
            "Failed to write email to ". trim($email) .' - mail not valid',
            $entity->process->getId()
        );
        return null;
    }

    public function writeInQueueWithAdmin(MailEntity $entity): ?MailEntity
    {
        $entity->testValid();
        $email = $entity->process->scope->toProperty()->contact->email->get();
        if ($this->testValidMail($email)) {
            return $this->mailRepository->writeInQueueWithAdmin($entity);
        }
        LogRepository::writeLogEntry(
            "Failed to write email to ". trim($email) .' - mail not valid',
            $entity->process->getId()
        );
        return null;
    }

    /**
     * @param $entityId
     *
     * @return MailEntity|SchemaEntity
     */
    public function writeMailDelete($entityId)
    {
        $mail = $this->mailRepository->readEntity($entityId);
        if ($mail && !$mail->hasId()) {
            throw new MailNotFoundException();
        }
        if (! $this->mailRepository->deleteEntity($entityId)) {
            throw new MailDeleteFailedException();
        }
        return $mail;
    }

    /**
     * @param string $mailType
     *
     * @return MailEntity
     * @throws TemplateNotFound
     */
    public function readResolvedEntity(string $mailType): MailEntity
    {
        $configEntity = $this->configRepository->readEntity();
        $entity = (new MailEntity())->toResolvedEntity($this->processList, $configEntity, $mailType, $this->initiator);
        if ($mailType == self::MAIL_TYPE_OVERVIEW && $this->mailAddress) {
            $entity->withProcessClient($this->mailAddress);
        }
        return $entity;
    }

    /**
     * @param string $mailValue
     *
     * @return bool
     */
    public function testValidMail(string $mailValue = ''): bool
    {
        if ($this->isRestrictedEmail($mailValue) || $this->isRestrictedDomain($mailValue)) {
            throw new RestrictedMailException();
        }

        $isValid = true;
        try {
            Validator::value(trim($mailValue))
                ->isMail()
                ->hasDNS()
                ->assertValid();
        } catch (MellonFailureException $exception) {
            $isValid = false;
        }
        return $isValid;
    }

    protected function isRestrictedEmail(string $mailValue): bool
    {
        return in_array($mailValue, $this->getRestrictedMailList());
    }

    protected function isRestrictedDomain(string $mailValue): bool
    {
        $emailParts = explode('@', $mailValue);
        if (count($emailParts) !== 2) {
            return false;
        }
        $domain = trim($emailParts[1]);
        return in_array($domain, $this->getRestrictedDomainList());
    }

    /**
     * @param ScopeEntity       $scope
     * @param ProcessList       $processList
     * @param DateTimeInterface $dateTime
     *
     * @return void
     */
    public function writeInQueueWithDailyProcessList(
        ScopeEntity $scope,
        DateTimeInterface $dateTime
    ) {
        $entity = (new MailEntity())->toScopeAdminProcessList($this->processList, $scope, $dateTime);
        $this->mailRepository->writeInQueueWithDailyProcessList($scope, $entity);
    }

    /**
     * @param string $name
     * @param string $origin
     * @param int    $secondsToLive
     *
     * @return void
     */
    public function writeEventLogEntry(string $name, string $origin, int $secondsToLive): void
    {
        if (!$this->processList || !$this->mailAddress) {
            return;
        }
        $eventLogRepository = new EventLogRepository();
        $eventLogEntity = new EventLogEntity();
        $eventLogEntity->addData([
            'name' => $name,
            'origin' => $origin,
            'referenceType' => 'mail.recipient.hash',
            'reference' => $eventLogRepository->hashStringValue($this->mailAddress),
            'context' => ['found' => $this->processList->getIds()],
        ])->setSecondsToLive($secondsToLive);

        $eventLogRepository->writeEntity($eventLogEntity);
    }

    /**
     * @throws ProcessListSummaryTooOftenException
     * @throws MailRecipientMissingException
     * @throws Exception
     */
    public function testEventLogEntries($name, $repetitionInSeconds)
    {
        if (!$this->mailAddress) {
            throw new MailRecipientMissingException();
        }
        $logRepository = new EventLogRepository();
        $eventLogEntries = $logRepository->readByNameAndRef(
            $name,
            $logRepository->hashStringValue($this->mailAddress)
        );
        $youngestTime = (new DateTimeHelper())->setTimestamp(\App::$now->getTimestamp() - $repetitionInSeconds);
        if ($eventLogEntries->count() > 0 && $eventLogEntries->getLast()->creationDateTime > $youngestTime) {
            throw new ProcessListSummaryTooOftenException();
        }
    }

    /**
     * @param int|null $scopeId
     *
     * @return DepartmentEntity
     * @throws MailSenderFromMissingException
     */
    protected function readDepartment(int $scopeId = null): DepartmentEntity
    {
        $noReplyDepartmentId = $this->configRepository->readProperty('mailings__noReplyDepartmentId');
        $department = ($scopeId) ?
            $this->departmentRepository->readByScopeId($scopeId) :
            $this->departmentRepository->readEntity($noReplyDepartmentId);
        if (null === $department) {
            throw new MailSenderFromMissingException();
        }
        return $department;
    }

    /**
     * @return array
     */
    protected function getRestrictedMailList(): array
    {
        $restrictedMailCsv = $this->configRepository->readProperty('notifications__blacklistedAddressList');
        $mailList = explode(',', $restrictedMailCsv);
        return array_map('trim', $mailList);
    }

    /**
     * @return array
     */
    protected function getRestrictedDomainList(): array
    {
        $restrictedDomainsCsv = $this->configRepository->readProperty('notifications__restrictedDomainList');
        $domainList = explode(',', $restrictedDomainsCsv);
        return array_map('trim', $domainList);
    }
}
