<?php

/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Apiclient as ApiclientRepository;
use BO\Zmsdb\Apikey as ApikeyRepository;
use Psr\Log\LoggerInterface;
use DateTimeInterface;

class ApikeyDeleteByCron
{
    /**
     * @var bool
     */
    protected $verbose = false;

    /**
     * @var bool
     */
    protected $isDryRun = true;

    /**
     * @var DateTimeInterface
     */
    protected $now;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @var ApiclientRepository
     */
    protected $apiclientRepository;

    /**
     * @var ApikeyRepository
     */
    protected $apikeyRepository;

    /**
     * @param DateTimeInterface    $now
     * @param bool                 $verbose
     * @param bool                 $isDryRun
     * @param LoggerInterface|null $logger
     */
    public function __construct(
        DateTimeInterface $now,
        bool $verbose = false,
        bool $isDryRun = true,
        LoggerInterface $logger = null
    ) {
        $this->apiclientRepository = new ApiclientRepository();
        $this->apikeyRepository = new ApikeyRepository();
        $this->now = $now;
        $this->verbose = $verbose;
        $this->isDryRun = $isDryRun;
        $this->logger = $logger ?? new class
        {
            public function info(string $message)
            {
                error_log($message);
            }
        };
        $this->log("INFO: Delete expired apikey entries older than '{$this->now->format('Y-m-d H:i:s')}'");
    }

    /**
     * @param $message
     *
     * @return void
     */
    protected function log($message)
    {
        if ($this->verbose) {
            $this->logger->info($message);
        }
    }

    /**
     * @return void
     */
    public function startProcessing()
    {
        $apiclientList = $this->apiclientRepository->readListWithInvalidation();
        foreach ($apiclientList as $apiclient) {
            $apiKeyList = $this->apikeyRepository->readListByClientId((int)$apiclient->apiClientID);
            foreach ($apiKeyList as $apiKey) {
                $expirationTime = $apiKey->ts + $apiclient->secondsToInvalidation;
                $this->log(
                    "\nINFO: Check Apikey: " . $apiKey->getId() . " for apiclient " . $apiclient->getId()
                );
                $this->log(
                    "INFO: Seconds to invalidation: ". ($expirationTime - $this->now->getTimestamp())
                );
                if ($expirationTime < $this->now->getTimestamp()) {
                    if (! $this->isDryRun) {
                        $this->apikeyRepository->deleteEntity($apiKey->getId());
                        $this->log(
                            "INFO: Removed expired Apikey!"
                        );
                    }
                }
            }
        }
    }
}
