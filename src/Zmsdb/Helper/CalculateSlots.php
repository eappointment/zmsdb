<?php

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Availability;
use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Scope as ScopeRepository;
use BO\Zmsdb\Slot as SlotRepository;
use BO\Zmsdb\Connection\Select as DBConnection;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsentities\Exception\ProcessBookableFailed;
use BO\Zmsentities\Scope as ScopeEntity;
use DateTimeImmutable;
use DateTimeZone;
use DateTimeInterface;
use Exception;

/**
 * @SuppressWarnings(Public)
 * @SuppressWarnings(Coupling)
 * @SuppressWarnings(Complexity)
 * @SuppressWarnings(TooManyMethods)
 */
class CalculateSlots
{
    private const MAX_LOCK_MULTIPLIER = 3;

    private const MAX_CALCULATION_PERIOD = 365;

    protected bool $verbose = false;

    protected bool $isDaily = false;

    protected bool $isDryRun = false;

    protected string $startTime;

    protected array $logList = [];

    private SlotRepository $slotRepository;

    private ConfigRepository $configRepository;

    /**
     * @param bool $verbose
     * @param bool $isDryRun
     * @param bool $isDaily
     */
    public function __construct(bool $verbose = false, bool $isDryRun = false, bool $isDaily = false)
    {
        $this->verbose = $verbose;
        $this->isDaily = $isDaily;
        $this->isDryRun = $isDryRun;
        $this->startTime = microtime(true);
        $this->configRepository = new ConfigRepository();
        $this->slotRepository = new SlotRepository();
    }

    public function log($message): self
    {
        $time = $this->getSpendTime();
        $memory = memory_get_usage() / (1024 * 1024);
        $text = sprintf("[CalculateSlots %07.3fs %07.1fmb] %s", "$time", $memory, $message);
        $this->logList[] = $text;
        if ($this->verbose) {
            error_log($text);
        }
        return $this;
    }

    public function dumpLogs(): void
    {
        foreach ($this->logList as $text) {
            if (!$this->verbose) {
                error_log($text);
            }
        }
        $this->verbose = true;
    }

    public function getSpendTime(): float
    {
        return round(microtime(true) - $this->startTime, 3);
    }

    protected function readCalculateSkip()
    {
        return $this->configRepository->readProperty('status__calculateSlotsSkip');
    }

    protected function readMaxPreBookingPeriod()
    {
        return $this->configRepository->readProperty('availability__calculateSlotsMaxPreBooking');
    }

    protected function readIgnoredScopesInSlotCalulation(): array
    {
        $scopeCsv = $this->configRepository->readProperty('availability__calculateSlotsIgnoreScopes');
        return (!empty($scopeCsv)) ? explode(',', $scopeCsv) : [];
    }

    protected function readForceVerbose()
    {
        $force = $this->configRepository->readProperty('status__calculateSlotsForceVerbose');
        if ($force) {
            $this->log("Forced verbose, see table config.status__calculateSlotsForceVerbose");
            $this->dumpLogs();
        }
        return $force;
    }

    /**
     * @throws Exception
     */
    protected function readLock(): ?DateTimeImmutable
    {
        $lockedDateTime = $this->configRepository->readProperty('status__calculateSlotsIsLocked');
        return ($lockedDateTime) ?
            new DateTimeImmutable($lockedDateTime, new DateTimeZone('Europe/Berlin')) :
            null;
    }

    /**
     * @param DateTimeImmutable $now
     * @param bool $maximizeDuration
     * @return void
     * @throws Exception
     */
    protected function writeLock(DateTimeImmutable $now, bool $maximizeDuration = false): void
    {
        $duration = ($maximizeDuration) ?
            $this->getExecutionTimeLimit(self::MAX_LOCK_MULTIPLIER) :
            $this->getExecutionTimeLimit();
        $lockedUntil = $this->readLock();
        $lockedDateTime = $now
            ->modify('+' . $duration . ' seconds')
            ->format('Y-m-d H:i:s');
        if (! $lockedUntil || $now > $lockedUntil || $this->isDaily) {
            $this->configRepository
                ->replaceProperty('status__calculateSlotsIsLocked', $lockedDateTime);
        }
    }

    /**
     * @return void
     */
    protected function writeDeletedLock(): void
    {
        $this->configRepository->replaceProperty('status__calculateSlotsIsLocked', '');
    }

    protected function readLastStart()
    {
        return $this->configRepository->readProperty('status__calculateSlotsLastStart');
    }

    protected function writeLastStart(DateTimeImmutable $now): void
    {
        $this->configRepository
            ->replaceProperty('status__calculateSlotsLastStart', $now->format('Y-m-d H:i:s'));
    }

    protected function readLastRun()
    {
        return $this->configRepository->readProperty('status__calculateSlotsLastRun');
    }

    protected function writeLastRun(): void
    {
        $lastRunDateTime = (new DateTimeImmutable())->format('Y-m-d H:i:s');
        $this->configRepository->replaceProperty('status__calculateSlotsLastRun', $lastRunDateTime);
    }

    public function getExecutionTimeLimit(int $extends = 1): int
    {
        return $this->configRepository->readProperty('availability__calculateSlotsWithTimeLimit') * $extends;
    }

    /**
     * @throws PDOFailed
     */
    public function writeMaintenanceQueries(): bool
    {
        $sqlList = $this->configRepository->readProperty('status__calculateSlotsMaintenanceSQL');
        if ($sqlList) {
            $pdo = DBConnection::getWriteConnection();
            foreach (explode("\n", $sqlList) as $sql) {
                $this->log("Maintenance query: $sql");
                $pdo->exec($sql);
            }
            return $this->writeCommit();
        }
        return false;
    }


    /**
     *
     *
     * @throws PDOFailed
     * @throws ProcessBookableFailed
     * @throws Exception
     */
    public function writeCalculations(DateTimeImmutable $now, bool $delete = false): bool
    {
        DBConnection::setTransaction();
        DBConnection::getWriteConnection();
        $this->readForceVerbose();
        $dryRun =  ($this->isDryRun) ? 'yes' : 'no';
        $this->log("Calculate with time (isDryRun: " . $dryRun . ") " . $now->format('c'));
        $lockedUntil = ($this->readLock()) ? $this->readLock()->format('c') : "-";
        $this->log("Calculation locked until " . $lockedUntil);

        if ($this->isSkip($now, $delete)) {
            return false;
        }
        if ($delete) {
            $this->waitForLock($now);
        }

        $this->writeLock($now, true);
        $this->writeLastStart($now);
        $this->writeCommit();

        if ($delete) {
            $this->deleteOldSlots($now);
        }

        $lastRunDateTime = $this->readLastRun();
        $this->log("Last Run on time=" . $lastRunDateTime);
        $filteredScopes = $this->readFilteredScopes();
        $listLength = count($filteredScopes);
        $loopTimeLimit = $this->getExecutionTimeLimit();
        foreach ($filteredScopes as $key => $scope) {
            $loop = $key + 1;
            // compare spended time to max excecution time
            if ($loopTimeLimit && $this->getSpendTime() > $loopTimeLimit) {
                if (!$this->isDaily) {
                    $this->log("Maximum execution time reached. Slot calculation is canceled");
                    break;
                }
                $loopTimeLimit += $loopTimeLimit;
                $this->writeLock(new DateTimeImmutable(), true);
            }
            $calculatedStart = (new DateTimeImmutable())->format(DateTimeInterface::ATOM);
            //lock all availabilities by scope to avoid changes during slot calculation
            (new Availability())->readLockByScope($scope->getId(), $now);
            // set public and callcenter slots to 0 if bookable start date is in future before next slot-process mapping
            if ($this->writeExpiredBookableStartByScope($scope, $now)) {
                $this->log("Calculated slots by expired bookable start $loop/$listLength for $scope");
            }
            if ($this->writeCalculatedScope($scope, $now)) {
                $calculatedEnd = (new DateTimeImmutable())->format(DateTimeInterface::ATOM);
                $this->log("Calculated slots $loop/$listLength for $scope from $calculatedStart until $calculatedEnd");
            }
            // commit transaction after writeCalculatedScope and writeExpiredBookableStartByScope methods
            $this->writeCommit();
        }

        // commit transaction after writeCanceledSlots method
        $this->writeCanceledSlots($now);

        // commit transaction after writeFullSlots method
        $this->writeFullSlots();

        $this->writeLastRun();
        $this->writeDeletedLock();
        // commit previous transaction
        $this->writeCommit();

        $this->log("Slot calculation finished");

        $this->writeMaintenanceQueries();

        return true;
    }

    /**
     * @return array
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    protected function readFilteredScopes(): array
    {
        $maxCalcuationPeriod = $this->readMaxPreBookingPeriod();
        $excludedScopes = $this->readIgnoredScopesInSlotCalulation();
        $maxPeriod = ($maxCalcuationPeriod > 0 && !$this->isDaily) ?
            $maxCalcuationPeriod :
            self::MAX_CALCULATION_PERIOD;
        $this->log("Ignore scopes with pre-booking period greater than $maxPeriod days");
        $scopeList = (new ScopeRepository())->readListWithMaxPrebookingPeriod($maxPeriod, 1);
        if (count($excludedScopes)) {
            $this->log("Exclude scopeIds " . implode(',', $excludedScopes) . " by config setting");
            $scopeList = $scopeList->withOutScopes($excludedScopes);
        }
        $scopeArray = $scopeList->getArrayCopy();
        shuffle($scopeArray);
        return $scopeArray;
    }

    protected function writeCommit(): bool
    {
        if (! $this->isDryRun) {
            DBConnection::writeCommit();
            return true;
        }
        return false;
    }

    /**
     * @param DateTimeImmutable $now
     * @param bool $delete
     * @return bool
     * @throws Exception
     */
    protected function isSkip(DateTimeImmutable $now, bool $delete): bool
    {
        if ($this->readCalculateSkip()) {
            $this->log("Skip calculation due to config setting status.calculateSlotsSkip");
            return true;
        }

        if ($this->readLock() > $now && !$delete) {
            $this->log("Skip calculation due to already existing slot calculation");
            return true;
        }

        return false;
    }

    /**
     * @param ScopeEntity       $scope
     * @param DateTimeImmutable $now
     *
     * @return bool
     * @throws ProcessBookableFailed
     * @throws Exception
     */
    protected function writeCalculatedScope(ScopeEntity $scope, DateTimeImmutable $now): bool
    {
        $updatedList = $this->slotRepository->writeByScope($scope, $now);
        foreach ($updatedList as $availability) {
            $this->log("Updated $availability with reason " . json_encode($availability['processingNote']));
        }
        $slotLastChange = $this->slotRepository->readLastChangedTimeByScope($scope);
        if (count($updatedList) || $this->slotRepository->hasScopeRelevantChanges($scope, $slotLastChange)) {
            $this->writePostProcessingByScope($scope, $now);
            return true;
        }
        return false;
    }

    /**
     * @throws Exception
     */
    protected function waitForLock(DateTimeImmutable $now): void
    {
        // wait for locked job and lock with multiplier to get long lock while deleting old slots
        $isLockedUntil = $this->readLock();
        $countReadLock = 0;
        while ($isLockedUntil && $now < $isLockedUntil) {
            if ($countReadLock % 5 === 0) {
                $this->log("Wait: Calculation is locked until " . $isLockedUntil->format('Y-m-d H:i:s'));
            }
            sleep(2);
            $isLockedUntil = $this->readLock();
            $countReadLock++;
        }
    }

    public function writeExpiredBookableStartByScope(ScopeEntity $scope, DateTimeImmutable $now): bool
    {
        $bookableStartDate = $now->modify("+" . $scope->getPreference('appointment', 'startInDaysDefault') . " days");
        if ($this->slotRepository->writeCanceledByBookableStart($bookableStartDate, $scope)) {
            $this->log(
                'scope ' . $scope->id . ' bookable in ' . $scope->getPreference('appointment', 'startInDaysDefault') .
                ' days -> canceled public and callcenter slots < ' . $bookableStartDate->format('Y-m-d H:i')
            );
            return true;
        }
        return false;
    }

    /**
     * @param ScopeEntity       $scope
     * @param DateTimeImmutable $now
     *
     * @return void
     */
    public function writePostProcessingByScope(ScopeEntity $scope, DateTimeImmutable $now): void
    {
        if ($this->slotRepository->deleteSlotProcessOnProcess($scope->id)) {
            $this->log("Finished to free slots for changed/deleted processes for scope $scope->id");
        }
        $this->slotRepository->writeCanceledByTimeAndScope($now, $scope);
        $this->slotRepository->deleteSlotProcessOnSlot($scope->id);

        if ($this->slotRepository->updateSlotProcessMapping($scope->id)) {
            $this->log("Updated Slot-Process-Mapping, mapped processes for scope $scope->id");
        }
    }

    /**
     * @throws PDOFailed
     */
    public function writeCanceledSlots(DateTimeImmutable $now, $modify = '+10 minutes'): void
    {
        DBConnection::getWriteConnection();
        if ($this->slotRepository->deleteSlotProcessOnProcess()) {
            $this->log("Finished to free slots for changed/deleted processes without scope filter");
        }
        if ($this->slotRepository->writeCanceledByTime($now->modify($modify))) {
            $this->log("Cancelled slots older than " . $now->modify($modify)->format('c'));
        }
        if ($this->slotRepository->writeCanceledByMissingTimeSlot()) {
            $this->log("Slots are canceled if they no longer fit into a block of opening hours");
        }
        $this->slotRepository->deleteSlotProcessOnSlot();
        if ($this->slotRepository->updateSlotProcessMapping()) {
            $this->log("Updated Slot-Process-Mapping, mapped processes without scope filter");
        }
        $this->writeCommit();
    }

    /**
     * @return void
     * @throws PDOFailed
     */
    public function writeFullSlots(): void
    {
        DBConnection::getWriteConnection();
        if ($this->slotRepository->writeUpdatedSlotStatus()) {
            $this->writeCommit();
            $this->log("Calculated full slot status");
        }
    }

    /**
     * @param DateTimeImmutable $now
     *
     * @throws PDOFailed
     */
    public function deleteOldSlots(DateTimeImmutable $now): void
    {
        DBConnection::getWriteConnection();
        $this->log("Maintenance: Delete slots older than " . $now->format('Y-m-d'));
        if ($this->slotRepository->deleteSlotsOlderThan($now)) {
            // commit transaction if deleting of old slots was successfull
            $this->writeCommit();
            $this->log("Deleted old slots successfully");
        }
    }

    /**
     * @return bool
     */
    public function isVerbose(): bool
    {
        return $this->verbose;
    }
}
