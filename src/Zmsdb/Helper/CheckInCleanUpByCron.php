<?php

declare(strict_types=1);

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\CheckInConfig;
use BO\Zmsdb\CheckInTask;
use Psr\Log\LoggerInterface;

class CheckInCleanUpByCron
{
    /** @var LoggerInterface */
    private $logger;

    /** @var CheckInConfig  */
    private $configRepository;

    /** @var CheckInTask  */
    private $taskRepository;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
        $this->configRepository = new CheckInConfig();
        $this->taskRepository = new CheckInTask();
    }

    public function startProcessing(?\DateTimeInterface $now = null): void
    {
        if (!$now && class_exists('\App') && isset(\App::$now)) {
            $now = \App::$now;
        } elseif (!$now) {
            $now = new \DateTimeImmutable('now');
        }

        $countDeleted = $this->configRepository->deleteOutdated($now);
        $this->logger->info($countDeleted . ' check-in-config entries have been deleted.');

        $countDeleted = $this->taskRepository->deleteOutdated($now);
        $this->logger->info($countDeleted . ' check-in-task entries have been deleted.');
    }
}
