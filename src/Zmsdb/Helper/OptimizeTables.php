<?php

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Slot as SlotRepository;
use BO\Zmsdb\Log as LogRepository;

use DateTimeInterface;

/**
 * @SuppressWarnings(Public)
 * @SuppressWarnings(Coupling)
 * @SuppressWarnings(Complexity)
 */

class OptimizeTables
{
    /** @var bool */
    protected $verbose = false;

    /** @var bool */
    protected $isDryRun = false;

    public function __construct(bool $verbose = false, bool $isDryRun = false)
    {
        $this->verbose = $verbose;
        $this->isDryRun = $isDryRun;
    }

    public function log($message): self
    {
        if ($this->verbose) {
            error_log($message);
        }
        return $this;
    }

    /**
     * @param DateTimeInterface $now
     */
    public function optimizeTables(DateTimeInterface $now): void
    {
        $this->log("Maintenance: Optimize slot and log database tables at ". $now->format(DateTimeInterface::ATOM));
        if (! $this->isDryRun) {
            (new SlotRepository())->writeOptimizedSlotTables();
            //(new LogRepository())->writeOptimizedTable();
            $this->log("Optimized tables successfully");
        }
    }
}
