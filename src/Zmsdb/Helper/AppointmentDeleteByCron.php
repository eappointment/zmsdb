<?php

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Exception\Process\ProcessArchiveUpdateFailed;
use BO\Zmsdb\Log as LogRepository;
use BO\Zmsentities\Process as ProcessEntity;

use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\ProcessArchive as ProcessArchiveRepo;
use BO\Zmsdb\Query\ProcessArchive as ProcessArchiveQueryRepo;
use BO\Zmsdb\ProcessStatusArchived as ProcessStatusRepository;

use Closure;
use DateTimeInterface;
use DateTimeImmutable;

/**
 * @codeCoverageIgnore
 */
class AppointmentDeleteByCron
{
    /** @var bool */
    protected $verbose = false;

    /** @var bool */
    protected $isDryRun = false;

    /** @var int */
    protected $limit = 10000;

    /** @var int */
    protected $loopCount = 500;

    /** @var DateTimeInterface */
    protected $time;

    /** @var DateTimeInterface */
    protected $now;

    /**
     * List of possible appointment statuses to be considered when removing expired appointments.
     */
    protected $statuslist = [
        ProcessEntity::STATUS_BLOCKED,
        ProcessEntity::STATUS_DELETED,
        ProcessEntity::STATUS_CONFIRMED,
        ProcessEntity::STATUS_QUEUED,
        ProcessEntity::STATUS_CALLED,
        ProcessEntity::STATUS_MISSED,
        ProcessEntity::STATUS_PROCESSING,
    ];

     /**
     * List of possible appointment statuses where the appointment is given another status change
     * if it was an unattended appointment (confirmed, queued, called), then archived for statistical purposes
     * and only removed from the database afterward.
     */
    protected $archivelist = [
        ProcessEntity::STATUS_CONFIRMED,
        ProcessEntity::STATUS_QUEUED,
        ProcessEntity::STATUS_CALLED,
        ProcessEntity::STATUS_MISSED,
        ProcessEntity::STATUS_PROCESSING,
        ProcessEntity::STATUS_PENDING,
    ];

    /**
     * @var array
     */
    protected $count = [];

    /**
     * @param                   $timeIntervalDays
     * @param DateTimeInterface $now
     * @param bool              $verbose
     * @param bool              $isDryRun
     */
    public function __construct(
        $timeIntervalDays,
        DateTimeInterface $now,
        bool $verbose = false,
        bool $isDryRun = false
    ) {
        $deleteInSeconds = (24 * 60 * 60) * $timeIntervalDays;
        $this->time = (new DateTimeImmutable())->setTimestamp($now->getTimestamp() - $deleteInSeconds);
        $this->now = $now;
        $this->verbose = $verbose;
        $this->isDryRun = $isDryRun;
        $this->log("INFO: Deleting appointments older than " . $this->time->format('c'));
    }

    protected function log($message): bool
    {
        return $this->verbose && error_log($message);
    }

    public function getCount(): array
    {
        return $this->count;
    }

    public function setLimit($limit): void
    {
        $this->limit = $limit;
    }

    public function setLoopCount($loopCount): void
    {
        $this->loopCount = $loopCount;
    }

    /**
     * @param bool $pending
     *
     * @throws ProcessArchiveUpdateFailed
*/
    public function startProcessing(bool $pending = false): void
    {
        if ($pending) {
            $this->statuslist[] = ProcessEntity::STATUS_PENDING;
        }
        $this->count = array_fill_keys($this->statuslist, 0);
        $this->deleteExpiredProcesses();
        $this->updateProcessArchive();
        $this->log("\nSUMMARY: Deleted processes: ".var_export($this->count, true));
    }

    protected function deleteExpiredProcesses(): void
    {
        foreach ($this->statuslist as $status) {
            $this->log("\nDelete expired processes with status $status:");
            $count = $this->deleteByCallback(function ($limit, $offset) use ($status) {
                $query = new ProcessRepository();
                return $query->readExpiredProcessListByStatus($this->time, $status, $limit, $offset, 1);
            });
            $this->count[$status] += $count;
        }
    }

    /**
     * @throws ProcessArchiveUpdateFailed
     */
    protected function updateProcessArchive(): void
    {
        // remove blocked entries
        $processArchiveRepo = new ProcessArchiveRepo();
        $processArchiveList = $processArchiveRepo->readListByStatus(ProcessEntity::STATUS_BLOCKED);
        if (! $this->isDryRun) {
            $processArchiveRepo->perform(ProcessArchiveQueryRepo::QUERY_UPDATE_STATUS, [
                'oldStatus' => ProcessEntity::STATUS_BLOCKED,
                'newStatus' => ProcessEntity::STATUS_DELETED,
            ]);
        }
        $this->log(
            "\nSet processArchive entries with status ".
            ProcessEntity::STATUS_BLOCKED . " to ".
            ProcessEntity::STATUS_DELETED ." for " .
            $processArchiveList->count() ." entries"
        );
    }

    protected function deleteByCallback(Closure $callback): int
    {
        $processCount = 0;
        $startposition = 0;
        while ($processCount < $this->limit) {
            $processList = $callback($this->loopCount, $startposition);
            if (0 == $processList->count()) {
                break;
            }
            foreach ($processList as $process) {
                if (!$this->removeProcess($process, $processCount)) {
                    $startposition++;
                }
                $processCount++;
            }
            if ($this->loopCount > $processList->count()) {
                break;
            }
        }
        return $processCount;
    }

    protected function removeProcess(ProcessEntity $process, $processCount): bool
    {
        if (in_array($process->status, $this->statuslist)) {
            if (in_array($process->status, $this->archivelist)) {
                $process = $this->updateProcessStatus($process);
                $this->log("INFO: $processCount. Archive $process");
                if (!$this->isDryRun) {
                    $this->archiveProcess($process);
                }
            }
            $this->log("INFO: $processCount. Delete $process");
            if (!$this->isDryRun) {
                $this->deleteProcess($process);
                return true;
            }
        }
        return false;
    }

    protected function updateProcessStatus(ProcessEntity $process): ProcessEntity
    {
        if (in_array($process->status, [
            ProcessEntity::STATUS_CONFIRMED,
            ProcessEntity::STATUS_QUEUED,
            ProcessEntity::STATUS_CALLED])
        ) {
            $process->status = 'missed';
        }
        return $process;
    }

    /**
     * @param ProcessEntity $process
     *
     * @return void
     */
    protected function archiveProcess(ProcessEntity $process): void
    {
        $archived = (new ProcessStatusRepository())->writeEntityFinished($process, $this->now);
        if ($archived) {
            LogRepository::writeLogEntry(
                "Archived (AppointmentDeleteByCron::archiveProcess) $process ",
                $process->getId()
            );
            $this->log("INFO: Archived with Status=$process->status and Id=" . $archived->archiveId);
        }
    }

    protected function deleteProcess(ProcessEntity $process): void
    {
        if ((new ProcessRepository())->writeDeletedEntity($process->id)) {
            $this->log("INFO: Process $process->id successfully removed");
            LogRepository::writeLogEntry(
                "Deleted (AppointmentDeleteByCron::deleteProcess) $process ",
                $process->getId()
            );
        } else {
            $this->log("WARN: Could not remove process '$process->id'!");
        }
    }
}
