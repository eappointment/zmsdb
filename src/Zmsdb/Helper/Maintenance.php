<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\MaintenanceSchedule as MaintenanceRepository;

class Maintenance
{
    /**
     * @return bool
     */
    public static function isMaintenanceModeActive(): bool
    {
        $activeEntity = (new MaintenanceRepository())->readActiveEntity();
        return $activeEntity !== null;
    }
}
