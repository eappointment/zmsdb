<?php

namespace BO\Zmsdb\Helper;

use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Mail as MailRepository;

/**
 * @codeCoverageIgnore
 */
class SendProcessListToScopeAdmin
{
    protected $scopeList;

    protected $datetime;

    protected $verbose = false;

    /** @var MailService */
    protected $mailService = null;

    public function __construct(\DateTimeImmutable $dateTime, $scopeId = false, $verbose = false)
    {
        $this->mailService = new MailService(new MailRepository(), new ConfigRepository());
        $this->dateTime = $dateTime;
        if ($verbose) {
            error_log("INFO: Send process list of current day to scope admin");
            $this->verbose = true;
        }
        if ($scopeId) {
            $scope = (new \BO\Zmsdb\Scope())->readEntity($scopeId);
            $this->scopeList = (new \BO\Zmsentities\Collection\ScopeList())->addEntity($scope);
        } else {
            $this->scopeList = (new \BO\Zmsdb\Scope)->readListWithScopeAdminEmail(1);
        }
    }

    public function startProcessing($commit)
    {
        foreach ($this->scopeList as $scope) {
            if ($this->verbose) {
                error_log("INFO: Processing $scope");
            }
            if ($commit) {
                $processList = (new \BO\Zmsdb\Process)
                    ->readProcessListByScopeAndTime($scope->getId(), $this->dateTime, 1);
                $processList = $processList
                   ->toQueueList($this->dateTime)
                   ->withStatus(array('confirmed', 'queued', 'reserved'))
                   ->withSortedArrival()
                   ->toProcessList();
                if (0 <= $processList->count()) {
                    $this->mailService->setProcessList($processList);
                    if ($this->mailService->writeInQueueWithDailyProcessList($scope, $this->dateTime) &&
                        $this->verbose
                    ) {
                        error_log('INFO: Send processList to:'. $scope->getContactEmail());
                    }
                } else {
                    error_log("WARNING: Processlist empty for $scope->id");
                }
            }
        }
    }
}
