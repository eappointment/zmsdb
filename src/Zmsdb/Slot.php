<?php
namespace BO\Zmsdb;

use BO\Zmsdb\Connection\Select;

use BO\Zmsentities\Exception\AppointmentNotFitInSlotList;
use BO\Zmsentities\Exception\ProcessBookableFailed;
use BO\Zmsentities\Slot as Entity;
use BO\Zmsentities\Collection\SlotList as Collection;
use BO\Zmsentities\Availability as AvailabilityEntity;
use BO\Zmsentities\Schema\Entity as SchemaEntity;
use BO\Zmsentities\Collection\AvailabilityList;
use BO\Zmsentities\Scope as ScopeEntity;
use BO\Zmsentities\Appointment as AppointmentEntity;
use BO\Zmsentities\Helper\DateTime as DateTimeHelper;

use DateTimeImmutable;
use DateTimeInterface;
use Exception;
use Exception\Pdo\DeadLockFound;
use Exception\Pdo\LockTimeout;
use Exception\Pdo\PDOFailed;

/**
 * @SuppressWarnings(Public)
 * @SuppressWarnings(Complexity)
 * @SuppressWarnings(Coupling)
 */
class Slot extends Base
{

    /**
     * maximum number of slots per appointment
     */
    const MAX_SLOTS = 10;

    /**
     * @param AppointmentEntity $appointment
     * @param int|null          $overwriteSlotsCount
     * @param bool              $extendSlotList
     *
     * @return Collection
     * @throws AppointmentNotFitInSlotList
     */
    public function readByAppointment(
        AppointmentEntity $appointment,
        int               $overwriteSlotsCount = null,
        bool              $extendSlotList = false
    ): Collection {
        $appointment = clone $appointment;
        $availability = (new Availability())->readByAppointment($appointment);
        // Check if availability allows multiple slots, but allow to overwrite
        if (!$availability->multipleSlotsAllowed || $overwriteSlotsCount >= 1) {
            $appointment->slotCount = ($overwriteSlotsCount >= 1) ? $overwriteSlotsCount : 1;
        }
        $slotList = $availability->getSlotList()->withSlotsForAppointment($appointment, $extendSlotList);
        foreach ($slotList as $slot) {
            $this->readByAvailability($slot, $availability, $appointment->toDateTime(), true);
        }
        return $slotList;
    }

    /**
     * @param Entity             $slot
     * @param SchemaEntity       $availability
     * @param DateTimeInterface  $date
     * @param bool               $getLock
     *
     * @return false|mixed
     */
    public function readByAvailability(
        Entity             $slot,
        SchemaEntity        $availability,
        DateTimeInterface  $date,
        bool               $getLock = false
    ) {
        $data = array();
        $data['scopeID'] = $availability->scope->id;
        $data['availabilityID'] = $availability->id;
        $data['year'] = $date->format('Y');
        $data['month'] = $date->format('m');
        $data['day'] = $date->format('d');
        $data['time'] = $slot->getTimeString();
        $sql = Query\Slot::QUERY_SELECT_SLOT;
        if ($getLock) {
            $sql .= ' FOR UPDATE';
        }
        $slotID = $this->fetchRow(
            $sql,
            $data
        );
        return $slotID ? $slotID['slotID'] : false ;
    }

    /**
     * @param ScopeEntity       $scope
     * @param DateTimeInterface $dateTime
     *
     * @return array
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function readByScopeAndDate(
        ScopeEntity $scope,
        DateTimeInterface $dateTime
    ) {
        return $this->fetchAll(Query\Slot::QUERY_SELECT_BY_SCOPE_AND_DAY, [
            'year' => $dateTime->format('Y'),
            'month' => $dateTime->format('m'),
            'day' => $dateTime->format('d'),
            'scopeID' => $scope->id,
        ]);
    }

    /**
     * @param ScopeEntity            $scope
     * @param DateTimeInterface|null $slotLastChange
     *
     * @return bool
     */
    public function hasScopeRelevantChanges(
        ScopeEntity $scope,
        DateTimeInterface $slotLastChange = null
    ): bool {
        $startInDaysDefault = (new Preferences())
            ->readProperty('scope', $scope->id, 'appointment', 'startInDaysDefault');
        $endInDaysDefault = (new Preferences())
            ->readProperty('scope', $scope->id, 'appointment', 'endInDaysDefault');
        if ($scope->preferences['appointment']['startInDaysDefault'] != $startInDaysDefault
            || $scope->preferences['appointment']['endInDaysDefault'] != $endInDaysDefault
        ) {
            (new Scope())->replacePreferences($scope); //TODO remove after ZMS1 is deactivated
            return true;
        }
        $startInDaysChange = (new Preferences())
            ->readChangeDateTime('scope', $scope->id, 'appointment', 'startInDaysDefault');
        $endInDaysChange = (new Preferences())
            ->readChangeDateTime('scope', $scope->id, 'appointment', 'endInDaysDefault');
        if ($startInDaysChange->getTimestamp() > $slotLastChange->getTimestamp()
            || $endInDaysChange->getTimestamp() > $slotLastChange->getTimestamp()) {
            return true;
        }
        return false;
    }

    /**
     * @throws Exception
     */
    public function isAvailabilityOutdated(
        AvailabilityEntity $availability,
        DateTimeInterface $now,
        DateTimeInterface $slotLastChange = null
    ): bool {
        $proposedChange = new Helper\AvailabilitySnapShot($availability, $now);
        $formerChange = new Helper\AvailabilitySnapShot($availability, $slotLastChange);

        if ($formerChange->hasOutdatedAvailability()) {
            $availability['processingNote'][] = 'outdated: availability change';
            return true;
        }
        if ($formerChange->hasOutdatedScope()
            && $this->hasScopeRelevantChanges($availability->scope, $slotLastChange)
        ) {
            $availability['processingNote'][] = 'outdated: scope change';
            return true;
        }
        if ($formerChange->hasOutdatedDayoff()) {
            $availability['processingNote'][] = 'outdated: dayoff change';
            return true;
        }
        // Be aware, that last slot change and current time might differ several days
        //  if the rebuild fails in some way
        if (1
            // First check if the bookable end date on current time was already calculated on last slot change
            && !$formerChange->hasBookableDateTime($proposedChange->getLastBookableDateTime())
            // Second check if between last slot change and current time could be a bookable slot
            && (
                (
                    !$formerChange->isOpenedOnLastBookableDay()
                    && $proposedChange->hasBookableDateTimeAfter($formerChange->getLastBookableDateTime())
                )
                // if calculation already happened the day before, check if lastChange time was before opening
                || (
                    $formerChange->isOpenedOnLastBookableDay()
                    && (
                        !$formerChange->isTimeOpenedOnLastBookableDay()
                        || $proposedChange->hasBookableDateTimeAfter(
                            $formerChange->getLastBookableDateTime()->modify('+1day 00:00:00')
                        )
                    )
                )
            )
            // Check if daytime is after booking start time if bookable end of now is calculated
            && (
                !$proposedChange->isOpenedOnLastBookableDay()
                || $proposedChange->isTimeOpenedOnLastBookableDay()
            )
        ) {
            $availability['processingNote'][] = 'outdated: new slots required';
            return true;
        }
        if ($availability->getBookableStart($slotLastChange) != $availability->getBookableStart($now)
            // First check, if bookable start from lastChange was not included in bookable time from now
            && !$availability->hasDate($availability->getBookableStart($slotLastChange), $now)
            // Second check, if availability had a bookable time on lastChange before bookable start from now
            && $availability->hasDateBetween(
                $availability->getBookableStart($slotLastChange),
                $availability->getBookableStart($now),
                $slotLastChange
            )
        ) {
            $availability['processingNote'][] = 'outdated: slots invalidated by bookable start';
            return true;
        }
        $availability['processingNote'][] = 'not outdated';
        return false;
    }

    /**
     * @return bool TRUE if there were changes on slots
     * @throws ProcessBookableFailed
     * @throws Exception
     */
    public function writeByAvailability(
        AvailabilityEntity $availability,
        DateTimeInterface $now,
        DateTimeInterface $slotLastChange = null
    ): bool {
        $now = DateTimeHelper::create($now);
        if (!$slotLastChange) {
            $slotLastChange = $this->readLastChangedTimeByAvailability($availability);
        }
        $availability['processingNote'][] = 'lastchange='.$slotLastChange->format('c');
        if (!$this->isAvailabilityOutdated($availability, $now, $slotLastChange)) {
            return false;
        }

        // Order is import, the following cancels all slots
        // and should only happen, if rebuild is triggered
        $cancelledSlots = $this->fetchAffected(Query\Slot::QUERY_CANCEL_AVAILABILITY, [
            'availabilityID' => $availability->id,
        ]);
        if (!$availability->withData(['bookable' => ['startInDays' => 0]])->hasBookableDates($now)) {
            $availability['processingNote'][] = "cancelled $cancelledSlots slots: availability not bookable ";
            return $cancelledSlots > 0;
        }
        $availability['processingNote'][] = "cancelled $cancelledSlots slots";
        $startDate = $availability->getBookableStart($now)->modify('00:00:00');
        $stopDate = $availability->getBookableEnd($now);
        $slotlist = $availability->getSlotList();
        $slotlistIntern = $slotlist->withValueFor('callcenter', 0)->withValueFor('public', 0);
        $time = $now->modify('00:00:00');
        $status = false;
        do {
            if ($availability->withData(['bookable' => ['startInDays' => 0]])->hasDate($time, $now)) {
                $writeStatus = $this->writeSlotListForDate(
                    $time,
                    ($time->getTimestamp() < $startDate->getTimestamp()) ? $slotlistIntern : $slotlist,
                    $availability
                );
                $status = $writeStatus ?: $status;
            }
            $time = $time->modify('+1day');
        } while ($time->getTimestamp() <= $stopDate->getTimestamp());

        return $status || ($cancelledSlots > 0);
    }

    /**
     * @throws ProcessBookableFailed
     * @throws Exception
     */
    public function writeByScope(
        ScopeEntity $scope,
        DateTimeInterface $now
    ): AvailabilityList {
        $slotLastChange = $this->readLastChangedTimeByScope($scope);
        $availabilityList = (new Availability())
            ->readAvailabilityListByScope($scope, 0, $slotLastChange->modify('-1 day'));
        $updatedList = new AvailabilityList();
        foreach ($availabilityList as $availability) {
            $availability->scope = clone $scope; //dayoff is required
            if ($this->writeByAvailability($availability, $now)) {
                $updatedList->addEntity($availability);
            }
        }
        return $updatedList;
    }

    protected function writeSlotListForDate(
        DateTimeInterface $time,
        Collection $slotlist,
        AvailabilityEntity $availability
    ) {
        $ancestors = [];
        $hasAddedSlots = false;
        $status = false;
        foreach ($slotlist as $slot) {
            $slot = clone $slot;
            $slotID = $this->readByAvailability($slot, $availability, $time);
            if ($slotID) {
                $query = new Query\Slot(Query\Base::UPDATE);
                $query->addConditionSlotId($slotID);
            } else {
                $query = new Query\Slot(Query\Base::INSERT);
                $hasAddedSlots = true;
            }
            $slot->status = 'free';
            $values = $query->reverseEntityMapping($slot, $availability, $time);
            $values['createTimestamp'] = time();
            $query->addValues($values);
            $writeStatus = $this->writeItem($query);
            if ($writeStatus && !$slotID) {
                $slotID = $this->getWriter()->lastInsertId();
            }
            $ancestors[] = $slotID;
            // TODO: Check if slot changed before writing ancestor IDs
            $this->writeAncestorIDs($slotID, $ancestors);
            $status = $writeStatus ?: $status;
        }
        if ($hasAddedSlots) {
            $availability['processingNote'][] = 'Added '.$time->format('Y-m-d');
        }
        return $status;
    }

    protected function writeAncestorIDs($slotID, array $ancestors)
    {
        $this->perform(Query\Slot::QUERY_DELETE_ANCESTOR, [
            'slotID' => $slotID,
        ]);
        $ancestorLevel = count($ancestors);
        foreach ($ancestors as $ancestorID) {
            if ($ancestorLevel <= self::MAX_SLOTS) {
                $this->perform(Query\Slot::QUERY_INSERT_ANCESTOR, [
                    'slotID' => $slotID,
                    'ancestorID' => $ancestorID,
                    'ancestorLevel' => $ancestorLevel,
                ]);
            }
            $ancestorLevel--;
        }
    }

    /**
     * @throws Exception
     */
    public function readLastChangedTime(): DateTimeImmutable
    {
        $last = $this->fetchRow(
            Query\Slot::QUERY_LAST_CHANGED
        );
        if (!$last['dateString']) {
            $last['dateString'] = '1970-01-01 12:00';
        }
        return new DateTimeImmutable($last['dateString'] . Select::$connectionTimezone);
    }

    /**
     * @throws Exception
     */
    public function readLastChangedTimeByScope(ScopeEntity $scope): DateTimeImmutable
    {
        $last = $this->fetchRow(
            Query\Slot::QUERY_LAST_CHANGED_SCOPE,
            [
                'scopeID' => $scope->id,
            ]
        );
        if (!$last['dateString']) {
            $last['dateString'] = '1970-01-01 12:00';
        }
        return new DateTimeImmutable($last['dateString'] . Select::$connectionTimezone);
    }

    /**
     * @throws Exception
     */
    public function readLastChangedTimeByAvailability(AvailabilityEntity $availabiliy): DateTimeImmutable
    {
        $last = $this->fetchRow(
            Query\Slot::QUERY_LAST_CHANGED_AVAILABILITY,
            [
                'availabilityID' => $availabiliy->id,
            ]
        );
        if (!$last['dateString']) {
            $last['dateString'] = '1970-01-01 12:00';
        }
        return new DateTimeImmutable($last['dateString'] . Select::$connectionTimezone);
    }

    /**
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     */
    public function updateSlotProcessMapping($scopeID = null): bool
    {
        if ($scopeID) {
            return $this->perform(
                Query\Slot::QUERY_INSERT_MISSING_PROCESS_ID
                . Query\Slot::QUERY_SELECT_MISSING_PROCESS_BY_SCOPE,
                ['scopeID' => $scopeID]
            );
        }
        return $this->perform(Query\Slot::QUERY_INSERT_MISSING_PROCESS_ID);
    }

    public function deleteSlotProcessOnSlot($scopeID = null): int
    {
        if ($scopeID) {
            $count = $this->fetchAffected(
                Query\Slot::QUERY_DELETE_SLOT_PROCESS_CANCELLED
                . Query\Slot::QUERY_DELETE_SLOT_PROCESS_CANCELLED_BY_SCOPE,
                ['scopeID' => $scopeID]
            );
        } else {
            $count = $this->fetchAffected(Query\Slot::QUERY_DELETE_SLOT_PROCESS_CANCELLED, []);
        }
        return $count;
    }

    public function deleteSlotProcessOnProcess($scopeID = null): int
    {
        if ($scopeID) {
            $processIdList = $this->fetchAll(
                Query\Slot::QUERY_SELECT_DELETABLE_SLOT_PROCESS_BY_SCOPE,
                ['scopeID' => $scopeID]
            );
        } else {
            $processIdList = $this->fetchAll(Query\Slot::QUERY_SELECT_DELETABLE_SLOT_PROCESS);
        }
        // Client side INSERT … SELECT … to reduce table locking
        foreach ($processIdList as $processId) {
            $this->perform(Query\Slot::QUERY_DELETE_SLOT_PROCESS_ID, $processId);
        }
        return count($processIdList);
    }


    public function writeSlotProcessMappingFor($processId): Slot
    {
        $this->perform(Query\Slot::QUERY_INSERT_SLOT_PROCESS_ID, [
            'processId' => $processId,
        ]);
        return $this;
    }

    public function deleteSlotProcessMappingFor($processId): Slot
    {
        $this->perform(Query\Slot::QUERY_DELETE_SLOT_PROCESS_ID, [
            'processId' => $processId,
        ]);
        return $this;
    }

    public function writeCanceledByTimeAndScope(DateTimeInterface $dateTime, ScopeEntity $scope): bool
    {
        $status = $this->perform(Query\Slot::QUERY_UPDATE_SLOT_MISSING_AVAILABILITY_BY_SCOPE, [
            'dateString' => $dateTime->format('Y-m-d'),
            'scopeID' => $scope->id,
        ]);
        return $this->perform(Query\Slot::QUERY_CANCEL_SLOT_OLD_BY_SCOPE, [
            'year' => $dateTime->format('Y'),
            'month' => $dateTime->format('m'),
            'day' => $dateTime->format('d'),
            'time' => $dateTime->format('H:i:s'),
            'scopeID' => $scope->id,
        ]) && $status;
    }
    /**
     * All slots that are before the bookable date of the scope will be set to cancelled
     */
    public function writeCanceledByBookableStart(DateTimeInterface $dateTime, ScopeEntity $scope)
    {
        return $this->perform(Query\Slot::QUERY_CANCEL_SLOT_EXPIRED_BY_SCOPE, [
            'year' => $dateTime->format('Y'),
            'month' => $dateTime->format('m'),
            'day' => $dateTime->format('d'),
            'scopeID' => $scope->id,
        ]);
    }

    public function writeCanceledByTime(DateTimeInterface $dateTime): bool
    {
        $status = $this->perform(Query\Slot::QUERY_UPDATE_SLOT_MISSING_AVAILABILITY, [
            'dateString' => $dateTime->format('Y-m-d'),
        ]);
        return $this->perform(Query\Slot::QUERY_CANCEL_SLOT_OLD, [
            'year' => $dateTime->format('Y'),
            'month' => $dateTime->format('m'),
            'day' => $dateTime->format('d'),
            'time' => $dateTime->format('H:i:s'),
        ]) && $status;
    }

    /**
     * Slots are canceled if they no longer fit into a block of opening hours
     *
     * @param DateTimeInterface $dateTime
     * @return bool
     * @throws Exception\Pdo\DeadLockFound
     * @throws Exception\Pdo\LockTimeout
     * @throws Exception\Pdo\PDOFailed
     */
    public function writeCanceledByMissingTimeSlot(): bool
    {
        return $this->perform(Query\Slot::QUERY_UPDATE_SLOT_MISSING_TIME_SLOT);
    }

    public function writeUpdatedSlotStatus(): bool
    {
        return $this->perform(Query\Slot::QUERY_UPDATE_SLOT_STATUS);
    }

    public function deleteSlotsOlderThan(DateTimeInterface $dateTime): bool
    {
        $status = $this->perform(Query\Slot::QUERY_DELETE_SLOT_OLD, [
            'year' => $dateTime->format('Y'),
            'month' => $dateTime->format('m'),
            'day' => $dateTime->format('d'),
        ]);
        return ($status && $this->perform(Query\Slot::QUERY_DELETE_SLOT_HIERA));
    }

    /**
     * This function is for debugging
     */
    public function readRowsByScopeAndDate(
        ScopeEntity $scope,
        DateTimeInterface $dateTime
    ) {
        return $this->fetchAll(Query\Slot::QUERY_SELECT_BY_SCOPE_AND_DAY, [
            'year' => $dateTime->format('Y'),
            'month' => $dateTime->format('m'),
            'day' => $dateTime->format('d'),
            'scopeID' => $scope->id,
        ]);
    }

    public function writeOptimizedSlotTables(): bool
    {
        $status = true;
        $status = ($status && $this->perform(Query\Slot::QUERY_OPTIMIZE_SLOT));
        $status = ($status && $this->perform(Query\Slot::QUERY_OPTIMIZE_SLOT_HIERA));
        $status = ($status && $this->perform(Query\Slot::QUERY_OPTIMIZE_SLOT_PROCESS));
        return ($status && $this->perform(Query\Slot::QUERY_OPTIMIZE_PROCESS));
    }
}
