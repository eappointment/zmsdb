<?php
namespace BO\Zmsdb;

use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Session as SessionRepository;

use BO\Zmsentities\Apikey as Entity;
use BO\Zmsentities\Collection\ApikeyList;
use BO\Zmsentities\Exception\SchemaValidation;
use BO\Zmsentities\Schema\Entity as SchemaEntity;
use BO\Zmsentities\Calendar as CalendarEntity;
use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Collection\ProcessList as ProcessList;

use BO\Zmsdb\Exception\Useraccount\UseraccountNotFound as UseraccountNotFoundException;
use DateTimeImmutable;
use DateTimeInterface;
use PDO;

/**
 * @SuppressWarnings(Public)
 * @SuppressWarnings(CouplingBetweenObjects)
 */
class Apikey extends Base
{
    const API_KEY_SESSION_NAME = 'apikey';

    public static $cache = [];

    /**
     *
     * @param string $apiKey
     *
     * @return SchemaEntity|Entity
     */
    public function readEntity(string $apiKey): Entity
    {
        $query = new Query\Apikey(Query\Base::SELECT);
        $query
            ->addEntityMapping()
            ->addResolvedReferences(0)
            ->addConditionApikey($apiKey);
        $entity = $this->fetchOne($query, new Entity());
        if ($entity->hasId()) {
            $entity->quota = $this->readQuotaList($apiKey);
        }
        return $entity;
    }

    /**
     *
     * @param string $clientKey
     *
     * @return SchemaEntity|Entity
     */
    public function readEntityByClientId(int $clientId): Entity
    {
        $query = new Query\Apikey(Query\Base::SELECT);
        $query
            ->addEntityMapping()
            ->addResolvedReferences(0)
            ->addConditionClientId($clientId);
        $entity = $this->fetchOne($query, new Entity());
        if ($entity->hasId() && isset($entity->key)) {
            $entity->quota = $this->readQuotaList($entity->key);
        }
        return $entity;
    }

    /**
     *
     * @param string $clientKey
     *
     * @return ApikeyList
     */
    public function readListByClientId(int $clientId): ApikeyList
    {
        $collection = new ApikeyList();
        $query = new Query\Apikey(Query\Base::SELECT);
        $query
            ->addEntityMapping()
            ->addResolvedReferences(0)
            ->addConditionClientId($clientId);
        $result = $this->fetchList($query, new Entity());


        if (count($result) > 0) {
            foreach ($result as $entity) {
                if ($entity->hasId() && isset($entity->key)) {
                    $entity->quota = $this->readQuotaList($entity->key);
                    $collection->addEntity($entity);
                }
            }
        }
        return $collection;
    }

    /**
     * read entity by api token from useraccount, while useraccount::$id is the clientKey
     *
     * @param string $token
     *
     * @return SchemaEntity|Entity
     * @throws UseraccountNotFoundException
     */
    public function readEntityByToken(string $token): Entity
    {
        $useraccount = (new Useraccount())->readEntityByToken($token);
        if (! $useraccount->hasId()) {
            throw new UseraccountNotFoundException();
        }
        $apiClientId = (new Useraccount())->readEntityIdByLoginName($useraccount->getId());
        $entity = $this->readEntityByClientId($apiClientId);
        if ($entity->hasId() && isset($entity->key)) {
            $entity->quota = $this->readQuotaList($entity->key);
        }
        return $entity;
    }

    /**
     * write a new apikey
     *
     * @param Entity $entity
     *
     * @return SchemaEntity|Entity
     */
    public function writeEntity(Entity $entity): Entity
    {
        $query = new Query\Apikey(Query\Base::INSERT);
        $query->addValues([
            'key' => $entity->key,
            'createIP' => (isset($entity->createIP)) ? $entity->createIP : '',
            'ts' => ($entity->ts) ?: (new DateTimeImmutable)->getTimestamp(),
            'apiClientID' => isset($entity->getApiClient()->apiClientID) ? $entity->getApiClient()->apiClientID : 1,
        ]);
        if ($this->writeItem($query)) {
            $this->updateQuota($entity->key, $entity);
        }
        return $this->readEntity($entity->key);
    }

    /**
     * update an existing active apikey quota
     *
     * @param
     *      entity
     * @param Entity $entity
     *
     * @return SchemaEntity|Entity
     */
    public function updateEntity($apiKey, Entity $entity): Entity
    {
        $this->updateQuota($apiKey, $entity);
        return $this->readEntity($apiKey);
    }

    /**
     * update apikey locked status
     *
     * @param string $apiKey
     * @param int $locked
     *
     * @return SchemaEntity|Entity
     */
    public function withUpdatedLockedStatus(string $apiKey, int $locked = 0): Entity
    {
        $query = new Query\Apikey(Query\Base::UPDATE);
        $query->addConditionApikey($apiKey);
        $query->addValues([
            'locked' => $locked
        ]);
        $this->writeItem($query);
        return $this->readEntity($apiKey);
    }

    /**
     * read specified api quota
     *
     * @param $apiKey
     * @param $route
     *
     * @return array|false
     */
    public function readQuota($apiKey, $route)
    {
        $query = new Query\Apiquota(Query\Base::SELECT);
        $query
            ->addEntityMapping()
            ->addResolvedReferences(0)
            ->addConditionApikey($apiKey)
            ->addConditionRoute($route);
        $statement = $this->fetchStatement($query);
        return $statement->fetch(PDO::FETCH_ASSOC);
    }

    /**
     * read api quotas by apikey
     *
     * @param
     *      apiKey
     *      entity
     *
     * @return array
     */
    public function readQuotaList($apiKey): array
    {
        $data = $this
            ->getReader()
            ->fetchAll(
                Query\Apiquota::getQueryReadApiQuotaListByKey(),
                [
                    'key' => $apiKey
                ]
            );
        return ($data) ?: [];
    }

    /**
     * write initial api quotas
     *
     * @param $apiKey
     * @param $route
     * @param $period
     * @param $requests
     *
     * @return bool
     */
    public function writeQuota($apiKey, $route, $period, $requests): bool
    {
        $query = new Query\Apiquota(Query\Base::INSERT);
        $query->addValues([
            'key' => $apiKey,
            'route' => $route,
            'period' => $period,
            'requests' => $requests,
            'ts' => (new DateTimeImmutable)->getTimestamp()
        ]);
        return $this->writeItem($query);
    }

    /**
     * update api quotas
     *
     * @param string $apiKey
     * @param Entity $entity
     *
     * @return void
     */
    public function updateQuota(string $apiKey, Entity $entity): void
    {
        if (isset($entity->quota)) {
            foreach ($entity->quota as $quota) {
                if ($this->readQuota($apiKey, $quota['route'])) {
                    $query = new Query\Apiquota(Query\Base::UPDATE);
                    $query->addConditionApikey($apiKey);
                    $query->addConditionRoute($quota['route']);
                    $query->addValues([
                        'key' => $apiKey,
                        'route' => $quota['route'],
                        'period' => $quota['period'],
                        'requests' => $quota['requests']
                    ]);
                    $this->writeItem($query);
                } else {
                    $this->writeQuota($apiKey, $quota['route'], $quota['period'], $quota['requests']);
                }
            }
        }
    }

    /**
     * delete an existing outdated apikey
     *
     * @param string $apiKey
     *
     * @return void
     */
    public function deleteEntity(string $apiKey): void
    {
        $query = new Query\Apikey(Query\Base::DELETE);
        $query->addConditionApikey($apiKey);
        if ($this->deleteItem($query)) {
            $queryQuota = new Query\Apiquota(Query\Base::DELETE);
            $queryQuota->addConditionApikey($apiKey);
            $this->deleteItem($queryQuota);
        }
    }

    /**
     * delete api quota by its period setting and creation timestamp
     *
     * @param DateTimeInterface $dateTime
     *
     * @return array
     */
    public function readExpiredQuotaListByPeriod(DateTimeInterface $dateTime): array
    {
        return $this->fetchAll(Query\Apiquota::getQueryReadApiQuotaExpired($dateTime));
    }

    /**
     * delete an existing outdated apikey
     *
     * @param $quotaId
     *
     * @return bool
     */
    public function writeDeletedQuota($quotaId): bool
    {
        $query = new Query\Apiquota(Query\Base::DELETE);
        $query->addConditionQuotaId($quotaId);
        return $this->deleteItem($query);
    }

    /**
     * write calendar data to session
     *
     * @param string         $apiKey
     * @param CalendarEntity $calendar
     *
     * @return void
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     * @throws SchemaValidation
     */
    public function writeCalendar(string $apiKey, CalendarEntity $calendar, int $slotsRequired = 0): void
    {
        $session = (new SessionRepository())->readEntityBasic(self::API_KEY_SESSION_NAME, $apiKey);
        $session['content']['calendar'] = $calendar;
        $session['content']['slotsRequired'] = $slotsRequired;
        $session->testValid();
        (new SessionRepository())->updateEntityBasic($session);
    }

    /**
     * write process data to sessions processlist
     *
     * @param string        $apiKey
     * @param ProcessEntity $process
     *
     * @return ProcessList
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function writeProcessInSession(string $apiKey, ProcessEntity $process): ProcessList
    {
        $session = (new SessionRepository())->readEntityBasic(self::API_KEY_SESSION_NAME, $apiKey);
        $processList = (isset($session->getContent()['processList'])) ?
            (new ProcessList())->addData($session->getContent()['processList']) :
            new ProcessList();
        if ($processList->hasEntity($process->getId())) {
            $processList = $processList->withOutProcessId($process->getId());
        }
        $processList->addEntity($process);
        $session['content']['processList'] = $processList;
        $session->testValid();
        $session = (new SessionRepository())->updateEntityBasic($session);
        return (new ProcessList())->addData($session->getContent()['processList']);
    }
}
