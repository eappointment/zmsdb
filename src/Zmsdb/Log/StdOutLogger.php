<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Log;

class StdOutLogger extends TestLogger
{
    /**
     * @codeCoverageIgnore
     */
    public function log($level, string|\Stringable $message, array $context = []): void
    {
        echo $message . PHP_EOL;

        parent::log($level, $message, $context);
    }
}
