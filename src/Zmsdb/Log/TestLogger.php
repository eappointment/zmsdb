<?php

declare(strict_types=1);

namespace BO\Zmsdb\Log;

use Psr\Log\AbstractLogger;

class TestLogger extends AbstractLogger
{
    public $records = [];

    /**
     * @codeCoverageIgnore
     */
    public function log($level, string|\Stringable $message, array $context = []): void
    {
        $this->records[] = compact('level', 'message', 'context');
    }
}
