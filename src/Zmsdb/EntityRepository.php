<?php

declare(strict_types=1);

namespace BO\Zmsdb;

use BO\Zmsdb\Exception\EntityRepository\DataNotFound;
use BO\Zmsdb\Exception\RepositoryException;
use BO\Zmsentities\Schema\Entity;

abstract class EntityRepository extends Base
{
    protected $entityClass = 'BO\\Zmsentities\\Schema\\Entity';

    protected $entityCache = [];

    // @todo: (proposal) use this to resolve references or define other entity relations/attributes
//    protected $metaData = [
//        'properties' => [],
//        'references' => []
//    ];

    /**
     * @param int|string $identifier
     * @param int $resolveReferences
     * @param bool $useCache
     * @return ?Entity
     */
    public function readById(int|string $identifier, int $resolveReferences = 0, bool $useCache = false): ?Entity
    {
        $cacheKey = "$identifier#$resolveReferences";

        if (!$useCache || !isset($this->entityCache[$cacheKey])) {
            $query = $this->getQueryInstance(Query\Base::SELECT)->addEntityMapping();
            $query->addPrimaryComparison($identifier);
            if ($resolveReferences) {
                $query->addResolvedReferences($resolveReferences);
            }

            $entity = $this->fetchOne($query, new $this->entityClass());
            if (!$entity->getId()) {
                $entity = null;
            }
            // @todo: implement this when extending the function
//            if ($entity && $resolveReferences) {
//                $entity = $this->readResolvedReferences($entity, $resolveReferences);
//            }

            $this->entityCache[$cacheKey] = $entity;
        }

        return clone $this->entityCache[$cacheKey];
    }

    /**
     * stores an entity in the db
     */
    public function persist(Entity $entity, int $cascadeLevel = null): void
    {
        $query = $this->getQueryInstance(Query\Base::INSERT);
        $values = $query->reverseEntityMapping($entity);

        $query->addValues($values);

        if ($this->writeItem($query)) {
            $lastInsertId = $this->getWriter()->lastInsertId();
            if ($lastInsertId !== false && $lastInsertId !== '0') {
                $entity[$entity::PRIMARY] = $lastInsertId;

                // this could be replaced with metaData property types
                $data = $query->postProcess((array) $entity);
                $entity[$entity::PRIMARY] = $data[$entity::PRIMARY];
            }

            $cascadeLevel = $cascadeLevel ?? $entity->getResolveLevel();
            $cacheKey = (string) $entity->getId() . "#$cascadeLevel";
            $this->entityCache[$cacheKey] = $entity;

            // @todo: implement this when extending the function
//            if ($cascadeLevel) {
//                $referedEntities = $this->metaData['references'];
//            }
        }
    }

    /**
     * @param Entity $entity
     * @param int|null $cascadeLevel
     * @return void
     * @throws Exception\RepositoryException
     */
    public function update(Entity $entity, int $cascadeLevel = null): void
    {
        $query = $this->getQueryInstance(Query\Base::UPDATE);
        $values = $query->reverseEntityMapping($entity);
        unset($values[$query::PRIMARY]);
        $query->addValues($values);

        $query->addPrimaryComparison($entity->getId());

        try {
            $affected = $this->fetchAffected((string) $query, $query->getParameters());
        } catch (\Exception $exception) {
            $this->throwDefaultException($exception);
        }

        $cascadeLevel = $cascadeLevel ?? $entity->getResolveLevel();

        if (!$affected) {
            $persisted = $this->readById($entity->getId(), $cascadeLevel);

            if (!$persisted) {
                throw new DataNotFound('The update failed since there was nothing persisted before.');
            }

            if (!$this->isEqualEntities($entity, $persisted, $entity->getResolveLevel())) {
                throw new RepositoryException('A Database update failed though changes exist.');
            }
        }

//      @todo: implement this when extending the function
//        if ($cascadeLevel) {
//            $referedEntities = $this->metaData['references'];
//        }

        $cacheKey = (string) $entity->getId() . "#$cascadeLevel";
        $this->entityCache[$cacheKey] = $entity;
    }

    protected function getQueryInstance($queryType): Query\Base
    {
        $className = get_called_class();
        $withoutNS   = substr($className, strrpos($className, '\\') + 1);
        $queryClass = substr($className, 0, -strlen($withoutNS)) . 'Query\\' . $withoutNS;
        
        return new $queryClass($queryType);
    }

    protected function throwDefaultException($previous)
    {
        throw new RepositoryException('', 0, $previous);
    }

    /**
     * @SuppressWarnings(Param)
     */
    protected function isEqualEntities(Entity $invested, Entity $base, $resolveLevel): bool
    {
        $data = $invested->getArrayCopy();
        foreach ($base->getArrayCopy() as $key => $value) {
            if (isset($data[$key]) && $data[$key] != $value) {
                return false;
            }
        }

        //todo: implement tests sub-entities according to $resolveLevel when needed

        return true;
    }
}
