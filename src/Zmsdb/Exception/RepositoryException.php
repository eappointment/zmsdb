<?php

declare(strict_types=1);

namespace BO\Zmsdb\Exception;

use Throwable;

class RepositoryException extends \Exception
{
    protected $code = 500;

    protected $message = 'The data processing encountered an error.';

    public function __construct($message = "", $code = 0, Throwable $previous = null)
    {
        if ($message) {
            $this->message = $message;
        }

        if ($code) {
            $this->code = $code;
        }

        parent::__construct($message, $code, $previous);
    }
}
