<?php

namespace BO\Zmsdb\Exception\Process;

class ProcessArchiveUpdateFailed extends \Exception
{
    protected $code = 500;

    protected $message = 'Failed to update process archive.';
}
