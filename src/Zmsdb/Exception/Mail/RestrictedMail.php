<?php

namespace BO\Zmsdb\Exception\Mail;

class RestrictedMail extends \Exception
{
    protected $code = 400;

    protected $message = "invalid mail";
}
