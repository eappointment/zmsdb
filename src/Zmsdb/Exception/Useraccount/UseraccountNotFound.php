<?php

namespace BO\Zmsdb\Exception\Useraccount;

class UseraccountNotFound extends \Exception
{
    protected $code = 404;

    protected $message = "Useraccount not found or not existing";
}
