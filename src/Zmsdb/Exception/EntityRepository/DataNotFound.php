<?php

declare(strict_types=1);

namespace BO\Zmsdb\Exception\EntityRepository;

use BO\Zmsdb\Exception\RepositoryException;

class DataNotFound extends RepositoryException
{
    protected $message = 'Data or Entry was not found';
}
