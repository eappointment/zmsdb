<?php

declare(strict_types=1);

namespace BO\Zmsdb;

use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsentities\Appointment;
use BO\Zmsentities\CheckInTask as TaskEntity;
use BO\Zmsentities\Collection\CheckInTaskList as TaskList;
use BO\Zmsentities\Collection\Base as BaseList;
use BO\Zmsentities\Process;
use BO\Zmsentities\Schema\Entity;

/**
 * @method TaskEntity readById(int|string $identifier, int $resolveReferences = 0, bool $useCache = false)
 * @method TaskList fetchList(Query\Base $query, Entity $entity, TaskList $resultList)
 * @method TaskEntity fetchOne(Query\Base $query, TaskEntity $entity)
 *
 * @SuppressWarnings(CouplingBetweenObjects)
 */
class CheckInTask extends EntityRepository
{
    protected $entityClass = 'BO\\Zmsentities\\CheckInTask';

    /**
     * @param Entity|Process|Appointment $referenceObj
     * @return TaskEntity|null
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function readByReferenceObject(Entity $referenceObj): ?TaskEntity
    {
        $query = new Query\CheckInTask(Query\Base::SELECT);
        $query
            ->addEntityMapping()
            ->addReferenceIdComparison([(int) $referenceObj->getId()])
            ->addOrderBy('scheduledTS', 'DESC'); // Process->id is not unique

        $task = $this->fetchOne($query, new TaskEntity());
        if (!$task->getId()) {
            return null;
        }
        $this->entityCache[$task->getId() . '#0'] = $task;
        if (array_key_exists('checkInTask', $referenceObj->getDefaults())) {
            $referenceObj->checkInTask = $task;
        }

        return $task;
    }

    public function listByReferenceObjectList(BaseList $sourceList): TaskList
    {
        $taskList = new TaskList();
        if (!count($sourceList)) {
            return $taskList;
        }

        $query = (new Query\CheckInTask(Query\Base::SELECT))->addEntityMapping();
        /** @var Entity $sourceEntity */
        $sourceEntity = current((array) $sourceList);
        $referenceIds = array_column((array) $sourceList, $sourceEntity::PRIMARY);
        $query->addReferenceIdComparison($referenceIds);
        $query->addOrderBy('scheduledTS', 'DESC');

        /** @var TaskList|TaskEntity[] $list */
        $list = $this->fetchList($query, new TaskEntity(), $taskList);
        //only use the newest when the referenceId is not unique
        $trunc = [];
        foreach ($list as $index => $task) { // other solution would be a special query to get an order before groupBy
            if (isset($trunc[$task->referenceId])) {
                unset($list[$index]);
            }
            $trunc[$task->referenceId] = true;
        }

        return $list;
    }

    /**
     * @param int[] $scopeIds
     * @return TaskList
     * @throws Exception\Pdo\DeadLockFound
     * @throws Exception\Pdo\LockTimeout
     * @throws Exception\Pdo\PDOFailed
     */
    public function listByScopeIds(array $scopeIds): TaskList
    {
        $scopeIds = array_map('intval', $scopeIds);
        $taskList = new TaskList();
        if (!count($scopeIds)) {
            return $taskList;
        }

        $query = (new Query\CheckInTask(Query\Base::SELECT))->addEntityMapping();
        $query->addScopeIdComparison($scopeIds);

        return $this->fetchList($query, new TaskEntity(), $taskList);
    }

    /**
     * @param string[] $statuses
     * @return TaskList
     * @throws Exception\Pdo\DeadLockFound
     * @throws Exception\Pdo\LockTimeout
     * @throws Exception\Pdo\PDOFailed
     */
    public function listByStatuses(array $statuses): TaskList
    {
        $statuses = array_filter($statuses, 'is_string');
        $statuses = array_filter($statuses, 'strlen');
        $taskList = new TaskList();
        if (!count($statuses)) {
            return $taskList;
        }

        $query = (new Query\CheckInTask(Query\Base::SELECT))->addEntityMapping();
        $query->addStatusComparison($statuses);


        return $this->fetchList($query, new TaskEntity(), $taskList);
    }

    /**
     * @param int $fromTS
     * @param ?int $toTS
     * @return TaskList
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function listByCheckInTime(int $fromTS, ?int $toTS = null): TaskList
    {
        $taskList = new TaskList();
        if ($fromTS === 0 && !is_int($toTS)) {
            return $taskList;
        }

        $query = (new Query\CheckInTask(Query\Base::SELECT))->addEntityMapping();
        $query->addCheckInTSLimits($fromTS, $toTS);
        $query->addOrderBy('checkInTS');

        return $this->fetchList($query, new TaskEntity(), $taskList);
    }

    /**
     * @param int $fromTS
     * @param ?int $toTS
     * @return TaskList
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function listByScheduledTime(int $fromTS = 0, ?int $toTS = null): TaskList
    {
        $taskList = new TaskList();
        if ($fromTS === 0 && !is_int($toTS)) {
            return $taskList;
        }

        $query = (new Query\CheckInTask(Query\Base::SELECT))->addEntityMapping();
        $query->addScheduledTSLimits($fromTS, $toTS);
        $query->addOrderBy('scheduledTS');

        return $this->fetchList($query, new TaskEntity(), $taskList);
    }

    /**
     * @return int (number of deleted items/lines)
     */
    public function deleteOutdated(\DateTimeInterface $now): int
    {
        $deleteQuery = new Query\CheckInTask(Query\Base::DELETE);
        $deleteQuery->addOutdatedCondition($now);

        return $this->fetchAffected($deleteQuery, $deleteQuery->getParameters());
    }
}
