<?php

namespace BO\Zmsdb;

use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsentities\RemoteCall as Entity;
use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\EventLog as EventLogEntity;
use BO\Zmsentities\Collection\RemoteCallList as Collection;

class RemoteCall extends Base
{
    public static $cache = [];

    /**
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function readEntity(string $primary): Entity
    {
        $query = new Query\RemoteCall(Query\Base::SELECT);
        $query
            ->setResolveLevel(0)
            ->addEntityMapping()
            ->addConditionPrimary($primary);
        return $this->fetchOne($query, new Entity());
    }

    /**
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function readListByProcessArchiveId(string $processArchiveId): Collection
    {
        $query = new Query\RemoteCall(Query\Base::SELECT);
        $query
            ->setResolveLevel(0)
            ->addEntityMapping()
            ->addConditionProcessArchiveId($processArchiveId);

        return $this->fetchList($query, new Entity(), new Collection());
    }

    /**
     * write a new remotecall
     *
     * @param Entity $entity
     *
     * @return Entity
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function writeNewEntity(Entity $entity): Entity
    {
        $query = new Query\RemoteCall(Query\Base::INSERT);
        $query->addValues([
            'processArchiveId' => $entity->processArchiveId,
            'name' => $entity->name,
            'url' => $entity->url,
            'call__count' => $entity->call['count'],
            'call__statuscode' => $entity->call['statuscode'],
            'call__requested' => $entity->call['requested'],
        ]);
        if ($entity->call['count'] > 0) {
            $query->addValues([
                'call__timestamp' =>  $entity->call['timestamp'],
            ]);
        }
        $this->writeItem($query);
        $newId = $this->getWriter()->lastInsertId();
        return $this->readEntity($newId);
    }

    /**
     * @param $entity
     *
     * @return Entity
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function writeUpdatedEntity($entity): Entity
    {
        $query = new Query\RemoteCall(Query\Base::UPDATE);
        $query->addConditionPrimary($entity->getId());
        $query->addValues([
            'name' => $entity->name,
            'url' => $entity->url,
            'call__count' => $entity->call['count'],
            'call__requested' => $entity->call['requested'],
            'call__statuscode' => $entity->call['statuscode'],
            'call__timestamp' => (isset($entity->call['timestamp'])) ? $entity->call['timestamp'] : null,
        ]);
        $this->writeItem($query);
        return $this->readEntity($entity->getId());
    }

    /**
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function writeCallRequested(ProcessEntity $process, string $name): void
    {
        $remoteCallList = $this->readListByProcessArchiveId($process->processArchiveId);
        if ($remoteCallList->count() > 0 && $entity = $remoteCallList->getByName($name)) {
            $query = new Query\RemoteCall(Query\Base::UPDATE);
            $query->addConditionPrimary($entity->getId());
            $query->addValues([
                'call__requested' => 1
            ]);
            $this->writeItem($query);
            $entity = $this->readEntity($entity->getId());
            $this->writeEventLog($process, $entity);
        }
    }

    /**
     * Update a remoteCall collection
     *
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function updateList(Collection $updateList): Collection
    {
        $list = new Collection();
        /** @var Entity $updateEntity */
        foreach ($updateList as $updateEntity) {
            $existingList = $this->readListByProcessArchiveId($updateEntity->processArchiveId);
            if (($updateEntity->hasId() && $existingList->hasEntity($updateEntity->getId())) ||
                $existingList->getMatchingByNameAndUrl($updateEntity) !== null
            ) {
                $entity = $this->writeUpdatedEntity($updateEntity);
                $list->addEntity($entity);
            } elseif ($updateEntity->isTypeAllowed()) {
                $entity = $this->writeNewEntity($updateEntity);
                $list->addEntity($entity);
            }
        }
        return $list;
    }

    public function deleteEntity($primary): void
    {
        $query = new Query\RemoteCall(Query\Base::DELETE);
        $query->addConditionPrimary($primary);
        $this->deleteItem($query);
    }

    /**
     * @param $processArchiveId
     *
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function deleteList($processArchiveId): void
    {
        $query = new Query\RemoteCall(Query\Base::DELETE);
        $query->addConditionProcessArchiveId($processArchiveId);
        $this->deleteItem($query);
    }

    protected function writeEventLog(ProcessEntity $process, Entity $entity): bool
    {
        if (! $entity->call['requested'] == 1) {
            return false;
        }
        $logRepository = new EventLog();
        $eventLogEntity = new EventLogEntity();
        $eventLogEntity->addData([
            'name' => self::getRelatedEventName($entity),
            'origin' => 'zmsdb',
            'referenceType' => 'remotecall.id',
            'reference' => $entity->getId(),
            'context' => ['process' => $process],
        ])->setSecondsToLive(EventLogEntity::LIVETIME_DAY * 3);
        return $logRepository->writeEntity($eventLogEntity);
    }

    public static function getRelatedEventName(Entity $entity): string
    {
        return 'REMOTECALL_'. strtoupper($entity->name) .'_REQUEST';
    }
}
