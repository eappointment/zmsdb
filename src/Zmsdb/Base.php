<?php

namespace BO\Zmsdb;

use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsentities\Schema\Entity;
use BO\Zmsentities\Collection\Base as BaseCollection;
use Closure;
use Exception;
use InvalidArgumentException;
use PDO;
use PDOException;
use PDOStatement;

/**
 * @SuppressWarnings(NumberOfChildren)
 * @SuppressWarnings(Public)
 *
 */
abstract class Base
{
    /**
     * @var PDO $writeDb Database connection
     */
    protected $writeDb = null;

    /**
     * @var PDO $readDb Database connection
     */
    protected $readDb = null;

    /**
     * Cache prepared statements
     *
     */
    protected static $preparedCache = [];

    /**
     * Make sure, we do not cache statements on different connections
     *
     */
    protected static $preparedConnectionId = null;

    /**
     * @param PDO|null $writeConnection
     * @param PDO|null $readConnection
     */
    public function __construct(PDO $writeConnection = null, PDO $readConnection = null)
    {
        $this->writeDb = $writeConnection;
        $this->readDb = $readConnection;
    }

    public static function init(PDO $writeConnection = null, PDO $readConnection = null): Base
    {
        return new static($writeConnection, $readConnection);
    }

    /**
     * @return PDO
     * @throws PDOFailed
     */
    public function getWriter(): PDO
    {
        if (null === $this->writeDb) {
            $this->writeDb = Connection\Select::getWriteConnection();
            $this->readDb = $this->writeDb;
        }
        return $this->writeDb;
    }

    /**
     * @return PDO
     */
    public function getReader(): ?PDO
    {
        if (null !== $this->writeDb) {
            // if readDB gets a reset, still use writeDB
            return $this->writeDb;
        }
        if (null === $this->readDb) {
            $this->readDb= Connection\Select::getReadConnection();
        }
        return $this->readDb;
    }

    /**
     * @param string $query
     *
     * @return mixed
     */
    public function fetchPreparedStatement($query)
    {
        $sql = "$query";
        $reader = $this->getReader();
        if (spl_object_hash($reader) != static::$preparedConnectionId) {
            // do not use prepared statements on a different connection
            static::$preparedCache = [];
            static::$preparedConnectionId = spl_object_hash($reader);
        }
        if (!isset(static::$preparedCache[$sql])) {
            $prepared = $this->getReader()->prepare($sql);
            static::$preparedCache[$sql] = $prepared;
        }
        return static::$preparedCache[$sql];
    }

    /**
     * @param PDOStatement   $statement
     * @param array $parameters
     *
     * @return mixed
     *
     * @throws PDOFailed
     * @throws LockTimeout
     * @throws DeadLockFound
     */
    public function startExecute($statement, $parameters)
    {
        return static::pdoExceptionHandler(function () use ($statement, $parameters) {
            $statement->execute($parameters);
            return $statement;
        });
    }

    /**
     * @throws PDOFailed
     * @throws LockTimeout
     * @throws DeadLockFound
     */
    protected static function pdoExceptionHandler(Closure $pdoFunction, $parameters = [])
    {
        try {
            $statement = $pdoFunction($parameters);
        } catch (PDOException $pdoException) {
            if (stripos($pdoException->getMessage(), 'Lock wait timeout') !== false) {
                throw new LockTimeout();
            }
            //@codeCoverageIgnoreStart
            if (stripos($pdoException->getMessage(), 'Deadlock found') !== false) {
                throw new DeadLockFound();
            }
            //@codeCoverageIgnoreEnd
            $message = "SQL: "
                . " Err: "
                .$pdoException->getMessage()
                //. " || Statement: "
                //.$statement->queryString
                //." || Parameters=". var_export($parameters, true)
                ;

            throw new PDOFailed($message, 0, $pdoException);
        }
        return $statement;
    }

    /**
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     */
    public function fetchStatement(Query\Base $query)
    {
        $parameters = $query->getParameters();
        return $this->startExecute($this->fetchPreparedStatement($query), $parameters);
    }

    /**
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws Exception
     */
    public function fetchOne(Query\Base $query, Entity $entity): Entity
    {
        $statement = $this->fetchStatement($query);
        $data = $statement->fetch(PDO::FETCH_ASSOC);
        if ($data) {
            $entity->exchangeArray($query->postProcessJoins($data));
            $entity->setResolveLevel($query->getResolveLevel());
        }
        $statement->closeCursor();
        return $entity;
    }

    /**
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     */
    public function fetchRow($query, $parameters = null)
    {
        return static::pdoExceptionHandler(function () use ($query, $parameters) {
            $prepared = $this->fetchPreparedStatement($query);
            $prepared->execute($parameters);
            $row = $prepared->fetch(PDO::FETCH_ASSOC);
            $prepared->closeCursor();
            return $row;
        });
    }

    /**
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     */
    public function fetchValue($query, $parameters = null)
    {
        return static::pdoExceptionHandler(function () use ($query, $parameters) {
            $prepared = $this->fetchPreparedStatement($query);
            $prepared->execute($parameters);
            $value = $prepared->fetchColumn();
            $prepared->closeCursor();
            return $value;
        });
    }

    /**
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     */
    public function fetchAll($query, $parameters = null)
    {
        return static::pdoExceptionHandler(function () use ($query, $parameters) {
            $prepared = $this->fetchPreparedStatement($query);
            $prepared->execute($parameters);
            $list = $prepared->fetchAll(PDO::FETCH_ASSOC);
            $prepared->closeCursor();
            return $list;
        });
    }

    /**
     * @param Query\Base $query
     * @param array|null $parameters
     * @return bool
     *
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     */
    public function fetchHandle($query, $parameters = null)
    {
        return static::pdoExceptionHandler(function () use ($query, $parameters) {
            $prepared = $this->fetchPreparedStatement($query);
            $prepared->execute($parameters);
            return $prepared;
        });
    }

    /**
     * @param Query\Base           $query
     * @param Entity               $entity
     * @param BaseCollection|array $resultList
     *
     * @return BaseCollection|array
     *
     * @throws PDOFailed
     * @throws LockTimeout
     * @throws DeadLockFound
     * @throws Exception
     */
    public function fetchList(Query\Base $query, Entity $entity, $resultList = [])
    {
        $statement = $this->fetchStatement($query);
        while ($data = $statement->fetch(PDO::FETCH_ASSOC)) {
            $dataEntity = clone $entity;
            $dataEntity->exchangeArray($query->postProcessJoins($data));
            $dataEntity->setResolveLevel($query->getResolveLevel());
            $resultList[] = $dataEntity;
        }
        $statement->closeCursor();
        return $resultList;
    }

    /**
     * Write an Item to database - Insert, Update
     *
     * @param Query\Base $query
     *
     * @return bool
     * @throws PDOFailed
     * @throws LockTimeout
     * @throws DeadLockFound
     *
     */
    public function writeItem(Query\Base $query): bool
    {
        return static::pdoExceptionHandler(function () use ($query) {
            $this->getWriter(); //Switch to writer for write/delete
            $statement = $this->fetchPreparedStatement($query);
            $status = $statement->execute($query->getParameters());
            $statement->closeCursor();
            return $status;
        });
    }

    /**
     * @param Query\Base $query
     *
     * @return bool
     *
     * @throws PDOFailed
     * @throws LockTimeout
     * @throws DeadLockFound
     */
    public function deleteItem(Query\Base $query): bool
    {
        return $this->writeItem($query);
    }

    /**
     * @param Query\Base $query
     *
     * @return int
     *
     * @throws PDOFailed
     * @throws LockTimeout
     * @throws DeadLockFound
     *
     */
    public function deleteItemWithCount(Query\Base $query): int
    {
        return static::pdoExceptionHandler(function () use ($query) {
            $this->getWriter(); //Switch to writer for write/delete
            /** @var PDOStatement $prepared */
            $prepared = $this->fetchPreparedStatement($query);
            $prepared->execute($query->getParameters());
            $count = $prepared->rowCount();
            $prepared->closeCursor();
            return $count;
        });
    }

    /**
     * @param string     $sql
     * @param array|null $parameters
     *
     * @return bool
     *
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     */
    public function perform($sql, $parameters = null)
    {
        return static::pdoExceptionHandler(function () use ($sql, $parameters) {
            $this->getWriter(); //Switch to writer for perform
            /** @var PDOStatement $prepared */
            $prepared = $this->fetchPreparedStatement($sql);
            $status = $prepared->execute($parameters);
            $prepared->closeCursor();
            return $status;
        });
    }

    /**
     * @param string     $sql
     * @param array|null $parameters
     *
     * @return int
     *
     * @throws PDOFailed
     * @throws LockTimeout
     * @throws DeadLockFound
 */
    public function fetchAffected($sql, $parameters = null)
    {
        return static::pdoExceptionHandler(function () use ($sql, $parameters) {
            $this->getWriter(); //Switch to writer for perform
            $prepared = $this->fetchPreparedStatement($sql);
            $prepared->execute($parameters);
            $count = $prepared->rowCount();
            $prepared->closeCursor();
            return $count;
        });
    }

    /**
     * @SuppressWarnings(Param)
     * @codeCoverageIgnore
     *
     * @param Entity $entity
     * @param int    $resolveReferences
     *
     * @return Entity
     */
    public function readResolvedReferences(Entity $entity, $resolveReferences)
    {
        return $entity;
    }

    /**
     * This function produces a hash value that contains info for comparison
     *
     * @param string        $value
     * @param callable|null $callable
     *
     * @return string
     */
    public function hashStringValue(string $value, callable $callable = null): string
    {
        if ($callable === null) {
            $callable = 'sha1';
        }

        if (is_callable($callable)) {
            $hash = $callable($value);

            if (is_string($callable)) {
                return $callable . ':' . $hash;
            }
            if ($callable instanceof Closure) {
                return 'closure:' . $hash;
            }
            // else
            return 'custom:' . $hash;
        }

        throw new InvalidArgumentException('hashStringValue() should be called with a callable as second parameter.');
    }
}
