<?php
namespace BO\Zmsdb;

use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsentities\Schema\Entity as SchemaEntity;
use BO\Zmsentities\Process as Entity;
use BO\Zmsentities\Collection\ProcessList as Collection;
use DateTimeInterface;

/**
 *
 * @SuppressWarnings(CouplingBetweenObjects)
 * @SuppressWarnings(TooManyPublicMethods)
 * @SuppressWarnings(Complexity)
 * @SuppressWarnings(TooManyMethods)
 */
class ProcessStatusArchived extends Process
{
    public function readArchivedEntity($archiveId, $resolveReferences = 0)
    {
        if (!$archiveId) {
            return null;
        }
        $query = new Query\ProcessStatusArchived(Query\Base::SELECT);
        $query->addEntityMapping()
            ->addResolvedReferences($resolveReferences)
            ->addConditionArchiveId($archiveId);
        $archive = $this->fetchOne($query, new Entity());
        return $this->readResolvedReferences($archive, $resolveReferences);
    }

    public function readListByScopeId($scopeId, $resolveReferences = 0)
    {
        $query = new Query\ProcessStatusArchived(Query\Base::SELECT);
        $query->addEntityMapping()
            ->addResolvedReferences($resolveReferences)
            ->addConditionScopeId($scopeId);
        return $this->readResolvedList($query, $resolveReferences);
    }

    public function readListByDate($dateTime, $resolveReferences = 0)
    {
        $query = new Query\ProcessStatusArchived(Query\Base::SELECT);
        $query->addEntityMapping()
            ->addResolvedReferences($resolveReferences)
            ->addConditionTime($dateTime);
        return $this->readResolvedList($query, $resolveReferences);
    }

    public function readListForStatistic($dateTime, \BO\Zmsentities\Scope $scope, $limit = 500, $resolveReferences = 0)
    {
        $query = new Query\ProcessStatusArchived(Query\Base::SELECT);
        $query->addEntityMapping()
            ->addResolvedReferences($resolveReferences)
            ->addJoinStatisticFailed($dateTime, $scope)
            ->addLimit($limit);
        return $this->readResolvedList($query, $resolveReferences);
    }

    public function readListIsMissed($isMissed = 1, $resolveReferences = 0)
    {
        $query = new Query\ProcessStatusArchived(Query\Base::SELECT);
        $query->addEntityMapping()
            ->addResolvedReferences($resolveReferences)
            ->addConditionIsMissed($isMissed);
        return $this->readResolvedList($query, $resolveReferences);
    }

    public function readListWithAppointment($withAppointment = 1, $resolveReferences = 0)
    {
        $query = new Query\ProcessStatusArchived(Query\Base::SELECT);
        $query->addEntityMapping()
            ->addResolvedReferences($resolveReferences)
            ->addConditionWithAppointment($withAppointment);
        return $this->readResolvedList($query, $resolveReferences);
    }

    protected function readResolvedList($query, $resolveReferences)
    {
        $processList = new Collection();
        $resultList = $this->fetchList($query, new Entity());
        if (count($resultList)) {
            foreach ($resultList as $entity) {
                if (0 == $resolveReferences) {
                    $processList->addEntity($entity);
                } else {
                    if ($entity instanceof Entity) {
                        $entity = $this->readResolvedReferences($entity, $resolveReferences);
                        $processList->addEntity($entity);
                    }
                }
            }
        }
        return $processList;
    }

    public function writeEntityFinished(
        Entity            $process,
        DateTimeInterface $now
    ) {
        $archived = null;
        if ($this->writeBlockedEntity($process)) {
            $archived = $this->writeArchivedProcess($process, $now);
            if ($process->processArchiveId) {
                (new ProcessArchive())->writeArchivedEntity($process->processArchiveId, $archived->archiveId);
            }
        }
        // update xRequest entry and update process id as well as archived id
        if ($archived) {
            $this->writeXRequestsArchived($process->id, $archived->archiveId);
        }
        return $archived;
    }

    /**
     * write an archived process to statistic table
     *
     */
    public function writeArchivedProcessToStatistic(
        Entity $process,
        $requestId,
        $clusterId,
        $providerId,
        $departmentId,
        $organisationId,
        $ownerId,
        $dateTime
    ) {
        return $this->perform(
            Query\ProcessStatusArchived::QUERY_INSERT_IN_STATISTIC,
            [
                'archiveId' => $process->archiveId,
                'scopeId' => $process->scope->getId(),
                'clusterId' => $clusterId,
                'providerId' => $providerId,
                'departmentId' => $departmentId,
                'organisationId' => $organisationId,
                'ownerId' => $ownerId,
                'date' => $process->getFirstAppointment()->toDateTime()->format('Y-m-d'),
                'withAppointment' => ($process->toQueue($dateTime)->withAppointment) ? 1 : 0,
                'requestId' => $requestId
            ]
        );
    }

    /**
     * write a new archived process to DB
     *
     * @param Entity             $process
     * @param DateTimeInterface $now
     * @param int                $resolveReferences
     *
     * @return Entity|SchemaEntity
     * @throws DeadLockFound
     * @throws LockTimeout
     * @throws PDOFailed
     */
    public function writeArchivedProcess(
        Entity            $process,
        DateTimeInterface $now,
        int               $resolveReferences = 0
    ): ?Entity {
        $query = new Query\ProcessStatusArchived(Query\Base::INSERT);
        $query->addValuesNewArchive($process, $now);
        $this->writeItem($query);
        $archiveId = $this->getWriter()->lastInsertId();
        Log::writeLogEntry("ARCHIVE (Archive::writeNewArchivedProcess) $archiveId -> $process ", $process->id);
        if (!$process->toQueue($now)->withAppointment) {
            (new ExchangeWaitingscope())->writeWaitingTime($process, $now);
        }
        return $this->readArchivedEntity($archiveId, $resolveReferences);
    }

    protected function writeXRequestsArchived($processId, $archiveId)
    {
        $query = new Query\XRequest(Query\Base::UPDATE);
        $query->addConditionProcessId($processId);
        $query->addValues([
            'BuergerID' => 0,
            'BuergerarchivID' => $archiveId
        ]);
        $this->writeItem($query);
    }
}
