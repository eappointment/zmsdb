<?php

namespace BO\Zmsdb;

use BO\Zmsentities\Exchange as ExchangeEntity;

class ExchangeSlotoverbooking extends Base
{
    public function readEntity(): ExchangeEntity
    {
        $entity = new ExchangeEntity();
        $entity['title'] = "Übersicht zur Überbuchung von Slots an Standorten nach Tagen";
        //$entity->setPeriod($datestart, $dateend, $period);
        $entity->addDictionaryEntry('subjectid', 'string', 'ID of a scope', 'scope.id');
        $entity->addDictionaryEntry('date', 'string', 'Date of day');
        $entity->addDictionaryEntry('bookedcount', 'number', 'booked slots');
        $entity->addDictionaryEntry('plannedcount', 'number', 'planned slots');
        $entity->addDictionaryEntry('capacityutilization', 'number', 'Capacity utilization in percent');
 
        $entity['visualization']['xlabel'] = ["subjectid"];
        $entity['visualization']['ylabel'] = ["capacityutilization"];

        $raw = $this
            ->fetchAll(
                constant("\BO\Zmsdb\Query\ExchangeSlotoverbooking::QUERY_READ_REPORT"),
                []
            );
        foreach ($raw as $entry) {
            $entity->addDataSet(array_values($entry));
        }
        return $entity;
    }

    /**
     * @throws \Exception
     */
    public function readSubjectList(): ExchangeEntity
    {
        $entity = new ExchangeEntity();
        $entity['title'] = "Slotbelegung ";
        $entity->addDictionaryEntry('id', 'string', '', '');
        $entity->addDataSet(["_"]);
        return $entity;
    }

    /**
     * @SuppressWarnings(Unused)
     */
    public function readPeriodList($subjectid, $period = 'day'): ExchangeEntity
    {
        $entity = new ExchangeEntity();
        $entity['title'] = "Slotbelegung ";
        $entity->addDictionaryEntry('id', 'string', '', '');
        $entity->addDataSet(["_"]);
        return $entity;
    }
}
