<?php

namespace BO\Zmsdb;

use BO\Zmsentities\Collection\ApiclientList;
use BO\Zmsentities\Schema\Entity as SchemaEntity;
use BO\Zmsentities\Apiclient as Entity;

class Apiclient extends Base
{
    public static $cache = [];

    /**
     * @param $clientKey
     *
     * @return Entity|SchemaEntity
     */
    public function readEntity($clientKey): Entity
    {
        $query = new Query\Apiclient(Query\Base::SELECT);
        $query
            ->addEntityMapping()
            ->addResolvedReferences(0)
            ->addConditionApiclientKey($clientKey);
        return $this->fetchOne($query, new Entity());
    }

    /**
     * read list of apiclient with invalidation settings
     *
     * @return ApiclientList
     */
    public function readListWithInvalidation(): ApiclientList
    {
        $collection = new ApiclientList();
        $query = new Query\Apiclient(Query\Base::SELECT);
        $query
            ->addEntityMapping()
            ->addResolvedReferences(0)
            ->addSecondsToInvalidation();
        $result = $this->fetchList($query, new Entity());
        if (count($result) > 0) {
            $collection->addData($result);
        }
        return $collection;
    }

    /**
     *
     * @return Entity|SchemaEntity
     */
    public function readEntityByUserLoginName(string $userLoginName): Entity
    {
        $query = new Query\Apiclient(Query\Base::SELECT);
        $query
            ->addEntityMapping()
            ->addResolvedReferences(0)
            ->addConditionShortname($userLoginName);
        return $this->fetchOne($query, new Entity());
    }

    /**
     * write a new apiclient
     *
     * @param Entity $entity
     *
     * @return Entity
     */
    public function writeEntity(Entity $entity): Entity
    {
        $query = new Query\Apiclient(Query\Base::INSERT);
        $query->addValues([
            'clientKey' => $entity->clientKey,
            'shortname' => $entity->shortname,
            'accesslevel' => $entity->accesslevel,
            'permission__remotecall' => (isset($entity->permission['remotecall'])) ?
                $entity->permission['remotecall'] :
                0,
            'secondsToInvalidation' => $entity->secondsToInvalidation,
        ]);

        $this->writeItem($query);
        return $this->readEntity($entity->clientKey);
    }

    /**
     * write a new apiclient
     *
     * @param Entity $entity
     *
     * @return Entity
     */
    public function updateEntity(Entity $entity): Entity
    {
        $query = new Query\Apiclient(Query\Base::UPDATE);
        $query->addValues([
            'clientKey' => $entity->clientKey,
            'shortname' => $entity->shortname,
            'accesslevel' => $entity->accesslevel,
            'permission__remotecall' => (isset($entity->permission['remotecall'])) ?
                $entity->permission['remotecall'] :
                0,
            'secondsToInvalidation' => $entity->secondsToInvalidation,
        ]);

        $this->writeItem($query);
        return $this->readEntity($entity->clientKey);
    }
}
