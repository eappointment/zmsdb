<?php

declare(strict_types=1);

namespace BO\Zmsdb;

use BO\Zmsentities\CheckInConfig as ConfigEntity;
use BO\Zmsentities\Collection\CheckInConfigList;

/**
 * @method ConfigEntity readById(int|string $identifier, int $resolveReferences = 0, bool $useCache = false)
 * @method CheckInConfigList fetchList(Query\Base $query, ConfigEntity $entity, CheckInConfigList $resultList = [])
 */
class CheckInConfig extends EntityRepository
{
    protected $entityClass = 'BO\\Zmsentities\\CheckInConfig';

    public function readByScopeId(int $scopeId): ?ConfigEntity
    {
        $query = new Query\CheckInConfig(Query\Base::SELECT);
        $query
            ->addEntityMapping()
            ->addScopeIdComparison($scopeId);

        $config = $this->fetchOne($query, new ConfigEntity());
        if (!$config->getId()) {
            $config = null;
        }
        $this->entityCache[$config->getId() . '#0'] = $config;

        /** @var ConfigEntity|null $config */
        return $config;
    }

    public function listByDepartmentId(int $departmentId): CheckInConfigList
    {
        $query = new Query\CheckInConfig(Query\Base::SELECT);
        $query
            ->addEntityMapping()
            ->addConditionDepartmentId($departmentId)
            ->addGroupBy('id');

        return $this->fetchList($query, new ConfigEntity(), new CheckInConfigList());
    }

    /**
     * @return int (number of deleted items/lines)
     */
    public function deleteOutdated(\DateTimeInterface $now): int
    {
        $deleteQuery = new Query\CheckInConfig(Query\Base::DELETE);
        $deleteQuery->addOutdatedCondition($now);

        return $this->fetchAffected($deleteQuery, $deleteQuery->getParameters());
    }
}
