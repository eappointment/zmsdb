<?php

namespace BO\Zmsdb\Connection;

use BO\Zmsdb\Exception\Pdo\PDOFailed;

/**
 *
 * @codeCoverageIgnore
 *
 * @SuppressWarnings(TooManyFields)
 * @SuppressWarnings(LongVariable)
 * Handle read and write connections
 */
class Select
{
    /**
     * @var Bool $enableProfiling
     */
    public static $enableProfiling = false;

    /**
     * @var String $readSourceName PDO connection string
     */
    public static $readSourceName = null;

    /**
     * @var String $writeSourceName PDO connection string
     */
    public static $writeSourceName = null;

    /**
     * @var String $dbname_zms
     */
    public static $dbname_zms = 'zmsbo';

    /**
     * @var String $username Login
     */
    public static $username = null;

    /**
     * @var String $password Credential
     */
    public static $password = null;

    /**
     * @var Array $pdoOptions compatible to the 4th PDO::__construct parameter
     */
    public static $pdoOptions = [];

    /**
     * @var String $connectionTimezone
     *
     */
    public static $connectionTimezone = ' UTC'; // not set explicitly with connection yet

    /** @var string|null */
    public static $connectionTimezoneDifference = null; // system default when null

    /**
     * @var Bool $enableWsrepSyncWait
     */
    public static $enableWsrepSyncWait = false;

    /**
     * @var Bool $enableWsrepSyncWait
     */
    public static $galeraConnection = false;

    /**
     * @var PdoInterface $readConnection for read only requests
     */
    protected static $readConnection = null;

    /**
     * @var PdoInterface $writeConnection for write only requests
     */
    protected static $writeConnection = null;

    /**
     * @var \Aura\Sql\Profiler $readProfiler for read only requests
     */
    protected static $readProfiler = null;

    /**
     * @var \Aura\Sql\Profiler $writeProfiler for write only requests
     */
    protected static $writeProfiler = null;

    /**
     * @var Bool $useTransaction
     *
     */
    protected static $useTransaction = false;

    /**
     * @var Bool $useProfiling
     *
     */
    protected static $useProfiling = false;

    /**
     * @var Bool $useQueryCache
     *
     */
    protected static $useQueryCache = true;

    /**
     * Create a PDO compatible object
     *
     * @param  String $dataSourceName compatible with PDO
     * @return PdoInterface
     */
    protected static function createPdoConnection($dataSourceName)
    {
        try {
            $pdoOptions = array_merge([
                ], self::$pdoOptions);
            $pdo = new Pdo($dataSourceName, self::$username, self::$password, $pdoOptions);
            $pdo->exec('SET NAMES "UTF8";');
            //$timezone = date_default_timezone_get();
            //$pdo->prepare('SET time_zone = ?;')->execute([$timezone]);
            $pdo->exec('SET SESSION sql_mode = "STRICT_ALL_TABLES";');
            $pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (\Exception $exception) {
            // Extend exception message with connection information
            $connectInfo = $dataSourceName;
            throw new \BO\Zmsdb\Exception\Pdo\PDOFailed(
                $connectInfo . $exception->getMessage(),
                (int)$exception->getCode(),
                $exception
            );
        }
        return $pdo;
    }

    /**
     * Set the read connection.
     * Usually this function is only required to set mockups for testing
     *
     * @param PdoInterface $connection
     */
    public static function setReadConnection(PdoInterface $connection)
    {
        self::$readConnection = $connection;
    }

    /**
     * Create or return a connection for reading data
     *
     * @return PdoInterface
     */
    public static function getReadConnection()
    {
        if (null === self::$readConnection) {
            self::$readConnection = self::createPdoConnection(self::$readSourceName);
            self::$readProfiler = new \Aura\Sql\Profiler\Profiler();
            self::$readProfiler->setActive(self::$enableProfiling);
            self::$readConnection->setProfiler(self::$readProfiler);
            //self::$readConnection->exec('SET SESSION TRANSACTION READ ONLY');
            if (!self::$useQueryCache) {
                try {
                    self::$readConnection->exec('SET SESSION query_cache_type = 0;');
                } catch (\Exception $exception) {
                    // ignore, query cache might be disabled
                }
            }
            if (self::$useProfiling) {
                self::$readConnection->exec('SET profiling = 1;');
            }
            if (self::$connectionTimezoneDifference) {
                self::$readConnection->exec("SET time_zone = '" . self::$connectionTimezoneDifference . "';");
            }
        }
        return self::$readConnection;
    }

    /**
     * Test if a read connection is established
     *
     */
    public static function hasReadConnection()
    {
        return (null === self::$readConnection) ? false : true;
    }

    /**
     * Close a connection for reading data
     *
     */
    public static function closeReadConnection()
    {
        self::$readConnection = null;
    }

    /**
     * Set the write connection.
     * Usually this function is only required to set mockups for testing
     *
     * @param  PdoInterface $connection
     * @return self
     */
    public static function setWriteConnection(PdoInterface $connection)
    {
        self::$writeConnection = $connection;
    }

    /**
     * Create or return a connection for writing data
     *
     * @return PdoInterface
     * @throws PDOFailed
     */
    public static function getWriteConnection()
    {
        if (null === self::$writeConnection) {
            self::$writeConnection = self::createPdoConnection(self::$writeSourceName);
            self::$writeProfiler = new \Aura\Sql\Profiler\Profiler();
            self::$writeProfiler->setActive(self::$enableProfiling);
            self::$writeConnection->setProfiler(self::$writeProfiler);
            if (self::$useTransaction) {
                self::$writeConnection->exec('SET SESSION TRANSACTION ISOLATION LEVEL READ COMMITTED');
                self::$writeConnection->exec('SET SESSION innodb_lock_wait_timeout=5');
                self::$writeConnection->beginTransaction();
            }
            if (!self::$useQueryCache) {
                try {
                    self::$writeConnection->exec('SET SESSION query_cache_type = 0;');
                } catch (\Exception $exception) {
                    // ignore, query cache might be disabled
                }
            }
            if (self::$useProfiling) {
                self::$writeConnection->exec('SET profiling = 1;');
            }
            if (self::$connectionTimezoneDifference) {
                self::$writeConnection->exec("SET time_zone = '" . self::$connectionTimezoneDifference . "';");
            }
            if (self::$galeraConnection && self::$enableWsrepSyncWait) {
                self::$writeConnection->exec(
                    'SET SESSION wsrep_sync_wait = (
                        SELECT CAST(value AS UNSIGNED) FROM config WHERE name = "setting__wsrepsync"
                    );'
                );
            }
            // On writing, use the same host to avoid racing/transaction conditions
            self::$readConnection = self::$writeConnection;
        }
        return self::$writeConnection;
    }

    /**
     * Test if a write connection is established
     *
     */
    public static function hasWriteConnection(): bool
    {
        return (self::$writeConnection !== null);
    }

    /**
     * Close a connection for writing data
     *
     */
    public static function closeWriteConnection()
    {
        self::$writeConnection = null;
    }

    /**
     * Set query cache
     *
     * @param Bool $useQueryCache
     *
     */
    public static function setQueryCache(bool $useQueryCache = true)
    {
        static::$useQueryCache = $useQueryCache;
    }

    /**
     * Set profiling
     *
     * @param Bool $useProfiling
     *
     */
    public static function setProfiling(bool $useProfiling = true)
    {
        static::$useProfiling = $useProfiling;
    }

    /**
     * Set the connection time zone to avoid or change platform dependent time zone influences
     * @see tests/Zmsdb/fixtures/mysql_zmsbo.sql.gz:14
     */
    public static function setTimeZone(string $name, string $hourDifference): void
    {
        static::$connectionTimezone           = ' ' . $name;
        static::$connectionTimezoneDifference = $hourDifference;
    }

    /**
     * Set cluster wide causality checks, needed for critical reads across different nodes
     *
     * @param Bool $wsrepStatus Set to true for critical reads
     */
    public static function setCriticalReadSession(bool $wsrepStatus = true)
    {
        static::$enableWsrepSyncWait = $wsrepStatus;
        static::getWriteConnection();
    }

    /**
     * Set transaction
     *
     * @param Bool $useTransaction
     *
     */
    public static function setTransaction(bool $useTransaction = true)
    {
        static::$useTransaction = $useTransaction;
    }

    /**
     * Check active transaction
     *
     */
    public static function hasActiveTransaction()
    {
        return (static::$useTransaction && static::getWriteConnection()->inTransaction());
    }

    /**
     * Rollback transaction if started
     *
     */
    public static function writeRollback(): bool
    {
        if (self::$useTransaction && self::getWriteConnection()->inTransaction()) {
            return self::getWriteConnection()->rollBack();
        }
        return false;
    }

    /**
     * Commit transaction if started
     *
     */
    public static function writeCommit(): bool
    {
        if (null !== self::$writeConnection && self::hasActiveTransaction()) {
            $status = self::getWriteConnection()->commit();
            self::$writeConnection->beginTransaction();
            return $status;
        }
        return false;
    }
}
