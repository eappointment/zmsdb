<?php

namespace BO\Zmsdb;

use BO\Zmsentities\Exchange as ExchangeEntity;
use Exception;
use DateTimeInterface;

class ExchangeAvailabilityreview extends ExchangeSimpleQuery
{
    /**
     * @SuppressWarnings(Length)
     * @throws Exception
     */
    public function readEntity(int $subjectid, DatetimeInterface $now): ExchangeEntity
    {
        $entity = new ExchangeEntity();
        $entity['title'] = "Review Öffnungszeiten";
        $entity->addDictionaryEntry(
            'Organisationsname',
            'string',
            'Name der Organisation'
        );
        $entity->addDictionaryEntry(
            'Standortname',
            'string',
            'Name des Standorts inkl. Kürzel'
        );
        $entity->addDictionaryEntry(
            'StandortID',
            'string',
            'ID of a scope',
            'scope.id'
        );
        $entity->addDictionaryEntry(
            'Startdatum',
            'string',
            'Beginn der Gültigkeit der Öffnungszeit'
        );
        $entity->addDictionaryEntry(
            'Endedatum',
            'string',
            'Ende der Gültigkeit der Öffnungszeit'
        );
        $entity->addDictionaryEntry(
            'Anfang',
            'string',
            'Tageszeit zum Anfang der Öffnungszeit'
        );
        $entity->addDictionaryEntry(
            'Ende',
            'string',
            'Tageszeit zum Ende der Öffnungszeit'
        );
        $entity->addDictionaryEntry(
            'jedexteWoche',
            'string',
            'Öffnungszeit findet nur jede x. Woche im Monat statt'
        );
        $entity->addDictionaryEntry(
            'allexWochen',
            'string',
            'Öffnungszeit finden nur alle x Wochen statt, Referenz ist der Montag in der Woche vom Startdatum'
        );
        $entity->addDictionaryEntry(
            'montag',
            'number',
            'Ein Wert größer als 0 bedeutet, dass die Öffnungszeit an diesem Wochentag geöffnet hat'
        );
        $entity->addDictionaryEntry(
            'dienstag',
            'number',
            'Ein Wert größer als 0 bedeutet, dass die Öffnungszeit an diesem Wochentag geöffnet hat'
        );
        $entity->addDictionaryEntry(
            'mittwoch',
            'number',
            'Ein Wert größer als 0 bedeutet, dass die Öffnungszeit an diesem Wochentag geöffnet hat'
        );
        $entity->addDictionaryEntry(
            'donnerstag',
            'number',
            'Ein Wert größer als 0 bedeutet, dass die Öffnungszeit an diesem Wochentag geöffnet hat'
        );
        $entity->addDictionaryEntry(
            'freitag',
            'number',
            'Ein Wert größer als 0 bedeutet, dass die Öffnungszeit an diesem Wochentag geöffnet hat'
        );
        $entity->addDictionaryEntry(
            'samstag',
            'number',
            'Ein Wert größer als 0 bedeutet, dass die Öffnungszeit an diesem Wochentag geöffnet hat'
        );
        $entity->addDictionaryEntry(
            'sonntag',
            'number',
            'Ein Wert größer als 0 bedeutet, dass die Öffnungszeit an diesem Wochentag geöffnet hat'
        );
        $entity->addDictionaryEntry(
            'Timeslot',
            'number',
            'Anzahl der Minuten, die ein Zeitslot einnimmt'
        );
        $entity->addDictionaryEntry(
            'Arbpltz',
            'number',
            'Anzahl der Terminarbeitsplätze'
        );
        $entity->addDictionaryEntry(
            'minusCall',
            'number',
            'Anzahl der Terminarbeitsplätze, um die für Callcenter das Angebot reduziert wird'
        );
        $entity->addDictionaryEntry(
            'minusOnline',
            'number',
            'Anzahl der Terminarbeitsplätze, um die für die Internetbuchung das Angebot reduziert wird'
        );
        $entity->addDictionaryEntry(
            'mehrfach',
            'number',
            'Ein Wert größer als 0 bedeutet, dass für einen Termin mehr als ein Zeitslot verwendet werden darf'
        );
        $entity->addDictionaryEntry(
            'buchVon',
            'number',
            'Anzahl der Tage, die mindestens im voraus gebucht werden muss'
        );
        $entity->addDictionaryEntry(
            'buchBis',
            'string',
            'Anzahl der Tage, die man maximal im voraus buchen kann'
        );

        //$entity['visualization']['xlabel'] = ["StandortID"];
        //$entity['visualization']['ylabel'] = ["buchVon", "buchBis"];

        $raw = $this
            ->getReader()
            ->fetchAll(
                constant("\BO\Zmsdb\Query\ExchangeAvailabilityreview::QUERY_READ_REPORT"),
                [
                    'departmentId' => $subjectid,
                    'dateString' => $now->format('Y-m-d'),
                ]
            );
        foreach ($raw as $entry) {
            $entity->addDataSet(array_values($entry));
        }
        return $entity;
    }

    /**
     * @throws Exception
     */
    public function readSubjectList(): ExchangeEntity
    {
        $raw = $this->getReader()->fetchAll(Query\ExchangeAvailabilityreview::QUERY_SUBJECTS);
        $entity = new ExchangeEntity();
        $entity['title'] = "Auswahl Öffnungszeitenübersicht nach Behörde";
        $entity->addDictionaryEntry('subject', 'string', 'Behörden ID', 'department.id');
        $entity->addDictionaryEntry('description', 'string', 'Beschreibung der Behörde');
        foreach ($raw as $entry) {
            $entity->addDataSet(array_values($entry));
        }
        return $entity;
    }

    /**
     * @SuppressWarnings(Param)
     */
    public function readPeriodList(): ExchangeEntity
    {
        $entity = new ExchangeEntity();
        $entity['title'] = "Öffnungszeitenübersicht nach Behörde ";
        $entity->addDictionaryEntry('subject', 'string', 'Behörden ID', 'department.id');
        $entity->addDataSet(["_"]);
        return $entity;
    }
}
