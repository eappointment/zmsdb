<?php

namespace BO\Zmsdb;

use BO\Zmsentities\Calendar as Entity;
use BO\Zmsentities\Cluster as ClusterEntity;
use BO\Zmsentities\Provider as ProviderEntity;
use BO\Zmsentities\Collection\AvailabilityList;
use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Collection\ScopeList;
use DateTimeImmutable;

/**
 * @SuppressWarnings(Coupling)
 *
 */
class Calendar extends Base
{
    public function readResolvedEntity(
        Entity $calendar,
        \DateTimeInterface $now,
        $resolveOnlyScopes = false,
        $slotType = 'public',
        $slotsRequired = 0,
        bool $untilBookableEnd = false
    ) {
        $calendar['freeProcesses'] = new ProcessList();
        $calendar['scopes'] = $calendar->getScopeList();
        $calendar = $this->readResolvedScopes($calendar);
        $calendar = $this->readResolvedProviders($calendar);
        $calendar = $this->readResolvedClusters($calendar);
        $calendar = $this->readResolvedRequests($calendar);
        $calendar = $this->readResolvedScopeReferences($calendar);
        if (count($calendar->scopes) < 1) {
            throw new Exception\CalendarWithoutScopes("No scopes resolved in $calendar");
        }
        $calendar = $this->readResolvedDays(
            $calendar,
            $resolveOnlyScopes,
            $now,
            $slotType,
            $slotsRequired,
            $untilBookableEnd
        );
        return $calendar;
    }

    /**
     * Resolve calendar scopes
     *
     */
    protected function readResolvedScopes(Entity $calendar)
    {
        $scopeList = new ScopeList();
        $scopeReader = new Scope();
        if (isset($calendar['scopes'])) {
            foreach ($calendar['scopes'] as $scope) {
                $scope = $scopeReader->readEntity($scope['id'], 1);
                $scopeList->addEntity($scope);
            }
        }
        $calendar['scopes'] = $scopeList->withUniqueScopes();
        return $calendar;
    }

    /**
     * Resolve Reference dayoff for departments, but do not resolve circular scope->department->scopes
     *
     */
    protected function readResolvedScopeReferences(Entity $calendar)
    {
        foreach ($calendar->scopes as $scope) {
            $scope['dayoff'] = (new DayOff())->readByScopeId($scope['id']);
        }
        return $calendar;
    }

    protected function readResolvedRequests(Entity $calendar)
    {
        $requestReader = new Request();
        $requestRelationQuery = new RequestRelation();
        if (isset($calendar['requests'])) {
            foreach ($calendar['requests'] as $key => $request) {
                $request = new \BO\Zmsentities\Request($request);
                $request = $requestReader->readEntity($request->getSource(), $request->getId());
                $calendar['requests'][$key] = $request;
                $requestRelationList = $requestRelationQuery
                    ->readListByRequestId($request->getId(), $request->getSource());
                foreach ($requestRelationList as $requestRelationItem) {
                    // we do not check multipleSlotsEnabled here, because the availability might need this information
                    // so we calculate slots as if multipleSlotsEnabled is true
                    $calendar->scopes->addRequiredSlots(
                        $requestRelationItem->getSource(),
                        $requestRelationItem->getProvider()->getId(),
                        $requestRelationItem->getSlotCount()
                    );
                }
            }
        }
        return $calendar;
    }

    protected function readResolvedClusters(Entity $calendar)
    {
        $scopeReader = new Scope();
        if (isset($calendar['clusters'])) {
            foreach ($calendar['clusters'] as $cluster) {
                $cluster = new ClusterEntity($cluster);
                $scopeList = $scopeReader->readByClusterId($cluster->getId(), 1);
                $calendar['scopes'] = $calendar['scopes']->addList($scopeList)->withUniqueScopes();
            }
        }
        return $calendar;
    }

    protected function readResolvedProviders(Entity $calendar)
    {
        $scopeReader = new Scope();
        $providerReader = new Provider();
        if (isset($calendar['providers'])) {
            foreach ($calendar['providers'] as $key => $provider) {
                $provider = new ProviderEntity($provider);
                $calendar['providers'][$key] = $providerReader->readEntity($provider->getSource(), $provider->getId());
                $scopeList = $scopeReader->readByProviderId($provider->getId(), 1);
                $calendar['scopes'] = $calendar['scopes']->addList($scopeList)->withUniqueScopes();
            }
        }


        return $calendar;
    }

    protected function readResolvedDays(
        Entity $calendar,
        $resolveOnlyScopes,
        \DateTimeInterface $now,
        $slotType,
        $slotsRequiredForce,
        bool $untilBookableEnd
    ) {
        if (!$resolveOnlyScopes) {
            $bookableEnd = $this->readBookableEndFromAvailability($calendar, $now);
            $calendar->bookableEnd = $bookableEnd->getTimestamp();
            if ($untilBookableEnd) {
                $calendar->setLastDayTime($bookableEnd);
            }
            $dayQuery = new Day();
            $dayList = $dayQuery->readByCalendar($calendar, $slotsRequiredForce);
            unset($dayQuery); // drop temporary scope list
            $calendar->days = $dayList->setStatusByType($slotType, $now);
        }
        return $calendar;
    }

    /**
     * @param Entity             $calendar
     * @param DateTimeImmutable $now
     *
     * @return DateTimeImmutable|null
     */
    protected function readBookableEndFromAvailability(Entity $calendar, DateTimeImmutable $now)
    {
        $availabilityRepo = new Availability();
        $scopeIds = $calendar->getScopeList()->getIds();
        $bookableEndByScope = $calendar->getScopeList()->getGreatestBookableEnd($now);
        $result = $availabilityRepo->readGreatestBookableEnd($now, $scopeIds);
        $maxBookableEnd = (is_array($result) && isset($result['maxEndInDays'])) ?
            DateTimeImmutable::createFromFormat('Y-m-d', $result['maxEndInDays']) :
            false;
        if ($maxBookableEnd === false) {
            $maxBookableEnd = $now->modify('+'. $bookableEndByScope . ' days');
        }
        return $maxBookableEnd->setTime(0, 0, 0);
    }
}
