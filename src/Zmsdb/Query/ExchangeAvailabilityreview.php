<?php

namespace BO\Zmsdb\Query;

class ExchangeAvailabilityreview extends Base
{
    const QUERY_READ_REPORT = '
        SELECT
            organisation.Organisationsname,
            CONCAT(standort.Bezeichnung, " ", standort.standortkuerzel) Standortname,
            standort.StandortID,
            oeffnungszeit.Startdatum,
            oeffnungszeit.Endedatum,
            oeffnungszeit.Terminanfangszeit Anfang,
            oeffnungszeit.Terminendzeit Ende,
            oeffnungszeit.jedexteWoche,
            oeffnungszeit.allexWochen,
            Wochentag & 2 montag,
            Wochentag & 4 dienstag,
            Wochentag & 8 mittwoch,
            Wochentag & 16 donnerstag,
            Wochentag & 32 freitag,
            Wochentag & 64 samstag,
            Wochentag & 1 sonntag,
            oeffnungszeit.Timeslot,
            oeffnungszeit.Anzahlterminarbeitsplaetze Arbpltz,
            oeffnungszeit.reduktionTermineCallcenter minusCall,
            oeffnungszeit.reduktionTermineImInternet minusOnline,
            oeffnungszeit.erlaubemehrfachslots mehrfach,
            IF(oeffnungszeit.`Offen_ab`,
            oeffnungszeit.`Offen_ab`,
            standort.`Termine_ab`) buchVon,
            IF(oeffnungszeit.`Offen_bis`,
            oeffnungszeit.`Offen_bis`,
            standort.`Termine_bis`) buchBis
        FROM
            oeffnungszeit
        LEFT JOIN
            standort ON oeffnungszeit.StandortID = standort.StandortID
        LEFT JOIN
            behoerde ON standort.BehoerdenID = behoerde.BehoerdenID
        LEFT JOIN
            organisation ON behoerde.OrganisationsID = organisation.OrganisationsID
        WHERE
            oeffnungszeit.Endedatum >= :dateString AND behoerde.BehoerdenID = :departmentId
        ORDER BY organisation.Organisationsname,
            Standortname,
            oeffnungszeit.Wochentag,
            oeffnungszeit.Startdatum,
            oeffnungszeit.Terminanfangszeit;
        ';

    const QUERY_SUBJECTS = '
      SELECT
          d.`BehoerdenID` as subject,
          d.`Name` AS description
      FROM '. Department::TABLE .' AS d
      ORDER BY d.`BehoerdenID`
    ';
}
