<?php

namespace BO\Zmsdb\Query;

class Mimepart extends Base
{
    /**
     * @var String TABLE mysql table reference
     */
    public const TABLE = 'mailpart';

    /**
     * No resolving required here
     */
    protected $resolveLevel = 0;

    public function getEntityMapping(): array
    {
        return [
            'mime' => 'mimepart.mime',
            'content' => 'mimepart.content',
            'base64' => 'mimepart.base64',
            'attachmentName' => 'mimepart.attachmentName',
        ];
    }

    public function addConditionQueueId($queueId): static
    {
        $this->query->where('mimepart.queueId', '=', $queueId);
        return $this;
    }

    /**
     * @param array $data
     * @return array
     */
    public function postProcess($data): array
    {
        $data['attachmentName'] = isset($data['attachmentName']) ? (string) $data['attachmentName'] : '';
        return $data;
    }
}
