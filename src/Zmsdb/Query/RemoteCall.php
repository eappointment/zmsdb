<?php

namespace BO\Zmsdb\Query;

use BO\Zmsdb\Connection\Select;
use BO\Zmsentities\Permission as PermissionEntity;
use DateTime;
use Exception;

/**
 * @SuppressWarnings(Public)
 */
class RemoteCall extends Base implements MappingInterface
{
    /**
     * @var String TABLE mysql table reference
     */
    const TABLE = 'remotecall';

    const ALIAS = 'remotecall';

    public const PRIMARY = 'id';

    public function getEntityMapping(): array
    {
        return [
            'id' => self::TABLE .'.id',
            'processArchiveId' => self::TABLE .'.processArchiveId',
            'name' => self::TABLE .'.name',
            'url' => self::TABLE .'.url',
            'call__count' => self::TABLE .'.call__count',
            'call__statuscode' => self::TABLE .'.call__statuscode',
            'call__requested' => self::TABLE .'.call__requested',
            'call__timestamp' => self::TABLE .'.call__timestamp',
        ];
    }

    public function addConditionPrimary($primary): RemoteCall
    {
        $this->query->where('remotecall.id', '=', $primary);
        return $this;
    }

    public function addConditionProcessArchiveId($processArchiveId): RemoteCall
    {
        $this->query->where('remotecall.processArchiveId', '=', $processArchiveId);
        return $this;
    }
}
