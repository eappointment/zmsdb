<?php

namespace BO\Zmsdb\Query;

use DateTimeImmutable;

/**
 * @SuppressWarnings(Public)
 */
class Apikey extends Base implements MappingInterface
{
    /**
     * @var String TABLE mysql table reference
     */
    const TABLE = 'apikey';
    const APICLIENT_ALIAS = 'apiclient';

    protected function addRequiredJoins()
    {
        $this->leftJoin(
            new Alias('apiclient', self::APICLIENT_ALIAS),
            self::TABLE .'.apiClientID',
            '=',
            self::APICLIENT_ALIAS .'.apiClientID'
        );
    }


    public function getEntityMapping()
    {
        return [
            'key' => self::TABLE .'.key',
            'createIP' => self::TABLE .'.createIP',
            'apiclient__apiClientID' => self::APICLIENT_ALIAS .'.apiClientID',
            'apiclient__clientKey' => self::APICLIENT_ALIAS .'.clientKey',
            'apiclient__accesslevel' => self::APICLIENT_ALIAS .'.accesslevel',
            'apiclient__shortname' => self::APICLIENT_ALIAS .'.shortname',
            'apiclient__lastChange' =>
                self::expression(
                    'UNIX_TIMESTAMP(`'. self::APICLIENT_ALIAS .'`.`updateTimestamp`)'
                ),
            'apiclient__permission__remotecall' =>  self::APICLIENT_ALIAS .'.permission__remotecall',
            'apiclient__secondsToInvalidation' => self::APICLIENT_ALIAS .'.secondsToInvalidation',
            'ts' => self::TABLE .'.ts',
            'locked' => self::TABLE .'.locked',
        ];
    }

    public function addConditionApikey($apikey): Apikey
    {
        $this->query->where(self::TABLE .'.key', '=', $apikey);
        return $this;
    }

    public function addConditionClientId($clientId): Apikey
    {
        $this->query->where(self::TABLE .'.apiClientID', '=', $clientId);
        return $this;
    }

    public function addExpirationCondition(\DateTimeInterface $now): EventLog
    {
        $this->query->where(function (ConditionBuilder $query) use ($now) {
            $query->andWith(
                self::ALIAS . '.ts' + $now->getTimestamp(),
                '<',
                $now->getTimestamp()
            );
        });
        $this->query->orderBy('appointments__0__date', 'ASC');
        return $this;
    }

    /**
     * @throws \Exception
     */
    public function postProcess($data)
    {
        $data["apiclient__lastChange"] =
            (new DateTimeImmutable())
                ->setTimestamp($data["apiclient__lastChange"])
                ->getTimestamp();
        $data['ts'] =
            (new DateTimeImmutable())
                ->setTimestamp($data['ts'])
                ->getTimestamp();
        return $data;
    }
}
