<?php

namespace BO\Zmsdb\Query;

use BO\Zmsentities\Process as ProcessEntity;

/**
 *
 * @SuppressWarnings(Methods)
 * @SuppressWarnings(Complexity)
 */
class ProcessArchive extends Base implements MappingInterface
{

    /**
     *
     * @var String TABLE mysql table reference
     */
    const TABLE = 'process_archive';
    const ALIAS = 'process_archive';

    const QUERY_UPDATE_STATUS = "UPDATE `process_archive` pa
        SET pa.status = :newStatus
        WHERE pa.status = :oldStatus
        LIMIT 10000
    ";

    public const PRIMARY = 'id';

    protected $resolveLevel = 0;

    public function getEntityMapping(): array
    {
        return [
            'id' => self::ALIAS .'.'.  static::PRIMARY,
            'processId' => self::ALIAS .'.processId',
            'archiveId' => self::ALIAS .'.archiveId',
            'createIP' => self::ALIAS .'.createIP',
            'createTimestamp' => self::ALIAS .'.createTimestamp',
            'lastChange' => self::expression("UNIX_TIMESTAMP(". self::ALIAS .".lastChange)"),
            'status' => self::ALIAS .'.status',
        ];
    }

    public function addConditionPrimary($primary): self
    {
        $this->query->where(self::ALIAS .'.'.  static::PRIMARY, '=', $primary);
        return $this;
    }

    public function addConditionStatus($status): self
    {
        $this->query->where(self::ALIAS .'.status', '=', $status);
        return $this;
    }

    public function addConditionSkipArchived(): self
    {
        $this->query->where(self::ALIAS .'.status', '!=', ProcessEntity::STATUS_ARCHIVED);
        return $this;
    }
}
