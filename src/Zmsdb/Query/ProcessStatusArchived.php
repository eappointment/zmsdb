<?php

namespace BO\Zmsdb\Query;

/**
*
* @SuppressWarnings(TooManyPublicMethods)
* @SuppressWarnings(Complexity)
 */
class ProcessStatusArchived extends Base implements MappingInterface
{

    /**
     *
     * @var String TABLE mysql table reference
     */
    const TABLE = 'buergerarchiv';
    const STATISTIC_TABLE = 'statistik';
    const ALIAS = 'processStatusArchiv';

    const QUERY_INSERT_IN_STATISTIC = '
        INSERT INTO '. self::STATISTIC_TABLE .' SET
            lastbuergerarchivid = :archiveId,
            termin = :withAppointment,
            datum = :date,
            anliegenid = :requestId,
            info_dl_id = :providerId,
            standortid = :scopeId,
            clusterid = :clusterId,
            behoerdenid = :departmentId,
            organisationsid = :organisationId,
            kundenid = :ownerId
    ';

    public function addJoin()
    {
        return [
            $this->addJoinScope(),
        ];
    }

    public function getEntityMapping()
    {
        return [
            'archiveId' => self::ALIAS . '.BuergerarchivID',
            'status' => self::expression('"archived"'),
            'appointments__0__date' => self::expression(
                'CONCAT(`'. self::ALIAS .'`.`Datum`, " ", `'. self::ALIAS .'`.`Timestamp`)'
            ),
            'appointments__0__scope__id' => self::ALIAS . '.StandortID',
            'scope__id' => self::ALIAS . '.StandortID',
            '__clientsCount' => self::ALIAS . '.AnzahlPersonen',
            'waitingTime' => self::ALIAS . '.wartezeit',
            'queue__arrivalTime' => self::expression(
                'CONCAT(`'. self::ALIAS .'`.`Datum`, " 00:00:00")'
            ),
            'queue__callTime' => self::expression(
                'CONCAT(`'. self::ALIAS .'`.`Datum`, " ", SEC_TO_TIME(`wartezeit`))'
            ),
            'queue__withAppointment' => self::ALIAS . '.mitTermin',
            'queue__status' => self::expression(
                'IF(`'. self::ALIAS .'`.`nicht_erschienen`,
                    "missed",
                    "finished"
                )'
            ),
        ];
    }

    public function addConditionArchiveId($archiveId)
    {
        $this->query->where(self::ALIAS . '.BuergerarchivID', '=', $archiveId);
        return $this;
    }

    public function addConditionScopeId($scopeId)
    {
        $this->query->where(self::ALIAS . '.StandortID', '=', $scopeId);
        return $this;
    }

    public function addConditionTime(\DateTimeInterface $now)
    {
        $this->query->where(self::ALIAS . '.Datum', '=', $now->format('Y-m-d'));
        return $this;
    }

    public function addConditionIsMissed($missed)
    {
        $this->query->where(self::ALIAS . '.nicht_erschienen', '=', $missed);
        return $this;
    }

    public function addConditionWithAppointment($withAppointment)
    {
        $this->query->where(self::ALIAS . '.mitTermin', '=', $withAppointment);
        return $this;
    }

    public function addJoinStatisticFailed($dateTime, \BO\Zmsentities\Scope $scope)
    {
        //use existing index with StandortID and Datum
        $this->leftJoin(
            new Alias(self::STATISTIC_TABLE, 'statistic'),
            self::expression('
                statistic.StandortID = '. self::ALIAS .'.StandortID
                AND statistic.Datum = '. self::ALIAS .'.Datum
                AND `statistic`.`lastbuergerarchivid` = '. self::ALIAS .'.BuergerarchivID
            ')
        );
        $this->query->where(function (\Solution10\SQL\ConditionBuilder $query) use ($dateTime, $scope) {
            $query->andWith(
                self::expression('statistic.lastbuergerarchivid IS NULL AND '. self::ALIAS .'.Datum'),
                '>',
                $dateTime->format('Y-m-d')
            );
            $query->andWith(self::ALIAS . '.nicht_erschienen', '=', 0);
            $query->andWith(self::ALIAS . '.StandortID', '=', $scope->id);
        });
        return $this;
    }

    public function addValuesNewArchive(\BO\Zmsentities\Process $process, \DateTimeInterface $now)
    {
        $this->addValues([
            'StandortID' => $process->scope['id'],
            'Datum' => $process->getFirstAppointment()->toDateTime()->format('Y-m-d'),
            'mitTermin' => ($process->toQueue($now)->withAppointment) ? 1 : 0,
            'nicht_erschienen' => ('missed' == $process->queue['status']) ? 1 : 0,
            'Timestamp' => $now->format('H:i:s'),
            'wartezeit' => ($process->getWaitedSeconds() > 0) ? $process->getWaitedMinutes() : 0,
            'AnzahlPersonen' => $process->getClients()->count()
        ]);
    }

    public function postProcess($data)
    {
        $data[$this->getPrefixed("appointments__0__date")] =
            strtotime($data[$this->getPrefixed("appointments__0__date")]);
        $data[$this->getPrefixed("queue__callTime")] =
            strtotime($data[$this->getPrefixed("queue__callTime")]);
        $data[$this->getPrefixed("queue__arrivalTime")] =
            strtotime($data[$this->getPrefixed("queue__arrivalTime")]);
        if (isset($data[$this->getPrefixed('__clientsCount')])) {
            $clientsCount = $data[$this->getPrefixed('__clientsCount')];
            unset($data[$this->getPrefixed('__clientsCount')]);
            while ($clientsCount-- > 0) {
                $data[$this->getPrefixed('clients__'.$clientsCount.'__familyName')] = 'Unknown';
            }
        }
        return $data;
    }

    /**
     * Add Scope to the dataset
     */
    protected function addJoinScope(): Scope
    {
        $this->leftJoin(
            new Alias(Scope::TABLE, 'scope'),
            self::ALIAS . '.StandortID',
            '=',
            'scope.StandortID'
        );
        return new Scope($this, $this->getPrefixed('scope__'));
    }
}
