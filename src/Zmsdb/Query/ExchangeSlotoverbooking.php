<?php

namespace BO\Zmsdb\Query;

class ExchangeSlotoverbooking extends Base
{
    /**
     * @var String TABLE mysql table reference
     */
    const TABLE = 'slot_process';

    const QUERY_READ_REPORT = '
        SELECT 
            `scopeID` as StandortID, 
            CONCAT(year, "-", LPAD(month, 2, 0), "-", LPAD(day, 2, 0)) as Datum, 
            SUM(slotcount), 
            SUM(intern), 
            ROUND((SUM(slotcount)/SUM(intern)) * 100,0) as ProzentAuslastung 
        FROM ( 
            SELECT s.*, COUNT(sp.slotID) as slotcount 
            FROM slot AS s 
                LEFT JOIN slot_process as sp USING(slotID) 
            WHERE status IN ("free","full") 
            GROUP BY s.slotID ) AS innerquery 
        GROUP BY scopeID, Datum
        HAVING ProzentAuslastung > 100 
        ORDER BY scopeID, ProzentAuslastung DESC
    ';
}
