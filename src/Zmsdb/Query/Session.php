<?php

namespace BO\Zmsdb\Query;

class Session extends Base
{
    /**
     * @var String TABLE mysql table reference
     */
    const TABLE = 'sessiondata';

    /**
     * No resolving required here
     */
    protected $resolveLevel = 0;

    const QUERY_WRITE = '
        REPLACE INTO
            sessiondata
        SET
            sessionid=?,
            sessionname=?,
            sessioncontent=?
    ';

    const QUERY_DELETE = '
        DELETE FROM
            sessiondata
        WHERE
            sessionid=? AND
            sessionname=?
    ';

    public function getEntityMapping($type)
    {
        $mapping = [
            'id' => 'session.sessionid',
            'name' => 'session.sessionname',
            'content' => 'session.sessioncontent',
            'ts',
        ];

        if ($type === 'reduced') {
            unset($mapping['content']);
        }

        return $mapping;
    }

    public function addConditionSessionId($sessionId)
    {
        $this->query->where('session.sessionid', '=', $sessionId);
        return $this;
    }

    public function addConditionSessionName(string $sessionName)
    {
        if (empty($sessionName)) {
            throw new \InvalidArgumentException('The \'sessionName\' must not be empty.');
        }

        $this->query->where('session.sessionname', '=', $sessionName);
        return $this;
    }

    public function addConditionTsAgeComparison(int $ageInSeconds, string $comparison): Session
    {
        if (!in_array($comparison, [self::IS_EQ, self::IS_LT, self::IS_GT, self::IS_GTE, self::IS_LTE])) {
            throw new \InvalidArgumentException("The comparison '$comparison' is not implemented/allowed");
        }

        $this->query->where(
            self::expression(
                'UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(`session`.`ts`)'
            ),
            $comparison,
            $ageInSeconds
        );

        return $this;
    }

    /**
     * postProcess data if necessary
     *
     */
    public function postProcess($data)
    {
        if (isset($data[$this->getPrefixed('content')])) {
            $data[$this->getPrefixed('content')] = json_decode($data[$this->getPrefixed('content')], 1);
        }

        return $data;
    }
}
