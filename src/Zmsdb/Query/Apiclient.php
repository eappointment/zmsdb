<?php

namespace BO\Zmsdb\Query;

use BO\Zmsdb\Connection\Select;
use BO\Zmsentities\Permission as PermissionEntity;
use DateTime;
use Exception;

/**
 * @SuppressWarnings(Public)
 */
class Apiclient extends Base implements MappingInterface
{
    /**
     * @var String TABLE mysql table reference
     */
    const TABLE = 'apiclient';

    const ALIAS = 'apiclient';

    public function getEntityMapping(): array
    {
        return [
            'apiClientID' => self::TABLE .'.apiClientID',
            'clientKey' => self::TABLE .'.clientKey',
            'shortname' => self::TABLE .'.shortname',
            'accesslevel' => self::TABLE .'.accesslevel',
            'lastChange' => self::TABLE .'.updateTimestamp',
            'permission__remotecall' => self::TABLE .'.permission__remotecall',
            'secondsToInvalidation' => self::TABLE .'.secondsToInvalidation',
        ];
    }

    public function addConditionApiclientKey($clientKey): Apiclient
    {
        $this->query->where('apiclient.clientKey', '=', $clientKey);
        return $this;
    }

    public function addConditionShortname(string $userLoginName): Apiclient
    {
        $this->query->where('apiclient.shortname', '=', $userLoginName);
        return $this;
    }

    public function addSecondsToInvalidation(): Apiclient
    {
        $this->query->where('apiclient.secondsToInvalidation', '>', 0);
        return $this;
    }

    /**
     * @throws Exception
     */
    public function postProcess($data): array
    {
        $data[$this->getPrefixed("lastChange")] =
            (new DateTime($data[$this->getPrefixed("lastChange")] . Select::$connectionTimezone))
            ->getTimestamp();
        return $data;
    }
}
