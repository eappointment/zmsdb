<?php

declare(strict_types=1);

namespace BO\Zmsdb\Query;

use BO\Zmsentities\CheckInTask as TaskEntity;
use BO\Zmsentities\Helper\DateTime;

class CheckInTask extends Base
{
    public const TABLE = 'checkintask';
    public const ALIAS = 'checkInTask';
    public const PRIMARY = 'id';
    public const OUTDATED_AFTER = '-30 months';

    protected $resolveLevel = 0;

    public function getEntityMapping(): array
    {
        return [
            'id' => self::ALIAS . '.id',
            'status' => self::ALIAS . '.status',
            'refersTo' => self::ALIAS . '.refersTo',
            'referenceId' => self::ALIAS . '.referenceId',
            'scopeId' => self::ALIAS . '.scopeId',
            'checkInTS' => self::ALIAS . '.checkInTS',
            'scheduledTS' => self::ALIAS . '.scheduledTS',
            'checkInSource' => self::ALIAS . '.checkInSource',
            'lastChange' => self::ALIAS . '.lastChange',
        ];
    }

    public function reverseEntityMapping(TaskEntity $checkInTask): array
    {
        $map = (array) $checkInTask;
        unset($map['$schema']);
        unset($map['checkInDateTime']);
        unset($map['scheduledDateTime']);
        unset($map['lastChange']);

        return $map;
    }

    /**
     * @param array $data
     * @return array
     */
    public function postProcess($data): array
    {
        $data['id'] = (int) $data['id'];
        $data['referenceId'] = (int) $data['referenceId'];
        $data['scopeId'] = (int) $data['scopeId'];
        $data['checkInTS'] = (int) $data['checkInTS'];
        $data['scheduledTS'] = (int) $data['scheduledTS'];

        return $data;
    }

    /**
     * @param int[] $referenceIds
     * @return $this
     */
    public function addReferenceIdComparison(array $referenceIds): static
    {
        $this->query->where(self::ALIAS . '.referenceId', 'IN', $referenceIds);

        return $this;
    }

    /**
     * @param int[] $scopeIds
     * @return $this
     */
    public function addScopeIdComparison(array $scopeIds): static
    {
        $this->query->orWhere(self::ALIAS . '.scopeId', 'IN', $scopeIds);

        return $this;
    }

    /**
     * @param string[] $statuses
     * @return $this
     */
    public function addStatusComparison(array $statuses): static
    {
        $this->query->where(self::ALIAS . '.status', 'IN', $statuses);

        return $this;
    }

    public function addCheckInTSLimits(int $fromTS, ?int $toTS = null): static
    {
        if ($fromTS) {
            $this->query->where(self::ALIAS . '.checkInTS', '>=', $fromTS);
        }
        if ($toTS !== null) {
            $this->query->where(self::ALIAS . '.checkInTS', '<=', $toTS);
        }

        return $this;
    }

    public function addScheduledTSLimits(int $fromTS, ?int $toTS = null): static
    {
        if ($fromTS) {
            $this->query->where(self::ALIAS . '.scheduledTS', '>=', $fromTS);
        }
        if ($toTS !== null) {
            $this->query->where(self::ALIAS . '.scheduledTS', '<=', $toTS);
        }

        return $this;
    }

    public function addOutdatedCondition(
        \DateTimeInterface $now,
        $outdatedAfter = self::OUTDATED_AFTER
    ): static {
        $now = DateTime::create($now);
        $this->query->where(self::ALIAS . '.scheduledTS', '<', (int) $now->modify($outdatedAfter)->format('U'));

        return $this;
    }
}
