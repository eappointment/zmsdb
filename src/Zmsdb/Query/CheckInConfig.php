<?php

declare(strict_types=1);

namespace BO\Zmsdb\Query;

use BO\Zmsentities\CheckInConfig as ConfigEntity;
use BO\Zmsentities\Helper\DateTime;
use Solution10\SQL\Expression;

class CheckInConfig extends Base
{
    public const TABLE = 'checkinconfig';
    public const ALIAS = 'checkInConfig';
    public const PRIMARY = 'id';
    public const OUTDATED_AFTER = '-14 days';

    protected $resolveLevel = 0;

    public function getEntityMapping(): array
    {
        return [
            'id' => self::ALIAS . '.id',
            'status' => self::ALIAS . '.status',
            'scopeIds' => self::ALIAS . '.scopeIds',
            'startBeforeAvailability' => self::ALIAS . '.startBeforeAvailability',
            'startBeforeMinutes' => self::ALIAS . '.startBeforeMinutes',
            'tooEarlyMinutes' => self::ALIAS . '.tooEarlyMinutes',
            'toleranceMinutes' => self::ALIAS . '.toleranceMinutes',
            'activeAfterClosing' => self::ALIAS . '.activeAfterClosing',
            'languages' => self::ALIAS . '.languages',
            'msgAlternative' => self::ALIAS . '.msgAlternative',
            'msgLate' => self::ALIAS . '.msgLate',
            'msgEarly' => self::ALIAS . '.msgEarly',
            'msgWrongDay' => self::ALIAS . '.msgWrongDay',
            'msgWrongCounter' => self::ALIAS . '.msgWrongCounter',
            'msgSuccess' => self::ALIAS . '.msgSuccess',
            'msgMaintenance' => self::ALIAS . '.msgMaintenance',
            'lastChange' => self::ALIAS . '.lastChange',
        ];
    }

    public function reverseEntityMapping(ConfigEntity $checkInConfig): array
    {
        $map = (array) $checkInConfig;
        $map['startBeforeAvailability'] = (int) $map['startBeforeAvailability'];
        $map['activeAfterClosing'] = (int) $map['activeAfterClosing'];
        unset($map['$schema']);
        unset($map['lastChange']);

        return $map;
    }

    /**
     * @param array $data
     * @return array
     */
    public function postProcess($data): array
    {
        $data['startBeforeAvailability'] = (bool) $data['startBeforeAvailability'];
        $data['startBeforeMinutes'] = (int) $data['startBeforeMinutes'];
        $data['tooEarlyMinutes'] = (int) $data['tooEarlyMinutes'];
        $data['toleranceMinutes'] = (int) $data['toleranceMinutes'];
        $data['activeAfterClosing'] = (bool) $data['activeAfterClosing'];

        return $data;
    }

    public function addScopeIdComparison(int $scopeId): static
    {
        $this->query->where(self::ALIAS . '.scopeIds', 'LIKE', '%:' . $scopeId . ':%');

        return $this;
    }

    public function addConditionDepartmentId($departmentId): static
    {
        $this->innerJoin(
            new Alias('standort', 'scope'),
            self::expression(
                'scope.BehoerdenID = ' . $departmentId . ' AND '
                . 'checkInConfig.scopeIds LIKE CONCAT("%:", scope.StandortID, ":%")'
            )
        );
//        $this->query->where('scope.BehoerdenID', '=', $departmentId);

        return $this;
    }

    public function addOutdatedCondition(
        \DateTimeInterface $now,
        $outdatedAfter = self::OUTDATED_AFTER
    ): static {
        $now = DateTime::create($now);
        $this->query->where(self::ALIAS . '.lastChange', '<', $now->modify($outdatedAfter)->format('Y-m-d'));

        return $this;
    }
}
