<?php

namespace BO\Zmsdb\Query;

use BO\Zmsdb\Connection\Select;

class MailQueue extends Base
{
    /**
     * @var String TABLE mysql table reference
     */
    const TABLE = 'mailqueue';
    public const PRIMARY = 'id';

    const QUERY_DELETE = '
        DELETE mq,  mp
            FROM '. self::TABLE .' mq
            LEFT JOIN '. Mimepart::TABLE .' mp ON mp.queueId = mq.id
            WHERE mq.id=?
    ';

    const QUERY_POLLING_GROUPED_EMAILS = '
        SELECT 
            count(b.BuergerID) as anzahl, 
            b.Email, 
            s.StandortID, 
            s.Bezeichnung 
        FROM buerger b 
            LEFT JOIN standort s 
                USING(StandortID) 
        WHERE 
            b.EMail != ""  AND s.Bezeichnung NOT LIKE "Kfz%"
        GROUP BY 
            b.EMail,
            b.StandortID 
        HAVING anzahl > 2 
        ORDER BY anzahl DESC
    ';

    const QUERY_POLLING_GROUPED_EMAILS_KFZ = '
        SELECT 
            count(b.BuergerID) as anzahl, 
            b.Email, 
            s.StandortID, 
            s.Bezeichnung 
        FROM buerger b 
            LEFT JOIN standort s 
                USING(StandortID) 
        WHERE 
            b.EMail != "" AND s.Bezeichnung LIKE "Kfz%"
        GROUP BY 
            b.EMail,
            b.StandortID 
        HAVING anzahl > 2 
        ORDER BY anzahl DESC
    ';

    public function getEntityMapping()
    {
        return [
            'id' => 'mailQueue.id',
            'createIP' => 'mailQueue.createIP',
            'createTimestamp' => 'mailQueue.createTimestamp',
            'subject' => 'mailQueue.subject',
            'client__email' => 'mailQueue.clientEmail',
            'client__familyName' => 'mailQueue.clientFamilyName',
            'process__id' => 'mailQueue.processID',
            'department__id' => 'mailQueue.departmentID'
        ];
    }

    public function addConditionItemId($itemId)
    {
        $this->query->where('mailQueue.id', '=', $itemId);
        return $this;
    }

    public function addOrderBy($parameter, $order = 'ASC')
    {
        $this->query->orderBy('mailQueue.'. $parameter, $order);
        return $this;
    }

    /**
     * @param array $data
     * @return array
     */
    public function postProcess($data): array
    {
        $data[static::PRIMARY] = (int) $data[static::PRIMARY];
        $data['createTimestamp'] = (int) ($data['createTimestamp']);
        return $data;
    }
}
