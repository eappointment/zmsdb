<?php

namespace BO\Zmsdb\Query;

class Config extends Base
{
    /**
     * @var String TABLE mysql table reference
     */
    const TABLE = 'config';

    const QUERY_SELECT = '
        SELECT * FROM config
    ';

    const QUERY_SELECT_PROPERTY =
            'SELECT
                value
            FROM config
            WHERE name = ?
            ';

    const QUERY_SELECT_PROPERTY_GROUP = '
        SELECT *
        FROM `config`
        WHERE
            `name` LIKE :groupName
    ';

    const QUERY_REPLACE_PROPERTY =
        'REPLACE INTO config
            SET name = :property, 
                changeTimestamp = :changeTimestamp,
                description = :description,
                value = :value
            ';

    const QUERY_READ_DESCRIPTIONS = '
        SELECT `name`, `description` FROM config;
    ';

    public function addConditionName($itemName)
    {
        $this->query->where(self::TABLE .'.name', '=', $itemName);
        return $this;
    }
}
