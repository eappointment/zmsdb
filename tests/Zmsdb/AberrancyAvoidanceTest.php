<?php

declare(strict_types=1);

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\Connection\Select;

class AberrancyAvoidanceTest extends Base
{
    public function testConnectionDetails(): void
    {
        $result = Select::getWriteConnection()->query("SELECT FROM_UNIXTIME(1459504500, '%d.%m.%Y, %H:%i');");

        self::assertSame('01.04.2016, 09:55', $result->fetchColumn());
    }
}
