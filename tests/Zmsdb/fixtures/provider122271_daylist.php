<?php

// @codingStandardsIgnoreFile
return array (
  '22-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '22',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 1,
      'callcenter' => 1,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '2',
      'intern' => '4',
      'callcenter' => '4',
      'type' => 'free',
    ),
  ),
  '18-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '18',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 2,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '10',
      'callcenter' => '0',
      'type' => 'free',
    ),
  ),
  '19-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '19',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 1,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '20',
      'callcenter' => '0',
      'type' => 'free',
    ),
  ),
  '20-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '20',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 22,
      'callcenter' => 22,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '24',
      'intern' => '48',
      'callcenter' => '48',
      'type' => 'free',
    ),
  ),
  '23-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '23',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 39,
      'callcenter' => 39,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '39',
      'intern' => '78',
      'callcenter' => '78',
      'type' => 'free',
    ),
  ),
  '24-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '24',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 57,
      'intern' => 124,
      'callcenter' => 124,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '69',
      'intern' => '138',
      'callcenter' => '138',
      'type' => 'free',
    ),
  ),
  '25-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '25',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 86,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '94',
      'callcenter' => '0',
      'type' => 'free',
    ),
  ),
  '26-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '26',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 22,
      'intern' => 124,
      'callcenter' => 45,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '23',
      'intern' => '132',
      'callcenter' => '46',
      'type' => 'free',
    ),
  ),
);
