<?php

// @codingStandardsIgnoreFile
return array (
  '02-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '02',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 19,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '19',
      'intern' => '38',
      'callcenter' => '19',
      'type' => 'free',
    ),
  ),
  '09-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '09',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 24,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '24',
      'intern' => '48',
      'callcenter' => '24',
      'type' => 'free',
    ),
  ),
  '23-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '23',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 26,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '26',
      'intern' => '52',
      'callcenter' => '26',
      'type' => 'free',
    ),
  ),
  '30-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '30',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 25,
      'intern' => 50,
      'callcenter' => 25,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '25',
      'intern' => '50',
      'callcenter' => '25',
      'type' => 'free',
    ),
  ),
  '04-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '04',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '26',
      'intern' => '52',
      'callcenter' => '26',
      'type' => 'full',
    ),
  ),
  '11-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '11',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '26',
      'intern' => '52',
      'callcenter' => '26',
      'type' => 'full',
    ),
  ),
  '18-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '18',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '26',
      'intern' => '52',
      'callcenter' => '26',
      'type' => 'full',
    ),
  ),
  '25-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '25',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '26',
      'intern' => '52',
      'callcenter' => '26',
      'type' => 'full',
    ),
  ),
);
