<?php

// @codingStandardsIgnoreFile
return array (
  '27-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '27',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 30,
      'intern' => 30,
      'callcenter' => 30,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'free',
    ),
  ),
);
