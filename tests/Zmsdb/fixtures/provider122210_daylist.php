<?php

// @codingStandardsIgnoreFile
return array (
  '27-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '27',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 13,
      'intern' => 13,
      'callcenter' => 13,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '18',
      'intern' => '18',
      'callcenter' => '18',
      'type' => 'free',
    ),
  ),
  '01-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '01',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '30',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '05-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '05',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '46',
      'intern' => '46',
      'callcenter' => '46',
      'type' => 'full',
    ),
  ),
  '08-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '08',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
  '12-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '12',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '46',
      'intern' => '46',
      'callcenter' => '46',
      'type' => 'full',
    ),
  ),
  '15-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '15',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
  '19-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '19',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '46',
      'intern' => '46',
      'callcenter' => '46',
      'type' => 'full',
    ),
  ),
  '22-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '22',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
  '26-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '26',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '46',
      'intern' => '46',
      'callcenter' => '46',
      'type' => 'full',
    ),
  ),
  '29-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '29',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
  '03-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '03',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '46',
      'intern' => '46',
      'callcenter' => '46',
      'type' => 'full',
    ),
  ),
  '06-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '06',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
  '10-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '10',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
  '13-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '13',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
  '17-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '17',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
  '20-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '20',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
  '24-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '24',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '30',
      'intern' => '30',
      'callcenter' => '30',
      'type' => 'full',
    ),
  ),
);
