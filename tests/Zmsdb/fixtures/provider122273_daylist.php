<?php

// @codingStandardsIgnoreFile
return array (
  '06-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '06',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 1,
      'callcenter' => 1,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '2',
      'intern' => '4',
      'callcenter' => '4',
      'type' => 'free',
    ),
  ),
  '21-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '21',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '1',
      'intern' => '2',
      'callcenter' => '2',
      'type' => 'free',
    ),
  ),
  '12-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '12',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '1',
      'intern' => '2',
      'callcenter' => '2',
      'type' => 'free',
    ),
  ),
  '19-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '19',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 1,
      'callcenter' => 1,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '7',
      'intern' => '14',
      'callcenter' => '14',
      'type' => 'free',
    ),
  ),
  '20-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '20',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 45,
      'callcenter' => 45,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '45',
      'intern' => '90',
      'callcenter' => '90',
      'type' => 'free',
    ),
  ),
  '23-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '23',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 43,
      'callcenter' => 43,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '43',
      'intern' => '86',
      'callcenter' => '86',
      'type' => 'free',
    ),
  ),
  '24-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '24',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 75,
      'intern' => 150,
      'callcenter' => 150,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '75',
      'intern' => '150',
      'callcenter' => '150',
      'type' => 'free',
    ),
  ),
  '25-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '25',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 47,
      'intern' => 94,
      'callcenter' => 94,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '47',
      'intern' => '94',
      'callcenter' => '94',
      'type' => 'free',
    ),
  ),
  '26-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '26',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 63,
      'intern' => 138,
      'callcenter' => 138,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '75',
      'intern' => '150',
      'callcenter' => '150',
      'type' => 'free',
    ),
  ),
);
