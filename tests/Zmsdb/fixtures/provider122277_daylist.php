<?php

// @codingStandardsIgnoreFile
return array (
  '17-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '17',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 1,
      'callcenter' => 1,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '2',
      'intern' => '4',
      'callcenter' => '4',
      'type' => 'free',
    ),
  ),
  '19-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '19',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 1,
      'callcenter' => 1,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '4',
      'intern' => '8',
      'callcenter' => '8',
      'type' => 'free',
    ),
  ),
  '20-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '20',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 29,
      'callcenter' => 29,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '29',
      'intern' => '58',
      'callcenter' => '58',
      'type' => 'free',
    ),
  ),
  '23-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '23',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 43,
      'callcenter' => 43,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '43',
      'intern' => '86',
      'callcenter' => '86',
      'type' => 'free',
    ),
  ),
  '24-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '24',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 38,
      'intern' => 87,
      'callcenter' => 87,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '52',
      'intern' => '104',
      'callcenter' => '104',
      'type' => 'free',
    ),
  ),
  '25-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '25',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 45,
      'intern' => 92,
      'callcenter' => 92,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '47',
      'intern' => '94',
      'callcenter' => '94',
      'type' => 'free',
    ),
  ),
  '26-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '26',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 39,
      'intern' => 82,
      'callcenter' => 82,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '47',
      'intern' => '94',
      'callcenter' => '94',
      'type' => 'free',
    ),
  ),
);
