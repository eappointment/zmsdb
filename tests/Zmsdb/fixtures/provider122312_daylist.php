<?php

// @codingStandardsIgnoreFile
return array (
  '23-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '23',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '2',
      'intern' => '2',
      'callcenter' => '2',
      'type' => 'free',
    ),
  ),
  '25-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '25',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '8',
      'intern' => '8',
      'callcenter' => '8',
      'type' => 'free',
    ),
  ),
  '26-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '26',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '14',
      'intern' => '14',
      'callcenter' => '14',
      'type' => 'free',
    ),
  ),
  '27-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '27',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 72,
      'intern' => 72,
      'callcenter' => 72,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '90',
      'intern' => '90',
      'callcenter' => '90',
      'type' => 'free',
    ),
  ),
  '30-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '30',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 118,
      'intern' => 118,
      'callcenter' => 118,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '166',
      'intern' => '166',
      'callcenter' => '166',
      'type' => 'free',
    ),
  ),
);
