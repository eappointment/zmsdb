<?php

// @codingStandardsIgnoreFile
return array (
  '20-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '20',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 21,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '24',
      'callcenter' => '0',
      'type' => 'free',
    ),
  ),
  '23-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '23',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 15,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '16',
      'callcenter' => '0',
      'type' => 'free',
    ),
  ),
  '24-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '24',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 4,
      'callcenter' => 4,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '4',
      'callcenter' => '4',
      'type' => 'free',
    ),
  ),
  '25-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '25',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 14,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '14',
      'callcenter' => '0',
      'type' => 'free',
    ),
  ),
  '01-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '01',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '15',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '04-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '04',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '31',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '05-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '05',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '22',
      'callcenter' => '22',
      'type' => 'full',
    ),
  ),
  '06-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '06',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '14',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '07-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '07',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '28',
      'callcenter' => '16',
      'type' => 'full',
    ),
  ),
  '08-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '08',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '22',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '11-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '11',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '22',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '12-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '12',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '18',
      'callcenter' => '18',
      'type' => 'full',
    ),
  ),
  '13-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '13',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '14',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '14-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '14',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '22',
      'callcenter' => '22',
      'type' => 'full',
    ),
  ),
  '15-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '15',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '30',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '18-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '18',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '22',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '19-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '19',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '18',
      'callcenter' => '18',
      'type' => 'full',
    ),
  ),
  '20-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '20',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '14',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '21-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '21',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '22',
      'callcenter' => '22',
      'type' => 'full',
    ),
  ),
  '22-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '22',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '30',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '25-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '25',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '8',
      'intern' => '22',
      'callcenter' => '8',
      'type' => 'full',
    ),
  ),
  '26-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '26',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '21',
      'callcenter' => '21',
      'type' => 'full',
    ),
  ),
  '27-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '27',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '14',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '28-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '28',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '22',
      'callcenter' => '22',
      'type' => 'full',
    ),
  ),
  '29-04-2016' => 
  array (
    'year' => '2016',
    'month' => '04',
    'day' => '29',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '7',
      'intern' => '14',
      'callcenter' => '7',
      'type' => 'full',
    ),
  ),
  '02-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '02',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '28',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '03-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '03',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '30',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '04-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '04',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '28',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '06-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '06',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '14',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '09-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '09',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '22',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '10-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '10',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '18',
      'callcenter' => '18',
      'type' => 'full',
    ),
  ),
  '11-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '11',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '14',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '12-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '12',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '22',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '13-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '13',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '30',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '17-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '17',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '18',
      'callcenter' => '18',
      'type' => 'full',
    ),
  ),
  '18-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '18',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '14',
      'callcenter' => '0',
      'type' => 'full',
    ),
  ),
  '19-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '19',
    'status' => 'notBookable',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '0',
      'intern' => '22',
      'callcenter' => '22',
      'type' => 'full',
    ),
  ),
);
