<?php

// @codingStandardsIgnoreFile
return array (
  '09-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '09',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '1',
      'intern' => '2',
      'callcenter' => '1',
      'type' => 'free',
    ),
  ),
  '23-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '23',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 55,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '59',
      'intern' => '118',
      'callcenter' => '59',
      'type' => 'free',
    ),
  ),
  '26-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '26',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '10',
      'intern' => '10',
      'callcenter' => '10',
      'type' => 'free',
    ),
  ),
  '27-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '27',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 32,
      'intern' => 32,
      'callcenter' => 32,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '54',
      'intern' => '54',
      'callcenter' => '54',
      'type' => 'free',
    ),
  ),
  '28-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '28',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 4,
      'intern' => 4,
      'callcenter' => 4,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '10',
      'intern' => '10',
      'callcenter' => '10',
      'type' => 'free',
    ),
  ),
  '30-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '30',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 55,
      'intern' => 133,
      'callcenter' => 55,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '79',
      'intern' => '158',
      'callcenter' => '79',
      'type' => 'free',
    ),
  ),
);
