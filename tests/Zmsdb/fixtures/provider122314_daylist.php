<?php

// @codingStandardsIgnoreFile
return array (
  '25-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '25',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '6',
      'intern' => '6',
      'callcenter' => '6',
      'type' => 'free',
    ),
  ),
  '26-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '26',
    'status' => 'full',
    'freeAppointments' => 
    array (
      'public' => 0,
      'intern' => 0,
      'callcenter' => 0,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '14',
      'intern' => '14',
      'callcenter' => '14',
      'type' => 'free',
    ),
  ),
  '27-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '27',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 76,
      'intern' => 76,
      'callcenter' => 76,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '82',
      'intern' => '82',
      'callcenter' => '82',
      'type' => 'free',
    ),
  ),
  '30-05-2016' => 
  array (
    'year' => '2016',
    'month' => '05',
    'day' => '30',
    'status' => 'bookable',
    'freeAppointments' => 
    array (
      'public' => 163,
      'intern' => 163,
      'callcenter' => 163,
      'type' => 'sum',
    ),
    'allAppointments' => 
    array (
      'public' => '166',
      'intern' => '166',
      'callcenter' => '166',
      'type' => 'free',
    ),
  ),
);
