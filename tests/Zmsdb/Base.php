<?php

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\Connection\Select;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsentities\Schema\Entity;
use DateTimeImmutable;
use DateTimeInterface;
use PHPUnit\Framework\TestCase;

abstract class Base extends TestCase
{
    public const DB_OPERATION_SECONDS_DEFAULT = 1;
    public static $username = 'superuser';
    public static $password = 'vorschau';
    /** @var DateTimeImmutable|null  */
    public static $now = null;
    public static $requestRelationCount = 2532;
    public static $requestCount = 308;

    /**
     * @throws PDOFailed
     */
    public function setUp(): void
    {
        static::$now = new DateTimeImmutable('2016-04-01T11:55:00+02:00');
        Select::setTransaction();
        Select::setProfiling(true);
        Select::setQueryCache(false);
        Select::setTimeZone('UTC', '+00:00');
        Select::getWriteConnection();
    }

    public function tearDown(): void
    {
        //error_log("Memory usage " . round(memory_get_peak_usage() / 1024, 0) . "kb");
        \BO\Zmsdb\Scope::$cache = [];
        \BO\Zmsdb\Availability::$cache = [];
        \BO\Zmsdb\Department::$departmentCache = [];
        \BO\Zmsdb\DayOff::$commonList = null;
        Select::writeRollback();
        Select::closeWriteConnection();
        Select::closeReadConnection();
        //Select::writeCommit();
        Select::setTransaction(false);
    }

    protected function getFixturePath($filename)
    {
        $path = dirname(__FILE__) . '/fixtures/' . $filename;
        return $path;
    }

    public function readFixture($filename)
    {
        $path = $this->getFixturePath($filename);
        if (!is_readable($path) || !is_file($path)) {
            throw new \Exception("Fixture $path is not readable");
        }
        return file_get_contents($path);
    }

    public function assertEntity($entityClass, $entity)
    {
        $this->assertInstanceOf($entityClass, $entity);
        /** @var Entity $entity */
        $this->assertTrue($entity->testValid());
    }

    public function assertEntityList($entityClass, $entityList)
    {
        foreach ($entityList as $entity) {
            $this->assertEntity($entityClass, $entity);
        }
    }

    protected function dumpProfiler()
    {
        echo "\nProfiler:\n";
        $profiles = Select::getReadConnection()->getProfiler()->getProfiles();
        foreach ($profiles as $profile) {
            $this->dumpProfile($profile);
        }
        /*
        $profiling = \BO\Zmsdb\Connection\Select::getReadConnection()->fetchAll('SHOW PROFILES');
        foreach ($profiling as $profile) {
            echo $profile['Query_ID']. ' ' . $profile['Duration']. ' ' . $profile['Query'] . "\n";
        }
        */
        //var_dump($profiling);
    }

    protected function dumpProfile($profile)
    {
        $statement = $profile['statement'];
        $statement = preg_replace('#\s+#', ' ', $statement);
        $statement = substr($statement, 0, 250);
        echo round($profile['duration'] * 1000, 6) . "ms $statement \n";
    }
}
