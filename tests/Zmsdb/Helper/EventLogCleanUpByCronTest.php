<?php

declare(strict_types=1);

namespace BO\Zmsdb\Tests\Helper;

use BO\Zmsdb\EventLog as EventLogRepository;
use BO\Zmsdb\Helper\EventLogCleanUpByCron;
use BO\Zmsentities\EventLog;
use PHPUnit\Framework\TestCase;
use BO\Zmsdb\Log\TestLogger;

class EventLogCleanUpByCronTest extends TestCase
{
    /** @var EventLogRepository  */
    protected $repo;

    protected $ref;

    public function setUp(): void
    {
        $this->repo = new EventLogRepository();
        $this->ref  = (string) random_int(0, 99999);
        $entry = new EventLog();
        $entry->addData([
            'name' => EventLog::CLIENT_PROCESSLIST_REQUEST,
            'origin' => 'zmsapi 2.2.2',
            'referenceType' => 'system.test.run',
            'reference' => $this->ref,
            'creationDateTime' => new \DateTime('2001-01-01 01:01:01'),
        ])->setSecondsToLive(EventLog::LIVETIME_HOUR, new \DateTime('2001-01-01 01:01:01'));
        $this->repo->writeEntity($entry);
    }

    public function testStartProcessing(): void
    {
        $logger = new TestLogger();
        self::assertCount(1, $this->repo->readByNameAndRef(EventLog::CLIENT_PROCESSLIST_REQUEST, $this->ref));
        $sut = new EventLogCleanUpByCron(true, $logger);
        $sut->startProcessing(true, new \DateTime('2001-01-01 03:02:01'));
        self::assertStringContainsString('Delete old eventlog entries', $logger->records[0]['message']);
        self::assertSame(0, $this->repo->readByNameAndRef(EventLog::CLIENT_PROCESSLIST_REQUEST, $this->ref)->count());
    }
}
