<?php

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\Helper\PickupsDeleteByCron;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Scope as ScopeRepository;

class PickupsDeleteByCronTest extends Base
{
    public function testConstructor(): void
    {
        $helper = new PickupsDeleteByCron(static::$now, false, true);
        $this->assertInstanceOf(PickupsDeleteByCron::class, $helper);
    }

    public function testWithoutExpiredPickups(): void
    {
        $now = static::$now;
        $expiredDate = clone $now->modify('-165 days');

        $scope = (new ScopeRepository())->readEntity(141, 0, true);
        (new ProcessRepository())->writeNewPickup($scope, $expiredDate);

        $deletePickupsDryRun = new PickupsDeleteByCron($now, false, true); // verbose
        $deletePickupsDryRun->setLimit(300);
        $deletePickupsDryRun->startProcessing();

        $this->assertEquals(0, $deletePickupsDryRun->getCount()[141]);
    }

    public function testWithExpiredPickups(): void
    {
        $now = new \DateTimeImmutable('2016-04-01 08:00:00');
        $expiredDate = clone $now->modify('-1 year');

        $scope = (new ScopeRepository())->readEntity(141, 0, true);
        (new ProcessRepository())->writeNewPickup($scope, $expiredDate);

        $deletePickupsDryRun = new PickupsDeleteByCron($now, false, true); // verbose
        $deletePickupsDryRun->setLimit(300);
        $deletePickupsDryRun->startProcessing();

        $this->assertEquals(1, $deletePickupsDryRun->getCount()[141]);
    }
}
