<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Tests\Helper;

use BO\Zmsdb\AccessStats as Repository;
use BO\Zmsdb\Helper\AccessStatsCleanUp;
use BO\Zmsdb\Log\TestLogger;
use BO\Zmsentities\AccessStats;
use PHPUnit\Framework\TestCase;

class AccessStatsCleanUpTest extends TestCase
{
    public function setUp(): void
    {
        $now = new \DateTime('2001-01-01 01:01:01');
        $entity = new AccessStats();
        $entity->lastActive = $now->getTimestamp() - Repository::DELETE_ENTRIES_AFTER_SECONDS - 1;

        $repo = new Repository();
        $repo->writeEntity($entity);
    }

    public function testStartProcessing(): void
    {
        $now = new \DateTime('2001-01-01 01:01:01');
        $logger = new TestLogger();
        $sut = new AccessStatsCleanUp($logger, $now);

        $sut->startProcessing();

        self::assertStringContainsString('1 accessstats table lines have been deleted', $logger->records[0]['message']);
    }
}
