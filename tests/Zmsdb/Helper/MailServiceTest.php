<?php

namespace BO\Zmsdb\Tests;

use BO\Mellon\ValidMail;
use BO\Zmsdb\Helper\MailService;
use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Department as DepartmentRepository;
use BO\Zmsdb\Mail as MailRepository;
use BO\Zmsentities\Collection\ProcessList;
use BO\Zmsentities\Mail as MailEntity;

class MailServiceTest extends Base
{

    public function testConstructor(): void
    {
        $mailService = new MailService(new MailRepository(), new ConfigRepository(), new DepartmentRepository());
        $this->assertInstanceOf(MailService::class, $mailService);

        $mailService->setMailAddress('test@test.com');
        $this->assertEquals('test@test.com', $mailService->getMailAddress());

        $mailService->setInitiator('unittest');
        $this->assertEquals('unittest', $mailService->getInitiator());

        $mailService->setProcessList(new ProcessList());
        $this->assertInstanceOf(ProcessList::class, $mailService->getProcessList());
    }

    public function testWriteMailInQueue(): void
    {
        $mailService = new MailService(new MailRepository(), new ConfigRepository(), new DepartmentRepository());
        $entity = (new MailEntity())->getExample();
        $tmpVal = ValidMail::$disableDnsChecks;
        ValidMail::$disableDnsChecks = true;

        $mail = $mailService->writeMailInQueue($entity, static::$now, false);

        ValidMail::$disableDnsChecks = $tmpVal;

        $this->assertInstanceOf(MailEntity::class, $mail);
    }

    public function testRestrictedMailAddress(): void
    {
        $this->expectException('\BO\Zmsdb\Exception\Mail\RestrictedMail');
        $mailService = new MailService(new MailRepository(), new ConfigRepository(), new DepartmentRepository());
        $entity = (new MailEntity())->getExample();
        $entity->getFirstClient()->email = "bo@service.berlinonline.de";
        $mailService->writeMailInQueue($entity, static::$now, false);
    }

    public function testRestrictedMailDomain(): void
    {
        $this->expectException('\BO\Zmsdb\Exception\Mail\RestrictedMail');
        $mailService = new MailService(new MailRepository(), new ConfigRepository(), new DepartmentRepository());
        $entity = (new MailEntity())->getExample();
        $entity->getFirstClient()->email = "bo@mailslurp.net";
        $mailService->writeMailInQueue($entity, static::$now, false);
    }
}
