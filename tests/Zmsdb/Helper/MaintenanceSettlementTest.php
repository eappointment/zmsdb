<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Tests\Helper;

use BO\Zmsdb\Helper\MaintenanceSettlement;
use BO\Zmsdb\MaintenanceSchedule;
use BO\Zmsdb\Tests\Mockups\ConfigRepoWithProperties;
use BO\Zmsentities\Config;
use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\MaintenanceSchedule as ScheduleEntity;
use BO\Zmsentities\Collection\MaintenanceScheduleList;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use Prophecy\PhpUnit\ProphecyTrait;

class MaintenanceSettlementTest extends TestCase
{
    use ProphecyTrait;

    public function testCheckActivation()
    {
        $configFake   = new ConfigRepoWithProperties();
        $scheduleList = new MaintenanceScheduleList();
        $repoProphecy = $this->prophesize(MaintenanceSchedule::class);
        $repoProphecy->readList()->willReturn($scheduleList);
        $repoProphecy->updateEntity(Argument::any())->willReturn(new ScheduleEntity());

        $scheduleEntry = new ScheduleEntity([
            'id'               => 42,
            'creatorId'        => 138,
            'creationDateTime' => DateTime::create('2015-12-21 12:34:56'),
            'startDateTime'    => null,
            'isActive'         => false,
            'isRepetitive'     => true,
            'timeString'       => '*/30 * * * *',
            'duration'         => 10,
            'area'             => 'zms',
            'documentBody'         => 'Wartung',
        ]);
        $scheduleList->offsetSet(42, $scheduleEntry);

        $testInst = new MaintenanceSettlement($configFake, $repoProphecy->reveal());
        $nowTime  = DateTime::create('2021-12-21 12:29:56');

        self::assertFalse($testInst->checkActivation($scheduleList, $nowTime));

        $reduced = (array) $scheduleEntry;
        $reduced['startDateTime'] = DateTime::create('2021-12-21 12:30:00')->format(DATE_ATOM);
        $configFake->replaceProperty(
            Config::PROPERTY_MAINTENANCE_NEXT,
            json_encode($reduced),
            "Don't change this! This is the next scheduled maintenance data"
        );

        self::assertFalse($testInst->checkActivation($scheduleList, $nowTime));

        self::assertTrue($testInst->checkActivation($scheduleList, $nowTime->modify('+1 minute')));
    }

    public function testDeactivateMaintenance()
    {
        $configFake   = new ConfigRepoWithProperties();
        $scheduleList = new MaintenanceScheduleList();
        $repoProphecy = $this->prophesize(MaintenanceSchedule::class);
        $repoProphecy->readList()->willReturn($scheduleList);
        $repoProphecy
            ->updateEntity(Argument::type(ScheduleEntity::class))
            ->shouldBeCalledOnce()
            ->willReturn(new ScheduleEntity());

        $scheduleEntry = new ScheduleEntity([
            'id'               => 42,
            'creatorId'        => 138,
            'creationDateTime' => DateTime::create('2015-12-21 12:34:56'),
            'startDateTime'    => DateTime::create('2021-12-21 12:30:00'),
            'isActive'         => true,
            'isRepetitive'     => true,
            'timeString'       => '*/30 * * * *',
            'duration'         => 10,
            'area'             => 'zms',
            'documentBody'         => 'Wartung',
        ]);
        $scheduleList->offsetSet(42, $scheduleEntry);

        $testInst = new MaintenanceSettlement($configFake, $repoProphecy->reveal());

        $testInst->deactivateMaintenance();

        self::assertFalse($scheduleEntry->isActive());
        self::assertNull($scheduleEntry->getStartDateTime());
    }

    public function testScheduleNextMaintenance()
    {
        $scheduleNtt1 = new ScheduleEntity([
            'id'               => 1,
            'creatorId'        => 138,
            'creationDateTime' => DateTime::create('2015-12-21 12:34:56'),
            'startDateTime'    => null,
            'isActive'         => false,
            'isRepetitive'     => true,
            'timeString'       => '*/30 * * * *',
            'duration'         => 15,
            'area'             => 'zms',
            'documentBody'         => 'Wartung',
        ]);

        $scheduleNtt2 = new ScheduleEntity([
            'id'               => 2,
            'creatorId'        => 138,
            'creationDateTime' => DateTime::create('2015-12-21 12:34:56'),
            'startDateTime'    => DateTime::create('2021-12-21 12:50:00'),
            'isActive'         => true,
            'isRepetitive'     => false,
            'timeString'       => '2021-12-21 12:50:00',
            'duration'         => 30,
            'area'             => 'zms',
            'documentBody'         => 'Wartung',
        ]);


        $scheduleList = new MaintenanceScheduleList();
        $scheduleList->offsetSet(2, $scheduleNtt2);

        $scheduleRepoProphecy = $this->prophesize(MaintenanceSchedule::class);
        $scheduleRepoProphecy
            ->readList()
            ->willReturn($scheduleList);

        $scheduleRepoProphecy
            ->updateEntity(Argument::type(ScheduleEntity::class))
            ->willReturn(new ScheduleEntity());

        $nowTime = DateTime::create('2021-12-21 13:00:00');
        $configFake = new ConfigRepoWithProperties();
        $testedService = new MaintenanceSettlement($configFake, $scheduleRepoProphecy->reveal());

        $testedService->scheduleNextMaintenance($scheduleList, $nowTime);

        self::assertNull($testedService->getPlannedMaintenance());

        $scheduleList->offsetSet(1, $scheduleNtt1);
        $testedService->scheduleNextMaintenance($scheduleList, $nowTime);

        self::assertInstanceOf(ScheduleEntity::class, $testedService->getPlannedMaintenance());

        $plannedEntry = $testedService->getPlannedMaintenance();

        self::assertSame('13:30:00', $plannedEntry->getStartDateTime()->format('H:i:s'));
    }
}
