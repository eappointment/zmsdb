<?php

namespace BO\Zmsdb\Tests\Helper;

use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Helper\CalculateSlots;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Slot as SlotRepository;
use BO\Zmsdb\Scope as ScopeRepository;

use BO\Zmsdb\Query\Process as ProcessQuery;
use BO\Zmsdb\Query\Slot as SlotQuery;
use BO\Zmsdb\Query\Base as BaseQuery;

use BO\Zmsdb\Tests\Base;
use BO\Zmsentities\Exception\ProcessBookableFailed;

use DateTimeImmutable;

/**
 *
 * @SuppressWarnings(Coupling)
 *
 */
class CalculateSlotsTest extends Base
{
    public function testConstructor()
    {
        $helper = new CalculateSlots(false, true);
        self::assertInstanceOf(CalculateSlots::class, $helper);
        self::assertLessThanOrEqual(0.001, $helper->getSpendTime());
        self::assertFalse($helper->isVerbose());
    }

    public function testLog()
    {
        $helper = new CalculateSlots(true, true);

        $capture = tmpfile();
        $backup = ini_set('error_log', stream_get_meta_data($capture)['uri']);
        $helper->log('This is a unit test');
        $helper->dumpLogs();
        ini_set('error_log', $backup);
        $logMessage = stream_get_contents($capture);
        self::assertStringContainsString('This is a unit test', $logMessage);
    }

    /**
     * @throws ProcessBookableFailed|PDOFailed
     */
    public function testWriteCalculations()
    {
        $configRepository = new ConfigRepository();
        $configRepository->replaceProperty('status__calculateSlotsForceVerbose', 0);
        $helper = new CalculateSlots(false, false);
        $status = $helper->writeCalculations(static::$now->modify('+ 5 minutes'), true);
        self::assertTrue($status);
    }

    /**
     * @throws ProcessBookableFailed
     * @throws PDOFailed
     */
    public function testWriteCalculationsWithCalculateSkip()
    {
        $configRepository = new ConfigRepository();
        $configRepository->replaceProperty('status__calculateSlotsSkip', 1);

        $helper = new CalculateSlots(false, true);
        $status = $helper->writeCalculations(static::$now);
        self::assertFalse($status);
    }

    /**
     * @throws ProcessBookableFailed
     * @throws PDOFailed
     */
    public function testLockedCalculations()
    {
        $configRepository = new ConfigRepository();
        $configRepository->replaceProperty('status__calculateSlotsIsLocked', '2016-04-01 12:00:00');

        $helper = new CalculateSlots(false, true);
        $status = $helper->writeCalculations(static::$now);
        self::assertFalse($status);
    }

    /**
     * @throws ProcessBookableFailed
     * @throws PDOFailed
     */
    public function testWriteCalculationsWithForcedVerbose()
    {
        $configRepository = new ConfigRepository();
        $configRepository->replaceProperty('status__calculateSlotsForceVerbose', 1);
        $helper = new CalculateSlots(false, true);

        $capture = tmpfile();
        $backup = ini_set('error_log', stream_get_meta_data($capture)['uri']);
        $helper->writeCalculations(static::$now);
        ini_set('error_log', $backup);
        $logMessage = stream_get_contents($capture);
        self::assertStringContainsString('Forced verbose', $logMessage);
    }

    /**
     * @throws PDOFailed
     */
    public function testWriteMaintenanceQueriesSql()
    {
        $configRepository = new ConfigRepository();
        $configRepository->replaceProperty(
            'status__calculateSlotsMaintenanceSQL',
            "UPDATE ". SlotQuery::TABLE ." SET status = 'free' WHERE status = 'free'"
        );
        $helper = new CalculateSlots(true, true);
        $capture = tmpfile();
        $backup = ini_set('error_log', stream_get_meta_data($capture)['uri']);
        $helper->writeMaintenanceQueries();
        ini_set('error_log', $backup);
        $logMessage = stream_get_contents($capture);
        self::assertStringContainsString('Maintenance query:', $logMessage);
    }

    /**
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     */
    public function testWritePostProcessingByScope()
    {
        $scope = (new ScopeRepository())->readEntity(141);
        $now = new DateTimeImmutable();
        $query = new ProcessQuery(BaseQuery::UPDATE);
        $query->addConditionProcessId(187528);
        $query->addConditionAuthKey('f1bc');

        $query->addValues(
            [
                'updateTimestamp' => $now->format('Y-m-d H:i:s'),
                'StandortID' => 141
            ]
        );
        (new ProcessRepository())->writeItem($query);

        $helper = new CalculateSlots(true, true);
        $capture = tmpfile();
        $backup = ini_set('error_log', stream_get_meta_data($capture)['uri']);
        $helper->writePostProcessingByScope($scope, static::$now);
        ini_set('error_log', $backup);
        $logMessage = stream_get_contents($capture);
        self::assertStringContainsString(
            'Finished to free slots for changed/deleted processes for scope 141',
            $logMessage
        );
        (new SlotRepository())->perform(SlotQuery::QUERY_DELETE_SLOT_PROCESS_ID, ['processId'=>187528]);
        self::assertStringContainsString(
            'Updated Slot-Process-Mapping, mapped processes for scope 141',
            $logMessage
        );
    }

    /**
     * @throws ProcessBookableFailed
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     */
    public function testDeleteSlotProcessOnProcess()
    {
        $now = new DateTimeImmutable();
        $query = new ProcessQuery(BaseQuery::UPDATE);
        $query->addConditionProcessId(187528);
        $query->addConditionAuthKey('f1bc');

        $query->addValues(
            [
                'updateTimestamp' => $now->format('Y-m-d H:i:s'),
                'StandortID' => 141
            ]
        );
        (new ProcessRepository())->writeItem($query);

        $helper = new CalculateSlots(true, true);
        $capture = tmpfile();
        $backup = ini_set('error_log', stream_get_meta_data($capture)['uri']);

        $dateTime = clone static::$now;
        $dateTime = $dateTime->modify('+10 minutes');

        self::assertEquals(1, count((new SlotRepository())->fetchAll(
            \BO\Zmsdb\Query\Slot::QUERY_SELECT_DELETABLE_SLOT_PROCESS
        )));

        $helper->writeCalculations($dateTime);

        ini_set('error_log', $backup);
        $logMessage = stream_get_contents($capture);
        self::assertStringContainsString(
            'Finished to free slots for changed/deleted processes without scope filter',
            $logMessage
        );
        self::assertStringContainsString(
            'Updated Slot-Process-Mapping, mapped processes without scope filter',
            $logMessage
        );
        self::assertEquals(0, count((new SlotRepository())->fetchAll(
            \BO\Zmsdb\Query\Slot::QUERY_SELECT_DELETABLE_SLOT_PROCESS
        )));
    }
}
