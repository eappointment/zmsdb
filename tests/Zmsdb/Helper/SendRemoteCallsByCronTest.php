<?php

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\Config as ConfigRepository;
use BO\Zmsdb\EventLog as EventLogRepository;
use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Exception\Process\ProcessReserveFailed;
use BO\Zmsdb\Helper\SendRemoteCallsByCron;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\ProcessStatusFree as ProcessFreeRepository;
use BO\Zmsdb\RemoteCall as RemoteCallRepository;
use BO\Zmsentities\EventLog as EventLogEntity;
use BO\Zmsentities\RemoteCall as RemoteCallEntity;
use BO\Zmsdb\Log\TestLogger;

/**
 * @suppressWarnings(coupling)
 */
class SendRemoteCallsByCronTest extends Base
{
    /**
     * @var RemoteCallEntity
     */
    protected $changeRemotecall;
    protected $deleteRemotecall;

    /**
     * @throws ProcessReserveFailed
     * @throws LockTimeout
     * @throws PDOFailed
     * @throws DeadLockFound
     */
    public function setUp(): void
    {
        parent::setUp();

        // stub for the application
        $appStub = new class {
            public static $now; // this is used by Entity::getCurrentTimestamp()
        };
        class_alias(get_class($appStub), 'App');
        \App::$now = static::$now;


        $this->eventLogRepository = new EventLogRepository();
        $this->remoteCallRepository = new RemoteCallRepository();
        $this->processRepository = new ProcessFreeRepository();

        $input = (new ProcessArchiveTest())->getTestProcessEntity();
        $this->process = $this->processRepository->writeEntityReserved($input, static::$now);

        $remoteCall             = new RemoteCallEntity([
            'name' => RemoteCallEntity::TYPE_CHANGE,
            'url' => 'eapp-bda://bda.service.berlin.de/change/?' .
                'x-bdaRouting=email:stelleXy@berlin.de,xta2:postfachXy,inbox:inboxXy',
            'processArchiveId' => $this->process->processArchiveId,
        ]);
        $this->changeRemotecall = $this->remoteCallRepository->writeNewEntity($remoteCall);

        $remoteCall             = new RemoteCallEntity([
            'name' => RemoteCallEntity::TYPE_DELETE,
            'url' => 'eapp-bda://bda.service.berlin.de/delete/?' .
                'x-bdaRouting=email:stelleXy@berlin.de,xta2:postfachXy,inbox:inboxXy',
            'processArchiveId' => $this->process->processArchiveId,
        ]);
        $this->deleteRemotecall = $this->remoteCallRepository->writeNewEntity($remoteCall);

        $eventLogEntity = new EventLogEntity([
            'name' => EventLogEntity::REMOTECALL_CHANGE_REQUEST,
            'origin' => 'zmsdb',
            'referenceType' => 'remotecall.id',
            'reference' => $this->changeRemotecall->getId(),
        ]);
        $eventLogEntity->setSecondsToLive(
            EventLogEntity::LIVETIME_HOUR,
            \App::$now
        );
        $this->eventLogRepository->writeEntity($eventLogEntity);
    }

    public function testConstructor(): void
    {
        $bdaService = new class
        {
            public const MESSAGE_TYPE_CHANGE = "bda:qtermin:verschiebung";
            public const MESSAGE_TYPE_DELETE = "bda:qtermin:stornierung";
            public function sendRequest(string $url, string $messageName): string
            {
                return $url . ' - ' . $messageName;
            }
        };
        $logger = new TestLogger();
        $helper = new SendRemoteCallsByCron($bdaService, true, $logger);
        self::assertTrue($helper->verbose);
        self::assertInstanceOf(SendRemoteCallsByCron::class, $helper);
        self::assertInstanceOf(RemoteCallRepository::class, $helper->remoteCallRepository);
        self::assertInstanceOf(ConfigRepository::class, $helper->configRepository);
        self::assertInstanceOf(EventLogRepository::class, $helper->eventLogRepository);
        self::assertInstanceOf(ProcessRepository::class, $helper->processRepository);
    }

    /**
     * @throws PDOFailed
     * @throws DeadLockFound
     * @throws LockTimeout
     */
    public function testStartProcessing(): void
    {
        $logger = new TestLogger();
        self::assertCount(1, $this->eventLogRepository->readByName(EventLogEntity::REMOTECALL_CHANGE_REQUEST));
        $helper = new SendRemoteCallsByCron($this->getBdaServiceStub(), true, $logger);
        $helper->startProcessing(self::$now, true);

        self::assertStringContainsString(
            'eapp-bda://bda.service.berlin.de/change/?x-bdaRouting=email:stelleXy@berlin.de,xta2:postfachXy,' .
            'inbox:inboxXy - bda:qtermin:verschiebung',
            $logger->records[0]['message']
        );
        self::assertStringContainsString('Handled remotecall entries by event log: 1', $logger->records[1]['message']);
    }

    public function testRepetitiveChange(): void
    {
        $this->testStartProcessing();

        $eventLogEntity = new EventLogEntity([
            'name' => EventLogEntity::REMOTECALL_CHANGE_REQUEST,
            'origin' => 'zmsdb',
            'referenceType' => 'remotecall.id',
            'reference' => $this->changeRemotecall->getId(),
        ]);
        $eventLogEntity->setSecondsToLive(
            EventLogEntity::LIVETIME_HOUR,
            \App::$now
        );
        $this->eventLogRepository->writeEntity($eventLogEntity);
        $this->eventLogRepository->writeEntity($eventLogEntity);

        $logger = new TestLogger();
        self::assertCount(3, $this->eventLogRepository->readByName(EventLogEntity::REMOTECALL_CHANGE_REQUEST));
        $helper = new SendRemoteCallsByCron($this->getBdaServiceStub(), true, $logger);

        \App::$now = \App::$now->modify('+1 second');
        $helper->startProcessing(self::$now, true);

        self::assertStringContainsString('Handled remotecall entries by event log: 1', $logger->records[1]['message']);
        self::assertStringContainsString(
            'eapp-bda://bda.service.berlin.de/change/?x-bdaRouting=email:stelleXy@berlin.de,xta2:postfachXy,' .
            'inbox:inboxXy - bda:qtermin:verschiebung',
            $logger->records[0]['message']
        );

    }

    public function testDeletionCall(): void
    {
        $this->testRepetitiveChange();

        $eventLogEntity = new EventLogEntity([
            'name' => EventLogEntity::REMOTECALL_DELETE_REQUEST,
            'origin' => 'zmsdb',
            'referenceType' => 'remotecall.id',
            'reference' => $this->deleteRemotecall->getId(),
        ]);
        $eventLogEntity->setSecondsToLive(
            EventLogEntity::LIVETIME_HOUR,
            \App::$now
        );
        $this->eventLogRepository->writeEntity($eventLogEntity);

        $logger = new TestLogger();
        self::assertCount(1, $this->eventLogRepository->readByName(EventLogEntity::REMOTECALL_DELETE_REQUEST));
        $helper = new SendRemoteCallsByCron($this->getBdaServiceStub(), true, $logger);

        \App::$now = \App::$now->modify('+1 second');
        $helper->startProcessing(self::$now, true);

        self::assertStringContainsString(
            'eapp-bda://bda.service.berlin.de/delete/?x-bdaRouting=email:stelleXy@berlin.de,xta2:postfachXy,' .
            'inbox:inboxXy - bda:qtermin:stornierung',
            $logger->records[0]['message']
        );
        self::assertStringContainsString('Handled remotecall entries by event log: 1', $logger->records[1]['message']);

    }

    protected function getBdaServiceStub(): object
    {
        return new class
        {
            public const MESSAGE_TYPE_CHANGE = "bda:qtermin:verschiebung";
            public const MESSAGE_TYPE_DELETE = "bda:qtermin:stornierung";
            public function sendRequest(string $url, string $messageName): string
            {
                return $url . ' - ' . $messageName;
            }

            public function getHttpStatusCode(): int
            {
                return 200;
            }
        };
    }
}
