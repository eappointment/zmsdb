<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Tests\Helper;

use BO\Zmsdb\Helper\MaintenanceUpdateProcess;
use BO\Zmsdb\MaintenanceSchedule;
use BO\Zmsdb\Tests\Mockups\ConfigRepoWithProperties;
use BO\Zmsentities\Helper\DateTime;
use BO\Zmsentities\MaintenanceSchedule as ScheduleEntity;
use BO\Zmsentities\Collection\MaintenanceScheduleList;
use PHPUnit\Framework\TestCase;
use Prophecy\Argument;
use BO\Zmsdb\Log\TestLogger;
use Prophecy\PhpUnit\ProphecyTrait;

class MaintenanceUpdateProcessTest extends TestCase
{
    use ProphecyTrait;

    protected $repo;
    protected $logger;
    protected $repetitiveSample;
    protected $oneTimeSample;

    public function setUp(): void
    {
        $this->repo = new MaintenanceSchedule();
        $this->logger = new TestLogger();
        $this->repetitiveSample = new ScheduleEntity([
            'id'               => 1,
            'creatorId'        => 138,
            'creationDateTime' => DateTime::create('2015-12-21 12:34:56'),
            'startDateTime'    => null,
            'isActive'         => false,
            'isRepetitive'     => true,
            'timeString'       => '0 0 * * *',
            'duration'         => 60,
            'area'             => 'zms',
            'documentBody'     => 'Wartung',
        ]);
        $this->oneTimeSample = new ScheduleEntity([
            'id'               => 2,
            'creatorId'        => 138,
            'creationDateTime' => DateTime::create('2015-12-21 12:30:00'),
            'startDateTime'    => null,
            'isActive'         => false,
            'isRepetitive'     => false,
            'timeString'       => '2021-12-21 01:00:00',
            'duration'         => 120,
            'area'             => 'zms',
            'documentBody'     => 'Wartung',
        ]);
    }

    public function testStartProcessing(): void
    {
        $scheduleNtt1 = new ScheduleEntity([
            'id'               => 1,
            'creatorId'        => 138,
            'creationDateTime' => DateTime::create('2015-12-21 12:34:56'),
            'startDateTime'    => null,
            'isActive'         => false,
            'isRepetitive'     => true,
            'timeString'       => '*/30 * * * *',
            'duration'         => 15,
            'area'             => 'zms',
            'documentBody'         => 'Wartung',
        ]);

        $scheduleNtt2 = new ScheduleEntity([
            'id'               => 2,
            'creatorId'        => 138,
            'creationDateTime' => DateTime::create('2015-12-21 12:34:56'),
            'startDateTime'    => null,
            'isActive'         => false,
            'isRepetitive'     => false,
            'timeString'       => '2021-12-21 12:50:00',
            'duration'         => 15,
            'area'             => 'zms',
            'documentBody'         => 'Wartung',
        ]);

        $scheduleNtt3 = new ScheduleEntity([
            'id'               => 3,
            'creatorId'        => 138,
            'creationDateTime' => DateTime::create('2015-12-21 12:34:56'),
            'startDateTime'    => null,
            'isActive'         => false,
            'isRepetitive'     => true,
            'timeString'       => '*/20 * * * *',
            'duration'         => 10,
            'area'             => 'zms',
            'documentBody'         => 'Wartung',
        ]);

        $scheduleList = new MaintenanceScheduleList();
        $scheduleList->offsetSet(1, $scheduleNtt1);
        $scheduleList->offsetSet(2, $scheduleNtt2);
        $scheduleList->offsetSet(3, $scheduleNtt3);

        $configFake = new ConfigRepoWithProperties();
        $scheduleRepoProphecy = $this->prophesize(MaintenanceSchedule::class);
        $scheduleRepoProphecy
            ->readList()
            ->willReturn($scheduleList);

        $scheduleRepoProphecy
            ->updateEntity(Argument::type(ScheduleEntity::class))
            ->willReturn(new ScheduleEntity());

        $logger = new TestLogger();
        $nowTime = DateTime::create('2021-12-21 12:00:00');
        $testedService = new MaintenanceUpdateProcess($logger, $configFake, $scheduleRepoProphecy->reveal());

        $testedService->startProcessing($nowTime);

        for ($i = 0; $i < 11; $i++) {
            $nowTime = $nowTime->modify('+5 minutes');
            $testedService->startProcessing($nowTime);
        }

        $changedScheduleList = new MaintenanceScheduleList();
        $changedScheduleList->offsetSet(2, $scheduleNtt2);
        $scheduleRepoProphecy
            ->readList()
            ->willReturn($changedScheduleList);

        $nowTime = $nowTime->modify('+10 minutes');
        $testedService = new MaintenanceUpdateProcess($logger, $configFake, $scheduleRepoProphecy->reveal());
        $testedService->startProcessing($nowTime);

        $expected = [
            'The next maintenance has been scheduled and the possible start is 2021-12-21 12:20:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 12:20:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 12:20:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 12:20:00',
            'Starting maintenance mode at 2021-12-21 12:20:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-21 12:30:00',
            'The maintenance mode is active until 2021-12-21 12:30:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 12:30:00',
            'The maintenance mode ended 2021-12-21 12:30:00',
            'Starting maintenance mode at 2021-12-21 12:30:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-21 12:40:00',
            'The maintenance mode is active until 2021-12-21 12:45:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 12:40:00',
            'The maintenance mode is active until 2021-12-21 12:45:00',
            'Current maintenance mode was aborted at 2021-12-21 12:40:00',
            'Starting maintenance mode at 2021-12-21 12:40:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-21 12:50:00',
            'The maintenance mode is active until 2021-12-21 12:50:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 12:50:00',
            'The maintenance mode ended 2021-12-21 12:50:00',
            'Starting maintenance mode at 2021-12-21 12:50:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-21 13:00:00',
            'The maintenance mode is active until 2021-12-21 13:05:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 13:00:00',
            'The maintenance mode ended 2021-12-21 13:05:00',
            'There is no (further) maintenance scheduled',
        ];

        self::assertSame($expected, array_column($logger->records, 'message'));
    }

    public function testSeamlessSuccessor(): void
    {
        $this->oneTimeSample['timeString'] = '2021-12-21 10:30:00';
        $this->oneTimeSample['duration'] = 30;
        $this->repetitiveSample['timeString'] = '0 */1 * * *';
        $this->repetitiveSample['duration'] = 60;

        $this->oneTimeSample = $this->repo->writeEntity($this->oneTimeSample);
        $this->repetitiveSample = $this->repo->writeEntity($this->repetitiveSample);

        $nowTime = DateTime::create('2021-12-21 10:00:00');
        $configFake = new ConfigRepoWithProperties();

        $testedService = new MaintenanceUpdateProcess($this->logger, $configFake, $this->repo);
        $testedService->startProcessing($nowTime);

        for ($i = 0; $i < 12; $i++) {
            $nowTime = $nowTime->modify('+10 minutes');
            $testedService->startProcessing($nowTime);
        }

        $this->repo->deleteEntity($this->repetitiveSample->getId());
        $this->repo->deleteEntity($this->oneTimeSample->getId());

        $expected = [
            'The next maintenance has been scheduled and the possible start is 2021-12-21 10:30:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 10:30:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 10:30:00',
            'Starting maintenance mode at 2021-12-21 10:30:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-21 11:00:00',
            'The maintenance mode is active until 2021-12-21 11:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 11:00:00',
            'The maintenance mode is active until 2021-12-21 11:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 11:00:00',
            'The maintenance mode ended 2021-12-21 11:00:00',
            'Starting maintenance mode at 2021-12-21 11:00:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-21 12:00:00',
            'The maintenance mode is active until 2021-12-21 12:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 12:00:00',
            'The maintenance mode is active until 2021-12-21 12:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 12:00:00',
            'The maintenance mode is active until 2021-12-21 12:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 12:00:00',
            'The maintenance mode is active until 2021-12-21 12:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 12:00:00',
            'The maintenance mode is active until 2021-12-21 12:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 12:00:00',
            'The maintenance mode ended 2021-12-21 12:00:00',
            'Starting maintenance mode at 2021-12-21 12:00:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-21 13:00:00',
        ];

        self::assertSame($expected, array_column($this->logger->records, 'message'));
    }

    public function testSeamlessSelfRepetition(): void
    {
        $this->repetitiveSample['timeString'] = '0 */1 * * *';
        $this->repetitiveSample['duration'] = 60;
        $this->repetitiveSample = $this->repo->writeEntity($this->repetitiveSample);

        $nowTime = DateTime::create('2021-12-21 10:00:00');
        $configFake = new ConfigRepoWithProperties();

        $testedService = new MaintenanceUpdateProcess($this->logger, $configFake, $this->repo);
        $testedService->startProcessing($nowTime);

        for ($i = 0; $i < 7; $i++) {
            $nowTime = $nowTime->modify('+30 minutes');
            $testedService->startProcessing($nowTime);
        }

        $this->repo->deleteEntity($this->repetitiveSample->getId());

        $expected = [
            'The next maintenance has been scheduled and the possible start is 2021-12-21 11:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 11:00:00',
            'Starting maintenance mode at 2021-12-21 11:00:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-21 12:00:00',
            'The maintenance mode is active until 2021-12-21 12:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 12:00:00',
            'The maintenance mode ended 2021-12-21 12:00:00',
            'Starting maintenance mode at 2021-12-21 12:00:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-21 13:00:00',
            'The maintenance mode is active until 2021-12-21 13:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 13:00:00',
            'The maintenance mode ended 2021-12-21 13:00:00',
            'Starting maintenance mode at 2021-12-21 13:00:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-21 14:00:00',
            'The maintenance mode is active until 2021-12-21 14:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 14:00:00',

        ];

        self::assertSame($expected, array_column($this->logger->records, 'message'));
    }

    public function testOverlappingSelfRepetition(): void
    {
        $this->repetitiveSample['timeString'] = '0 */1 * * *';
        $this->repetitiveSample['duration'] = 70;
        $this->repetitiveSample = $this->repo->writeEntity($this->repetitiveSample);

        $nowTime = DateTime::create('2021-12-21 00:00:00');
        $configFake = new ConfigRepoWithProperties();

        $testedService = new MaintenanceUpdateProcess($this->logger, $configFake, $this->repo);
        $testedService->startProcessing($nowTime);

        for ($i = 0; $i < 6; $i++) {
            $nowTime = $nowTime->modify('+30 minutes');
            $testedService->startProcessing($nowTime);
        }

        $this->repo->deleteEntity($this->repetitiveSample->getId());

        $expected = [
            'The next maintenance has been scheduled and the possible start is 2021-12-21 01:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 01:00:00',
            'Starting maintenance mode at 2021-12-21 01:00:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-21 02:00:00',
            'The maintenance mode is active until 2021-12-21 02:10:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 02:00:00',
            'The maintenance mode is active until 2021-12-21 02:10:00',
            'Starting maintenance mode at 2021-12-21 02:00:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-21 03:00:00',
            'The maintenance mode is active until 2021-12-21 03:10:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 03:00:00',
            'The maintenance mode is active until 2021-12-21 03:10:00',
            'Starting maintenance mode at 2021-12-21 03:00:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-21 04:00:00',
        ];

        self::assertSame($expected, array_column($this->logger->records, 'message'));
    }

    public function testOverlappingPartly(): void
    {
        $this->oneTimeSample['timeString'] = '2021-12-21 06:00:00';
        $this->oneTimeSample['duration'] = 120;
        $this->repetitiveSample['timeString'] = '0 7 * * *';
        $this->repetitiveSample['duration'] = 180;

        $this->oneTimeSample = $this->repo->writeEntity($this->oneTimeSample);
        $this->repetitiveSample = $this->repo->writeEntity($this->repetitiveSample);

        $nowTime = DateTime::create('2021-12-21 05:00:00');
        $configFake = new ConfigRepoWithProperties();

        $testedService = new MaintenanceUpdateProcess($this->logger, $configFake, $this->repo);
        $testedService->startProcessing($nowTime);

        for ($i = 0; $i < 7; $i++) {
            $nowTime = $nowTime->modify('+60 minutes');
            $testedService->startProcessing($nowTime);
        }

        $this->repo->deleteEntity($this->repetitiveSample->getId());
        $this->repo->deleteEntity($this->oneTimeSample->getId());

        $expected = [
            'The next maintenance has been scheduled and the possible start is 2021-12-21 06:00:00',
            'Starting maintenance mode at 2021-12-21 06:00:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-21 07:00:00',
            'The maintenance mode is active until 2021-12-21 08:00:00',
            'Current maintenance mode was aborted at 2021-12-21 07:00:00',
            'Starting maintenance mode at 2021-12-21 07:00:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-22 07:00:00',
            'The maintenance mode is active until 2021-12-21 10:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-22 07:00:00',
            'The maintenance mode is active until 2021-12-21 10:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-22 07:00:00',
            'The maintenance mode ended 2021-12-21 10:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-22 07:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-22 07:00:00',
            'The maintenance schedule did not change and the possible start is 2021-12-22 07:00:00',
        ];

        self::assertSame($expected, array_column($this->logger->records, 'message'));
    }

    public function testOverlappingCompletely(): void
    {
        $this->oneTimeSample['timeString'] = '2021-12-21 01:30:00';
        $this->oneTimeSample['duration'] = 2900;
        $this->repetitiveSample['timeString'] = '0 8 * * *';
        $this->repetitiveSample['duration'] = 60;

        $this->oneTimeSample = $this->repo->writeEntity($this->oneTimeSample);
        $this->repetitiveSample = $this->repo->writeEntity($this->repetitiveSample);

        $nowTime = DateTime::create('2021-12-21 01:00:00');
        $configFake = new ConfigRepoWithProperties();

        $testedService = new MaintenanceUpdateProcess($this->logger, $configFake, $this->repo);
        $testedService->startProcessing($nowTime);

        for ($i = 0; $i < 9; $i++) {
            $nowTime = $nowTime->modify('+60 minutes');
            $testedService->startProcessing($nowTime);
        }

        $this->repo->deleteEntity($this->repetitiveSample->getId());
        $this->repo->deleteEntity($this->oneTimeSample->getId());

        $expected = [
            'The next maintenance has been scheduled and the possible start is 2021-12-21 01:30:00',
            'Starting maintenance mode at 2021-12-21 02:00:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-21 08:00:00',
            'The maintenance mode is active until 2021-12-23 01:50:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 08:00:00',
            'The maintenance mode is active until 2021-12-23 01:50:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 08:00:00',
            'The maintenance mode is active until 2021-12-23 01:50:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 08:00:00',
            'The maintenance mode is active until 2021-12-23 01:50:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 08:00:00',
            'The maintenance mode is active until 2021-12-23 01:50:00',
            'The maintenance schedule did not change and the possible start is 2021-12-21 08:00:00',
            'The maintenance mode is active until 2021-12-23 01:50:00',
            'The next maintenance has been scheduled and the possible start is 2021-12-22 08:00:00',
            'The maintenance mode is active until 2021-12-23 01:50:00',
            'The maintenance schedule did not change and the possible start is 2021-12-22 08:00:00',
            'The maintenance mode is active until 2021-12-23 01:50:00',
            'The maintenance schedule did not change and the possible start is 2021-12-22 08:00:00',
        ];

        self::assertSame($expected, array_column($this->logger->records, 'message'));
    }
}
