<?php

namespace BO\Zmsdb\Tests;

use BO\Mellon\ValidMail;
use BO\Zmsdb\Exception\Mail\ClientWithoutEmail;
use BO\Zmsdb\Exception\Pdo\DeadLockFound;
use BO\Zmsdb\Exception\Pdo\LockTimeout;
use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Helper\SendMailReminder;
use BO\Zmsdb\Process as ProcessRepository;
use BO\Zmsdb\Query\Process as ProcessQuery;
use BO\Zmsdb\Query\Base as BaseQuery;
use BO\Zmsdb\Log as LogRepository;
use BO\Zmsentities\Exception\TemplateNotFound;

class SendMailReminderTest extends Base
{

    public function testConstructor(): void
    {
        $helper = new SendMailReminder(static::$now);
        $this->assertInstanceOf(SendMailReminder::class, $helper);
    }

    public function testReminder2Hours(): void
    {
        $mailReminderDryRun = new SendMailReminder(static::$now, 2, false); // verbose
        $mailReminderDryRun->setLimit(10);

        $tmp = ValidMail::$disableDnsChecks;
        ValidMail::$disableDnsChecks = true;

        $mailReminderDryRun->startProcessing();

        ValidMail::$disableDnsChecks = $tmp;

        $this->assertEquals(10, $mailReminderDryRun->getCount());

        $logList = (new LogRepository())->readByProcessId(55662);

        $this->assertEquals(55662, $logList[2]['reference']);
        $this->assertStringContainsString('Write (Mail::writeInQueue)', $logList[2]['message']);
        $this->assertStringContainsString('Terminerinnerung', $logList[2]['message']);
        $this->assertStringNotContainsString('@', $logList[2]['message']);
    }

    public function testReminder24Hours(): void
    {
        $now = static::$now->modify('+1 days');
        $mailReminderDryRun = new SendMailReminder($now, 24, false); // verbose
        $mailReminderDryRun->setLimit(20);

        $tmp = ValidMail::$disableDnsChecks;
        ValidMail::$disableDnsChecks = true;

        $mailReminderDryRun->startProcessing();

        ValidMail::$disableDnsChecks = $tmp;

        $this->assertEquals(6, $mailReminderDryRun->getCount());
    }

    public function testReminderWithoutInvalidMailDNS(): void
    {
        $query = new ProcessRepository();
        // Use the ID of the process in the first test to check if it is processed with invalid email.
        $process = $query->readEntity(55662, '2d2c');
        $process->clients[0]['email'] = 'test.invalid.example.com';
        $query->updateEntity($process, static::$now);
        $mailReminderDryRun = new SendMailReminder(static::$now, 1, false); // verbose
        $mailReminderDryRun->setLimit(20);

        $tmp = ValidMail::$disableDnsChecks;
        ValidMail::$disableDnsChecks = true;

        $mailReminderDryRun->startProcessing();

        ValidMail::$disableDnsChecks = $tmp;

        $logList = (new LogRepository())->readByProcessId(55662);
        $this->assertEquals(55662, $logList[0]['reference']);
        $this->assertEquals(19, $mailReminderDryRun->getCount());
    }

    public function testReminderWithoutInvalidMailString(): void
    {
        $query = new ProcessRepository();
        // Use the ID of the process in the first test to check if it is processed with invalid email.
        $process = $query->readEntity(55662, '2d2c');
        $process->clients[0]['email'] = '1234567890';
        $process = $query->updateEntity($process, static::$now);
        
        $mailReminderDryRun = new SendMailReminder(static::$now, 1, false); // verbose
        $mailReminderDryRun->setLimit(20);

        $tmp = ValidMail::$disableDnsChecks;
        ValidMail::$disableDnsChecks = true;

        $mailReminderDryRun->startProcessing();

        ValidMail::$disableDnsChecks = $tmp;

        $logList = (new LogRepository())->readByProcessId(55662);
        $this->assertEquals(55662, $logList[0]['reference']);
        $this->assertEquals(19, $mailReminderDryRun->getCount());
    }

    public function testReminderWithoutEmail(): void
    {
        $mailReminderDryRun = new SendMailReminder(static::$now, 1, false); // verbose
        $mailReminderDryRun->setLimit(20);

        $tmp = ValidMail::$disableDnsChecks;
        ValidMail::$disableDnsChecks = true;

        $mailReminderDryRun->startProcessing();

        ValidMail::$disableDnsChecks = $tmp;

        self::assertEquals(20, $mailReminderDryRun->getCount());

        $query = new ProcessQuery(BaseQuery::UPDATE);
        $query->addConditionProcessId(55662);
        $query->addConditionAuthKey('2d2c');
        $query->addValues(['EMail' => '']);
        (new ProcessRepository())->writeItem($query);

        $mailReminderDryRun = new SendMailReminder(static::$now, 1, false); // verbose
        $mailReminderDryRun->setLimit(20);

        $tmp = ValidMail::$disableDnsChecks;
        ValidMail::$disableDnsChecks = true;

        $mailReminderDryRun->startProcessing();

        ValidMail::$disableDnsChecks = $tmp;

        self::assertEquals(20, $mailReminderDryRun->getCount());
    }

    /**
     * @throws LockTimeout
     * @throws TemplateNotFound
     * @throws PDOFailed
     * @throws ClientWithoutEmail
     * @throws DeadLockFound
     */
    public function testReminderWithRestrictedMail(): void
    {
        $query = new ProcessQuery(BaseQuery::UPDATE);
        $query->addConditionProcessId(55662);
        $query->addConditionAuthKey('2d2c');
        $query->addValues(['EMail' => 'test@dummy.de']);
        (new ProcessRepository())->writeItem($query);

        $mailReminderDryRun = new SendMailReminder(static::$now, 1, false); // verbose
        $mailReminderDryRun->setLimit(20);

        $tmp = ValidMail::$disableDnsChecks;
        ValidMail::$disableDnsChecks = true;

        $mailReminderDryRun->startProcessing();

        ValidMail::$disableDnsChecks = $tmp;

        $logList = (new LogRepository())->readByProcessId(55662);

        $this->assertEquals(55662, $logList[0]['reference']);
        $this->assertStringContainsString(
            'Failed to write email to test@dummy.de - mail restricted',
            $logList[0]['message']
        );
    }
}
