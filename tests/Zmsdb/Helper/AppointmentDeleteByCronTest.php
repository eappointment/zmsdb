<?php

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\Exception\Process\ProcessArchiveUpdateFailed;
use BO\Zmsdb\Helper\AppointmentDeleteByCron;
use BO\Zmsdb\Process as Query;
use BO\Zmsdb\ProcessStatusArchived;

use BO\Zmsentities\Process as ProcessEntity;

class AppointmentDeleteByCronTest extends Base
{

    public function testConstructor()
    {
        $now = new \DateTimeImmutable('2016-04-02 11:55');
        $helper = new AppointmentDeleteByCron(0, $now, false, true);
        $this->assertInstanceOf(AppointmentDeleteByCron::class, $helper);
    }

    /**
     * @throws ProcessArchiveUpdateFailed
     */
    public function testStartProcessingByCron()
    {
        $now = new \DateTimeImmutable('2016-04-02 00:10');
        $helper = new AppointmentDeleteByCron(0, $now, false, true); // verbose
        $helper->setLimit(10);
        $helper->setLoopCount(5);
        $helper->startProcessing(true);
        self::assertEquals(10, $helper->getCount()['confirmed']);
    }

    /**
     * @throws ProcessArchiveUpdateFailed
     */
    public function testStartProcessingExpiredExakt()
    {
        $now = new \DateTimeImmutable('2016-04-01 07:00');
        $expired = new \DateTimeImmutable('2016-04-01 07:00');
        $helper = new AppointmentDeleteByCron(0, $now, false, true); // verbose
        $helper->setLimit(10);
        $helper->setLoopCount(5);
        $helper->startProcessing(false);
        self::assertEquals(8, $helper->getCount()[ProcessEntity::STATUS_CONFIRMED]);
        self::assertCount(
            8,
            (new Query())->readExpiredProcessListByStatus($expired, ProcessEntity::STATUS_CONFIRMED)
        );
    }

    /**
     * @throws ProcessArchiveUpdateFailed
     */
    public function testStartProcessingExpiredExaktNoDryRun()
    {
        $now = new \DateTimeImmutable('2016-04-01 07:00');
        $expired = new \DateTimeImmutable('2016-04-01 07:00');
        $helper = new AppointmentDeleteByCron(0, $now, false);
        $helper->setLimit(10);
        $helper->setLoopCount(5);
        $helper->startProcessing(false);
        self::assertEquals(8, $helper->getCount()[ProcessEntity::STATUS_CONFIRMED]);
        self::assertCount(
            0,
            (new Query())->readExpiredProcessListByStatus($expired, ProcessEntity::STATUS_CONFIRMED)
        );
    }

    /**
     * @throws ProcessArchiveUpdateFailed
     */
    public function testStartProcessingBlockedPickup()
    {
        $now = static::$now;

        $scope = (new \BO\Zmsdb\Scope())->readEntity(141);
        $process = (new Query())->writeNewPickup($scope, $now);
        $process = (new Query())->readEntity($process->id, $process->authKey, 0);
        $process->status = ProcessEntity::STATUS_FINISHED;
        (new ProcessStatusArchived())->writeEntityFinished($process, $now);

        $helper = new AppointmentDeleteByCron(0, $now, false, true);
        $helper->setLimit(10);
        $helper->setLoopCount(5);
        $helper->startProcessing(false);
        self::assertEquals(
            1,
            count((new Query())->readProcessListByScopeAndStatus(
                0,
                ProcessEntity::STATUS_BLOCKED,
                0,
                10,
                0
            ))
        );

        $helper = new AppointmentDeleteByCron(0, $now, false);
        $helper->setLimit(10);
        $helper->setLoopCount(5);
        $helper->startProcessing(false);
        $appointmentUnits = count((new Query())->readProcessListByScopeAndStatus(
            0,
            ProcessEntity::STATUS_BLOCKED,
            0,
            10,
            0
        ));
        self::assertEquals(0, $appointmentUnits);
    }
}
