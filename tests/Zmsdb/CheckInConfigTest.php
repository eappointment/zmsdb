<?php

declare(strict_types=1);

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\CheckInConfig;
use BO\Zmsentities\CheckInConfig as ConfigEntity;

class CheckInConfigTest extends Base
{
    public function testPersistenceMapping(): void
    {
        $repo    = new CheckInConfig();
        $example = ConfigEntity::getExample();

        self::assertIsString($example->getId());
        $repo->persist($example);
        self::assertIsString($example->getId());

        $persisted = $repo->readById($example->getId());

        self::assertInstanceOf(ConfigEntity::class, $persisted);
        self::assertIsString($persisted->getId());
        self::assertSame($example->getId(), $persisted->getId());
        self::assertIsString($persisted->lastChange);
        self::assertNotEquals($persisted->lastChange, $example->lastChange);
        unset($persisted['lastChange']);
        unset($example['lastChange']);
        unset($persisted['id']);
        unset($example['id']);

        self::assertSame($example->__toString(), $persisted->__toString());
    }

    public function testReadByScopeId(): void
    {
        $repo = new CheckInConfig();
        $entity = (new ConfigEntity([]))->setScopeIds([123,234]);
        $repo->persist($entity);

        self::assertIsString($entity->getId());

        $persisted123 = $repo->readByScopeId(123);
        $persisted234 = $repo->readByScopeId(234);

        self::assertSame((array) $persisted234, (array) $persisted123);

        self::assertIsString($persisted234->getId());
        self::assertSame([123,234], $persisted234->getScopeIds());

        $byId = $repo->readById($persisted234->getId());

        self::assertSame((array) $persisted234, (array) $byId);
    }

    public function testListByDepartmentId(): void
    {
        $repo = new CheckInConfig();
        $entity = (new ConfigEntity([]))->setScopeIds([133,134]);
        $repo->persist($entity);

        $entity = (new ConfigEntity([]))->setScopeIds([135]);
        $repo->persist($entity);

        $list = $repo->listByDepartmentId(72);
        self::assertCount(2, $list);
    }
}
