<?php

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\Exception\Pdo\PDOFailed;
use BO\Zmsdb\Status as StatusRepository;
use BO\Zmsdb\Availability as AvailabilityRepository;
use BO\Zmsdb\Helper\CalculateSlots;
use BO\Zmsentities\Exception\ProcessBookableFailed;

class StatusTest extends Base
{
    public function testBasic()
    {
        $now = (new \DateTimeImmutable())->setTimestamp(static::$now->getTimestamp());
        $status = (new StatusRepository())->readEntity($now);
        $this->assertInstanceOf("\\BO\\Zmsentities\\Status", $status);
        //var_dump(\BO\Zmsdb\Connection\Select::getReadConnection()->getProfiler()->getProfiles());
    }
}
