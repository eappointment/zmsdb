<?php

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\Apiclient as Query;
use BO\Zmsentities\Apiclient as Entity;

class ApiclientTest extends Base
{
    public function testBasic()
    {
        $query = new Query();
        $entity = $query->readEntity('default');
        $this->assertEntity("\\BO\\Zmsentities\\Apiclient", $entity);

        $this->assertEquals('default', $entity->clientKey);
        $this->assertEquals('default', $entity->shortname);
    }

    public function testBlocked()
    {
        $query = new Query();
        $entity = $query->readEntity('8pnaRHkUBYJqz9i9NPDEeZq6mUDMyRHE');
        $this->assertEntity("\\BO\\Zmsentities\\Apiclient", $entity);

        $this->assertEquals('blocked', $entity->accesslevel);
    }

    public function testPermission()
    {
        $query = new Query();
        $entity = $query->readEntity('default');
        $this->assertFalse($entity->permission->hasRight('remotecall'));
    }

    public function testPermissionRemoteCall()
    {
        $query = new Query();
        $entity = $query->readEntity('default');
        $entity['permission']['remotecall'] = 1;

        $entityUpdated = $query->updateEntity($entity);
        $this->assertTrue($entityUpdated->getPermission()->hasRight('remotecall'));
    }

    public function testWriteEntity()
    {
        $input = $this->getTestEntity();
        $query = new Query();
        $entity = $query->writeEntity($input);
        $this->assertEntity("\\BO\\Zmsentities\\Apiclient", $entity);
        $this->assertEquals('wMdVa5Nu1seuCRSJxhKl2M3yw8zqaAil', $entity->clientKey);
    }

    protected function getTestEntity()
    {
        return (new Entity())->createExample();
    }
}
