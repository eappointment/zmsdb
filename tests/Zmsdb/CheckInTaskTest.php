<?php

declare(strict_types=1);

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\CheckInTask;
use BO\Zmsentities\CheckInTask as TaskEntity;
use BO\Zmsentities\Process as ProcessEntity;
use BO\Zmsentities\Collection\ProcessList;

class CheckInTaskTest extends Base
{
    public function testPersistenceMapping(): void
    {
        $repo    = new CheckInTask();
        $example = TaskEntity::getExample();
        unset($example['id']);

        self::assertFalse($example->getId());
        $repo->persist($example);
        self::assertIsInt($example->getId());

        /** @var TaskEntity $persisted */
        $persisted = $repo->readById($example->getId());

        self::assertInstanceOf(TaskEntity::class, $persisted);
        self::assertIsInt($persisted->getId());
        self::assertSame($example->getId(), $persisted->getId());
        self::assertIsString($persisted->lastChange);
        self::assertNotEquals($persisted->lastChange, $example->lastChange);

        $related = $repo->readByReferenceObject(new ProcessEntity(['id' => 666]));
        self::assertNull($related);
        $related = $repo->readByReferenceObject(new ProcessEntity(['id' => 123456]));
        self::assertSame($persisted->id, $related->id);

        unset($persisted['lastChange']);
        unset($example['lastChange']);
        unset($persisted['id']);
        unset($example['id']);

        self::assertSame($example->__toString(), $persisted->__toString());


    }

    public function testFilters(): void
    {
        $repo    = new CheckInTask();
        $testIds = [];
        /** @var TaskEntity $example */
        $example = TaskEntity::getExample(); // referenceId: 123456, scopeId: 123, scheduledTS: 1447922893

        unset($example['id']);
        $repo->persist($example);
        $testIds[123456] = $example->getId();

        unset($example['id']);
        $example->referenceId = 43210;
        $example->scopeId = 111;
        $example->status = TaskEntity::STATUS_NO_SHOW;
        $example->scheduledTS = 1447812345;
        $repo->persist($example);
        $testIds[43210] = $example->getId();

        unset($example['id']);
        $example->referenceId = 5432;
        $example->scopeId = 222;
        $example->status = TaskEntity::STATUS_EARLY;
        $example->scheduledTS = 1447923456;
        $example->checkInTS = 1447912345;
        $repo->persist($example);
        $testIds[5432] = $example->getId();

        unset($example['id']);
        $example->referenceId = 654;
        $example->scopeId = 222;
        $example->status = TaskEntity::STATUS_ARRIVED;
        $example->scheduledTS = 1447712345;
        $example->checkInTS = 1447712344;
        $repo->persist($example);
        $testIds[654] = $example->getId();

        // process id pool test
        unset($example['id']);
        $example->referenceId = 76;
        $example->scopeId = 333;
        $example->status = TaskEntity::STATUS_ARRIVED;
        $example->scheduledTS = 1347922222;
        $example->checkInTS = 1347922223;
        $repo->persist($example);

        unset($example['id']);
        $example->referenceId = 76;
        $example->scopeId = 333;
        $example->status = TaskEntity::STATUS_LATE;
        $example->scheduledTS = 1447922222;
        $example->checkInTS = 1447922223;
        $repo->persist($example);
        $testIds[76] = $example->getId();

        unset($example['id']);
        $example->referenceId = 8;
        $example->scopeId = 666;
        $example->status = TaskEntity::STATUS_NA;
        $example->scheduledTS = 1432101234;
        $example->checkInTS = 0;
        $repo->persist($example);
        $testIds[8] = $example->getId();

        $task = $repo->readByReferenceObject(new ProcessEntity(['id' => 76]));
        self::assertSame($testIds[76], $task->getId());

        $tmp = $repo->listByScopeIds([]);
        self::assertCount(0, $tmp);
        $tmp = $repo->listByScopeIds(explode(',', '111,222'));

        $resultIds = $tmp->getIds();
        asort($resultIds);

        self::assertSame([$testIds[43210], $testIds[5432], $testIds[654]], array_values($resultIds));

        $tmp = $repo->listByReferenceObjectList(new ProcessList());
        self::assertCount(0, $tmp);
        $tmp = $repo->listByReferenceObjectList(new ProcessList([
            new ProcessEntity(['id' => 654]),
            new ProcessEntity(['id' => 76]),
        ]));

        $resultIds = $tmp->getIds();
        asort($resultIds);

        self::assertSame([$testIds[654], $testIds[76]], array_values($resultIds));

        $tmp = $repo->listByStatuses(['', 1]);
        self::assertCount(0, $tmp);
        $tmp = $repo->listByStatuses([TaskEntity::STATUS_NO_SHOW, TaskEntity::STATUS_NA]);

        $resultIds = $tmp->getIds();
        asort($resultIds);

        self::assertSame([$testIds[43210], $testIds[8]], array_values($resultIds));

        $tmp = $repo->listByCheckInTime(0);
        self::assertCount(0, $tmp);
        $tmp = $repo->listByCheckInTime(0, 0);
        self::assertCount(3, $tmp);
        $tmp = $repo->listByCheckInTime(1447922222);

        $resultIds = $tmp->getIds();
        asort($resultIds);

        self::assertSame([$testIds[76]], array_values($resultIds));

        $tmp = $repo->listByScheduledTime(0);
        self::assertCount(0, $tmp);
        $tmp = $repo->listByScheduledTime(0, 1447777777);
        self::assertCount(3, $tmp);
        $tmp = $repo->listByScheduledTime(1447888888);

        $resultIds = $tmp->getIds();
        asort($resultIds);

        self::assertSame([$testIds[123456], $testIds[5432], $testIds[76]], array_values($resultIds));
    }
}
