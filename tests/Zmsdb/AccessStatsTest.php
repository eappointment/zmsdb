<?php
/**
 * @copyright BerlinOnline Stadtportal GmbH & Co. KG
 **/

declare(strict_types=1);

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\AccessStats as AccessStatsRepo;
use BO\Zmsentities\AccessStats as Entity;
use PHPUnit\Framework\TestCase;

class AccessStatsTest extends TestCase
{
    public function testRepository(): void
    {
        $repo = new AccessStatsRepo();
        $repo->freshenList(new \DateTime());
        $lastCount = count($repo->readList());
        $insertedId = $this->testWriteEntity();
        $this->testFetchCollection($lastCount + 1);
        $this->cleanUp($insertedId);
        $this->testFetchCollection($lastCount);
    }

    public function testWriteEntityFails(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $faultyRequest = new \stdClass();
        $faultyRequest->args = ['id' => null];

        $entity = new Entity($faultyRequest->args);
        (new AccessStatsRepo())->writeEntity($entity);
    }

    private function testWriteEntity(): int
    {
        $unsavedData = new Entity([
            'id'         => random_int(900000000000000000, PHP_INT_MAX),
            'role'       => 'citizen',
            'lastActive' => 2134567890,
            'location'   => 'zmsappointment',
        ]);

        $accessStatsRepo = new AccessStatsRepo();
        $createdEntity = $accessStatsRepo->writeEntity($unsavedData);

        self::assertNotNull($createdEntity);
        self::assertIsInt($createdEntity['id']);

        return $createdEntity['id'];
    }

    private function testFetchCollection(int $checkCount): void
    {
        $repo = new AccessStatsRepo();
        $list = $repo->readList();

        self::assertCount($checkCount, $list);
    }

    private function cleanUp(int $insertedId)
    {
        $accessStatsRepo = new AccessStatsRepo();
        $accessStatsRepo->deleteEntity($insertedId);
    }
}
