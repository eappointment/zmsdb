<?php

namespace BO\Zmsdb\Tests;

use BO\Zmsdb\Log as LogRepository;
use BO\Zmsdb\Query\Log as LogQueryRepository;
use BO\Zmsdb\Query\Base as BaseQueryRepository;

use BO\Zmsentities\Helper\DateTime as DateTimeHelper;

class LogTest extends Base
{
    public function testBasic(): void
    {
        $logRepo = new LogRepository();
        $logRepo::writeLogEntry("Test", 12345);
        $logList = $logRepo->readByProcessId(12345);
        $this->assertEquals(1477519199, $logList->getFirst()['expires_on']); //26. Mai 2016 23:59:59
        $this->assertEquals(12345, $logList->getFirst()['reference']);
    }

    public function testDeleteExpiredLogs()
    {
        $now = DateTimeHelper::create('2016-04-01 11:55:00');
        $dateTime = $now ->modify('-7 month');
        $logRepo = new LogRepository();
        $logRepo::writeLogEntry("Test", 12345, 'buerger', $dateTime);
        $logRepo::writeLogEntry("Test 2", 12345, 'buerger', $dateTime);
        $logRepo::writeLogEntry("Test 3", 12345, 'buerger', $dateTime);
        $logList = $logRepo->readByProcessId(12345);
        $this->assertEquals(3, $logList->count());
        $logRepo->deleteLogListByExpiresOnDate($now);
        $logRepo->deleteLogListByCreateTimestamp($now);
        $logList = $logRepo->readByProcessId(12345);
        $this->assertEquals(0, $logList->count());
    }

    public function testInTimeLogs()
    {
        $now = DateTimeHelper::create('2016-04-01 11:55:00');
        $dateTime = $now ->modify('-5 month');
        $logRepo = new LogRepository();
        $logRepo::writeLogEntry("Test", 12345, 'buerger', $dateTime );
        $logRepo::writeLogEntry("Test 2", 12345, 'buerger', $dateTime );
        $logRepo::writeLogEntry("Test 3", 12345, 'buerger', $dateTime );
        $logList = $logRepo->readByProcessId(12345);
        $this->assertEquals(3, $logList->count());

        $logRepo->deleteLogListByExpiresOnDate($now);
        $logRepo->deleteLogListByCreateTimestamp($now);
        $logList = $logRepo->readByProcessId(12345);

        $this->assertEquals(3, $logList->count());
    }

    public function testWithoutExpiresOnDate()
    {
        $now = DateTimeHelper::create('2016-04-01 11:55:00');
        $logRepo = new LogRepository();
        $query = new LogQueryRepository(BaseQueryRepository::INSERT);
        $message = " [Unittests]";
        $createTimestamp = $now->modify('- 7months')->format('Y-m-d H:m:s');
        $query->addValues(
            [
                'message' => $message,
                'reference_id' => 12345,
                'type' => 'buerger',
                'ts' => $createTimestamp,
            ]
        );
        $logRepo->writeItem($query);
        $logList = $logRepo->readByProcessId(12345);
        $this->assertEquals(1, $logList->count());

        $logRepo->deleteLogListByExpiresOnDate($now);
        $logRepo->deleteLogListByCreateTimestamp($now);
        $logList = $logRepo->readByProcessId(12345);

        $this->assertEquals(0, $logList->count());
    }

    public function testWithoutExpiresOnDateInTime()
    {
        $now = DateTimeHelper::create('2016-04-01 11:55:00');
        $logRepo = new LogRepository();
        $query = new LogQueryRepository(BaseQueryRepository::INSERT);
        $message = " [Unittests]";
        $createTimestamp = $now->modify('- 5months')->format('Y-m-d H:m:s');
        $query->addValues(
            [
                'message' => $message,
                'reference_id' => 12345,
                'type' => 'buerger',
                'ts' => $createTimestamp,
            ]
        );
        $logRepo->writeItem($query);
        $logList = $logRepo->readByProcessId(12345);
        $this->assertEquals(1, $logList->count());

        $logRepo->deleteLogListByExpiresOnDate($now);
        $logRepo->deleteLogListByCreateTimestamp($now);
        $logList = $logRepo->readByProcessId(12345);

        $this->assertEquals(1, $logList->count());
    }
}
